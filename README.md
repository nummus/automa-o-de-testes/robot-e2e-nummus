# Nummus

Automated testing framework powered by [Node.js](http://nodejs.org/), [Nightwatch.js](http://nightwatchjs.org/) using [W3C Webdriver](https://www.w3.org/TR/webdriver/) (formerly [Selenium](https://github.com/SeleniumHQ/selenium/wiki/JsonWireProtocol)), [Cucumber](https://github.com/cucumber/cucumber-js), [Nightwatch Cucumber Integrator](https://github.com/mucsi96/nightwatch-cucumber) and [Axios](https://github.com/axios/axios).

Nummus is a complete solution for end-to-end testing of web applications and websites.

***

## 1. Install Nummus

Install __Node.js__ minimum 18.16.0 or newer by following instructions available on [nodejs.org](https://nodejs.org).

From NPM:

```sh
npm i @nummus/nummus-cashback-robot
```

## Running tests

To run the complete test suite:

```sh
npm test
```

To run a test tag:

```sh
npm test -- --tags @YourTagHere
```

## Authors

* __Henrique Pavanatti - *Creator and maintainer* - [GitLab](https://gitlab.com/pavanatti)
