require('@babel/register')()

module.exports = {
  assertionsPath: './test/assertions',
  commandsPath: './test/commands',
  featuresPath: './test/features',
  filesPath: './test/files',
  pagesPath: './test/pages',
  resolversPath: './test/resolvers',
  servicesPath: './test/services',
  stepsPath: './test/steps',
  screenshotsPath: './reports/screenshots',
  reportsPath: './reports'
}
