const chalk = require('chalk')
const packageJson = require('./package.json')

module.exports = function () {
  const bannerRobot = () => {
    let env = 'test'

    process.env.NODE_ENV === 'development' ? (env = 'dev') : (env = 'test')

    /* eslint-disable */
    console.log('')
    console.log(chalk.dim.bold('★        ★        ★       ★ ') + chalk.bold.white('     ★'))
    console.log(chalk.white('      ') + chalk.bold('★') + chalk.dim.white('     ★    ,-,-.            ') + chalk.dim.white(' ★'))
    console.log(chalk.bold(' ★') + chalk.dim.white('              /.( o.\\ ') + chalk.dim.white('   ★'))
    console.log(chalk.cyan('   ,') + chalk.cyan('_') + chalk.cyan(',  ') + chalk.white(' ,') + chalk.white('_') + chalk.white(',  ') + chalk.bold('★') + chalk.dim.white(' \\ {. o/ ') + chalk.bold('★') + chalk.bold.white('      ★'))
    console.log(chalk.cyan('  {') + chalk.bold.red('●') + chalk.cyan(',') + chalk.bold.red('●') + chalk.cyan('}') + chalk.white(' {') + chalk.bold.blue('●') + chalk.white(',') + chalk.bold.blue('●') + chalk.white('}') + chalk.dim.white('    `-`-´ ') + chalk.dim.white('           ★'))
    console.log(chalk.cyan(' ./)__)') + chalk.white(' (__(\\.') + chalk.dim.white(`  ★    Environment: ${env}`))
    console.log(chalk.dim.yellow(' ══') + chalk.cyan('▾') + chalk.dim.yellow('═') + chalk.cyan('▾') + chalk.dim.yellow('══') + chalk.dim.yellow('═') + chalk.white('▾') + chalk.dim.yellow('═') + chalk.white('▾') + chalk.dim.yellow('═') + chalk.dim.yellow('═') + chalk.dim.yellow('═') + chalk.dim.yellow('═══════════════════════'))
    console.log(chalk.cyan(` ${packageJson.name} `, chalk.yellow(`(${packageJson.version})`)))
    console.log('')
    /* eslint-enable */
  }

  bannerRobot()
}
