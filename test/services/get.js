import Axios from 'axios'
import { properties, utils } from '../../lib/index.js'

export const getServiceFilter = async (url, filter, limit = 1000) => {
  if (typeof url !== 'string' || typeof filter !== 'string' || typeof limit !== 'number') {
    throw new Error('Invalid input parameters')
  }

  const urlToGet = `${properties.get('url.api')}${url}`

  try {
    const { data } = await Axios.get(urlToGet,
      {
        params: { limit, pageSize: limit, filter },
        headers: { empresa: properties.get('i.codigo') }
      })

    return data
  } catch (error) {
    const err = { error, functionName: 'getServiceFilter' }

    await utils.report.robotErrorHandler(err)
  }
}

export const getServiceQ = async (url, q) => {
  const urlToGet = `${properties.get('url.api')}${url}`

  try {
    const { data } = await Axios.get(urlToGet,
      {
        params: { q },
        headers: { empresa: properties.get('i.codigo') }
      })

    return data
  } catch (error) {
    const err = { error, functionName: 'getServiceQ' }

    await utils.report.robotErrorHandler(err)
  }
}

export const getServiceAll = async (url, limit = 1000) => {
  const urlToGet = `${properties.get('url.api')}${url}`

  try {
    const { data } = await Axios.get(urlToGet,
      {
        params: { limit, pageSize: limit },
        headers: { empresa: properties.get('i.codigo') }
      })

    return data
  } catch (error) {
    const err = { error, functionName: 'getServiceAll' }

    await utils.report.robotErrorHandler(err)
  }
}
