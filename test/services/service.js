import deleting from './delete.js'
import posting from './post.js'

const api = {
  posting,
  deleting
}

export default api
