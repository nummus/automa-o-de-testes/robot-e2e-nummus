import Axios from 'axios'
import { properties, resolvers, utils } from '../../lib/index.js'
import { getServiceAll, getServiceFilter } from './get.js'

export const putService = async (url, ids, body) => {
  const IdOrder = await ids.sort((a, b) => { return b - a })

  for (const id of IdOrder) {
    const urlWithId = `${properties.get('url.api')}${url}/${id}`

    try {
      await Axios.put(urlWithId, body, {
        headers: { empresa: properties.get('i.codigo') }
      })
    } catch (error) {
      const err = { error, functionName: 'putService' }

      await utils.report.robotErrorHandler(err)
    }
  }
}

export const putServiceNoId = async (url, body) => {
  const urlToPut = `${properties.get('url.api')}${url}`

  try {
    await Axios.put(urlToPut, body, {
      headers: { empresa: properties.get('i.codigo') }
    })
  } catch (error) {
    const err = { error, functionName: 'putServiceNoId' }

    await utils.report.robotErrorHandler(err)
  }
}

export const putJsonFilterService = async (urls, filter = '', body, limit = 1000) => {
  urls = await utils.resolver.textResolver(urls)

  if (Array.isArray(urls)) {
    for (const url of urls) {
      await performPut(url, filter, body, limit)
    }
  } else {
    await performPut(urls, filter, body, limit)
  }
}

export const performPut = async (urls, filter = '', body, limit = 1000) => {
  let ids
  let contentFilter
  let data = null

  const jsonObject = JSON.parse(body)
  const jsonSolved = await resolvers.resolvePlainJson(jsonObject)

  if (filter === '') {
    data = await getServiceAll(urls, limit)
  } else {
    data = await getServiceFilter(urls, filter, limit)
  }

  if (data.hasNext === undefined) {
    if (Array.isArray(data)) {
      ids = data.map((item) => item.id)
    } else {
      ids = [data].map((item) => item.id)
    }
  }

  if (data.hasNext !== undefined) {
    contentFilter = data.content
    ids = await contentFilter.map((item) => item.id)
  }

  await putService(urls, ids, jsonSolved)

  if (data.hasNext) {
    await putJsonFilterService(urls, filter, jsonSolved)
  }
}
