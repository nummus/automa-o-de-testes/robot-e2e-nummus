import ora from 'ora'
import { context, properties } from '../../../lib/index.js'

export const standardizeCompany = async (deleteTags) => {
  if (properties.get('disable.interaction')) return

  console.log('')
  const spinConfig = ora('Iniciando configuração da empresa nummus...').start()

  try {
    spinConfig.text = 'Configurando...'

    spinConfig.succeed('Nummus configurado com sucesso.')
  } catch (error) {
    spinConfig.fail('Erro na configuração da empresa nummus.')

    throw error
  }

  const runTags = context.get('tags')
  const iTags = deleteTags.filter((iterator) => runTags.indexOf(iterator)).length

  if (!iTags) return

  console.log('')
  const spinDelete = ora('Limpando e atualizando os dados da empresa...').start()

  try {
    spinDelete.succeed('Dados da empresa limpos e atualizados.')
  } catch (error) {
    spinDelete.fail(`Erro ao limpar e atualizar os dados da empresa: ${spinDelete.text}`)

    throw error
  }

  if (properties.get('nightwatch.detailed.output')) console.log('')
}
