import chalk from 'chalk'
import { context, properties, utils } from '../../lib/index.js'
import { standardizeCompany } from '../services/custom/standardizer.js'

const doLogin = async () => {
  const systemName = properties.get('system.name')
  const companyMail = properties.get('company.email')
  const companyPassword = properties.get('company.password')
  const mailFieldElement = '[type="email"]'
  const passwordFieldElement = '[type="password"]'
  const enterButtonElement = '[type="submit"]'
  const companyElement = '#empresas'

  utils.logger.info('action', `Logging in "${systemName}"`)

  if (properties.get('nightwatch.detailed.output')) console.log('')

  await browser
    .waitForElementVisible(mailFieldElement)
    .waitForElementVisible(passwordFieldElement)
    .setValue(mailFieldElement, companyMail)
    .setValue(passwordFieldElement, companyPassword)
    .pause(500)

  await browser
    .click({ selector: enterButtonElement, abortOnFailure: true })
    .waitForElementVisible(companyElement)
}

const selectCompany = async () => {
  const companyName = properties.get('company.name')
  const companyListElement = '#empresas option'
  const totalElement = '[class="header-container center"]'

  utils.logger.info('action', `Selecting the company "${companyName}"`)

  if (properties.get('nightwatch.detailed.output')) console.log('')

  await browser
    .waitForElementPresent(companyListElement)
    .clickElementText(companyListElement, companyName)
    .pause(2000)
    .waitForElementVisible(totalElement)
}

export const beforeFeatureActions = async () => {
  context.set('isBeforeAllCalled', true)

  const loginStatus = !context.get('loginStatus')
  const scenarioNumber = context.get('currentScenarioNumber')
  const loginActionExecuted = scenarioNumber > 1 && loginStatus

  if (!context.get('isNewFeature') || loginActionExecuted) return

  console.log(chalk.green.bold('→ →'), chalk.green.bold('Before Feature'))
  console.log('')

  try {
    const runTags = context.get('tags')
    const shouldNotLogin = runTags.includes('@NaoExecutarLogin')

    if (!browser) throw new Error('Browser not found!')

    if (!loginActionExecuted) await browser.init()

    await browser.resizeWindow(1920, 1080)

    if (shouldNotLogin) console.log('')
    if (shouldNotLogin) return console.log(chalk.green.bold('← ←'), chalk.green.bold('Before Feature'))

    await doLogin()
    await selectCompany()

    await browser.getBearerAuthToken()

    context.set('loginStatus', true)
  } catch (error) {
    throw new Error('beforeFeatureActions' + error)
  }

  console.log('')
  console.log(chalk.green.bold('← ←'), chalk.green.bold('Before Feature'))
}

export const beforeScenarioActions = async () => {
  const skipBeforeScenario = context.get('tags').includes('@NaoExecutarBeforeScenario')
  console.log('')

  if (!context.get('isNewScenario') && !skipBeforeScenario) return

  console.log(chalk.yellow.bold('→ →'), chalk.yellow.bold('Before Scenario'))

  try {
    const tagsToExecuteClear = [
      '@Agenda', '@Ajustes', '@Vendas', '@Estoque', '@Financeiro', '@Loja', '@MinhaConta', '@Paciente'
    ]

    if (!browser) throw new Error('Browser not found!')

    await standardizeCompany(tagsToExecuteClear)

    await browser.waitForElementNotPresent(utils.action.loaderSelector)
  } catch (error) {
    throw new Error('beforeScenarioActions' + error)
  }

  console.log('')
  console.log(chalk.yellow.bold('← ←'), chalk.yellow.bold('Before Scenario'))
  console.log('')
}
