import { Then } from '@cucumber/cucumber'
import { context, resolvers, utils } from '../../lib/index.js'
import { putServiceNoId } from '../services/put.js'

Then(/^atualiza a seguinte informação em "([^"]*)"$/,
  async (url, data) => {
    if (!url) return

    const normalizedUrl = await utils.service.resolveUrl(url)

    const jsonObject = JSON.parse(data)
    const jsonSolved = await resolvers.resolvePlainJson(jsonObject)

    await putServiceNoId(normalizedUrl, jsonSolved)
  })

Then(/^atualiza a seguinte informação em "([^"]*)" no início da feature$/,
  async (url, data) => {
    if (!context.get('isNewFeature') || !url) return

    const normalizedUrl = await utils.service.resolveUrl(url)

    const jsonObject = JSON.parse(data)
    const jsonSolved = await resolvers.resolvePlainJson(jsonObject)

    await putServiceNoId(normalizedUrl, jsonSolved)
  })

Then(/^atualiza a seguinte informação em "([^"]*)" no início do scenario$/,
  async (url, data) => {
    if (!context.get('isNewScenario') || !url) return

    const normalizedUrl = await utils.service.resolveUrl(url)

    const jsonObject = JSON.parse(data)
    const jsonSolved = await resolvers.resolvePlainJson(jsonObject)

    await putServiceNoId(normalizedUrl, jsonSolved)
  })
