module.exports = {
  elements: {
    button: {
      selector: 'button'
    },
    listLine: {
      selector: 'table tr'
    }
  }
}
