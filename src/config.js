import Axios from 'axios'
import axiosRetry from 'axios-retry'
import os from 'os'
import { resolve } from 'path'
import PropertiesReader from 'properties-reader'
import context from './context.js'
import logger from './utils/logger.js'
import { filterBuilder } from './utils/service.utils.js'

Axios.interceptors.request.use(
  (config) => {
    if (context.get('bearerTokenHash')) {
      config.headers['Access-Control-Allow-Headers'] = 'Authorization'
      config.headers.Authorization = `Bearer ${context.get('bearerTokenHash')}`
    }

    config.headers['Filter-Encoded'] = true

    filterBuilder(config.params)

    return config
  },
  (error) => Promise.reject(error)
)

axiosRetry(Axios, {
  retries: 5,
  retryDelay: () => 2000,
  onRetry: (retryCount, err, requestConfig) => {
    logger.warn('warn', `Retry ${requestConfig.url} attempt ${retryCount}: ${err}`)
  }
})

const hostname = os.hostname()
context.set('hostname', hostname)

const reader = PropertiesReader(resolve('./config.properties'))
reader.append(resolve('./urls.properties'))

export const properties = {
  get: (key, defaultValue) => {
    const hostKey = reader.get(`${hostname}.${key}`)

    if (hostKey !== null) return hostKey

    const defaultKey = reader.get(`default.${key}`)

    if (defaultKey !== null) return defaultKey

    return defaultValue
  },
  getRaw: (key) => {
    const hostKey = reader.getRaw(`${hostname}.${key}`)

    if (hostKey !== null) return hostKey

    const defaultKey = reader.getRaw(`default.${key}`)

    if (defaultKey !== null) return defaultKey
  },
  set: (key, value) => reader.set(`${hostname}.${key}`, value)
}

export default properties
