import logger from '../utils/logger.js'

export default function tunIntoNull () {
  logger.debug('resolver', 'tunIntoNull() ✔')

  return null
}
