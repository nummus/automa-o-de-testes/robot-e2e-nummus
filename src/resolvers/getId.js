import { properties } from '../config.js'
import { doGet, doGetFilter } from '../services/get.core.js'
import { robotErrorHandler } from '../utils/report.utils.js'
import { textResolver } from '../utils/resolver.utils.js'

export default async function getId (url, filter = null, value = null, limit = 50) {
  /**
   * Get the url from properties
   */
  const resolvedUrl = await textResolver(url.toLowerCase())
  const urlMap = properties.get(resolvedUrl) || resolvedUrl

  if (filter && !value) {
    const error = 'Obrigatório informar o valor para o filtro'

    const err = { error, functionName: 'getId' }

    await robotErrorHandler(err)
  }

  if (!filter && value) {
    const error = 'Obrigatório informar o filtro para o valor'

    const err = { error, functionName: 'getId' }

    await robotErrorHandler(err)
  }

  /**
   * Searching data, strategies change depending of the filter variable or qUrls variable
   */
  const result = !filter && !value
    ? await doGet(urlMap, limit)
    : await doGetFilter(urlMap, filter, value, limit)

  /**
   * If result have the content property,
   * return the ids or if no filter has been applied,
   * we return the first id of the array
   */
  if (result?.content?.length) {
    if (!filter) return result.content[0].id

    return result.content.map(object => object.id)[0]
  }

  /**
   * Return the first id of the first item of the array
   */
  if (result?.length) return result[0].id

  /**
   * Return a message if no data has been found
   */

  const error = !filter
    ? `Nothing found in URL: ${urlMap}.`
    : `Nothing found in URL: ${urlMap} with filter: ${filter} and value: ${value}.`

  const err = { error, functionName: 'getId' }

  await robotErrorHandler(err)
}
