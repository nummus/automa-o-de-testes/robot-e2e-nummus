import logger from '../utils/logger.js'
import context from './../context.js'

export default async function getContext (type) {
  const value = context.get(type)

  logger.debug('resolver', `getContext(${type}) context get:`, value)

  return value
}
