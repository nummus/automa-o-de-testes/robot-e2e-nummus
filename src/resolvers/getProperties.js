import { properties } from '../config.js'

export default async function getProperties (type) {
  return properties.get(`${type}`)
}
