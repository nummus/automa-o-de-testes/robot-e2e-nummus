import getTime from './getTime.js'

export default async function getTimePlus (format, typesAndTimes) {
  let timeAfterAdd = null
  typesAndTimes = typesAndTimes.split('/')

  for (let index = 0; index < typesAndTimes.length; index++) {
    let formatToUse = format
    const addOrSubtractArray = typesAndTimes[index].split(':')
    const type = addOrSubtractArray[0]
    const quantity = addOrSubtractArray[1]

    if (index > 0) formatToUse = timeAfterAdd

    timeAfterAdd = await getTime(formatToUse, quantity, type)
  }

  return timeAfterAdd
}
