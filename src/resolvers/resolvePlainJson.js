import logger from '../utils/logger.js'
import { plainJsonResolver } from '../utils/resolver.utils.js'

export default async function resolvePlainJson (plainJson) {
  if (typeof plainJson === 'string') {
    plainJson = JSON.parse(plainJson)
  }

  logger.debug('resolver', 'resolvePlainJson() Json sent:', plainJson)

  await plainJsonResolver(plainJson)

  logger.debug('resolver', 'resolvePlainJson() Json resolved:', plainJson)

  return plainJson
}
