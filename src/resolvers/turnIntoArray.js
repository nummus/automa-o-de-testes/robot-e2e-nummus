import logger from '../utils/logger.js'
import { textResolver } from '../utils/resolver.utils.js'

export default async function turnIntoArray (...values) {
  logger.debug('resolver', 'turnIntoArray()', `Values sent: ${values}`)

  const valuesArray = await Promise.all(values.map((v) => textResolver(v)))

  logger.debug('resolver', 'turnIntoArray()', `Array returned: ${valuesArray}`)

  return valuesArray
}
