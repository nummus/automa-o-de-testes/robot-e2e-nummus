import logger from '../utils/logger.js'
import getJsonFileData from './getJsonFileData.js'
import resolvePlainJson from './resolvePlainJson.js'

export default async function resolveJson (jsonPath) {
  const jsonData = getJsonFileData(jsonPath)

  logger.debug('resolver', 'resolveJson() json data:', jsonData)

  const jsonResolved = await resolvePlainJson(jsonData)

  logger.debug('resolver', 'resolveJson() json data resolved:', jsonResolved)

  return jsonResolved
}
