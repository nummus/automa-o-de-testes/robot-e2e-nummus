import properties from '../config.js'
import { doGet } from '../services/get.core.js'
import { textResolver } from '../utils/resolver.utils.js'

export default async function getIdPosition (url, position = 0, limit = 1000) {
  const urlResolved = await textResolver(url.toLowerCase())
  const urlMap = properties.get(urlResolved) || urlResolved

  const data = await doGet(urlMap, limit)

  if (!data) return

  const dataArray = data.content || data
  const ids = dataArray.map(item => item.id)

  const id = ids[position]

  return id
}
