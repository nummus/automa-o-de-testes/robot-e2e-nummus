import fs from 'fs'
import { join, resolve } from 'path'
import { getConfig } from '../utils/base.utils.js'
import logger from '../utils/logger.js'

export default function getJsonFileData (filePath) {
  const jsonPath = join(resolve(getConfig('filesPath')), filePath)

  logger.debug('resolver', 'getJsonFileData() json path:', jsonPath)

  const jsonData = fs.readFileSync(jsonPath, 'utf-8')

  logger.debug('resolver', 'getJsonFileData() json data:', jsonData)

  return JSON.parse(jsonData)
}
