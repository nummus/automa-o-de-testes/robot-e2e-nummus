import jp from 'jsonpath'
import logger from '../utils/logger.js'
import { textResolver } from '../utils/resolver.utils.js'

export default async function filterJson (object, filter = '$.*') {
  logger.debug('resolver', 'filterJson() Object sent:', object)
  logger.debug('resolver', 'filterJson() Filter sent:', filter)

  const objectResolver = await textResolver(object)

  logger.debug('resolver', 'filterJson() Object resolved:', objectResolver)

  const objectResolverFilter = await jp.query(objectResolver, filter)

  logger.debug('resolver', 'filterJson() Object filtered:', objectResolverFilter)

  return objectResolverFilter
}
