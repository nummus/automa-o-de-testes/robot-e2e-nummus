import { DateTime } from 'luxon'
import context from '../context.js'
import { isNumber, isString } from '../utils/base.utils.js'
import logger from '../utils/logger.js'
import { addTime, isDateOrFormat, parameterValidToAdd, parameterValidToSubtract, subtractTime } from '../utils/time.utils.js'

/*!
 * https://moment.github.io/luxon/#/formatting?id=table-of-tokens
 *
 * ['+Day', '+Month', '+Year', '+Hour', '+Minute', '+Second']
 * ['-Day', '-Month', '-Year', '-Hour', '-Minute', '-Second']
 * ['days', 'months', 'years', 'hours', 'minutes', 'seconds']
 *
 * Set arguments, examples:
 *
 * await getTime(1)
 * await getTime(1, '+Year')
 * await getTime('yyyy-LL-dd')
 * await getTime('yyyy-LL-dd', 1)
 * await getTime('yyyy-LL-dd', 1, '+Year')
 */
export default function getTime (...args) {
  let timeFormat = 'D'
  let timeToAddSubtract = 0
  let timePeriodResult = null
  let timeChangeParameter = '+DAY'

  let timeNow = DateTime.now().setLocale('pt-br')

  args = args.map(arg => /^\d+$/g.test(arg) ? parseInt(arg) : arg)

  const [arg0, arg1, arg2] = args

  if (isNumber(arg0)) {
    timeToAddSubtract = arg0
    timeChangeParameter = isString(arg1) ? arg1.toUpperCase().trim() : '+DAY'
  } else if (isString(arg0)) {
    timeFormat = arg0
    timeToAddSubtract = isNumber(arg1) ? arg1 : 0
    timeChangeParameter = isString(arg2) ? arg2.toUpperCase().trim() : '+DAY'
  }

  const isDate = isDateOrFormat(timeFormat)

  if (isDate && !isDate.invalid) {
    timeNow = isDate.parsedDate
    timeFormat = isDate.format
  }

  if (!timeToAddSubtract) {
    timePeriodResult = timeNow.toFormat(timeFormat)
    const dateObjectPlus3 = timeNow.plus({ hours: 3 })

    context.set('dateObject', timeNow.c)
    context.set(`${timeFormat}`, timePeriodResult)
    context.set('dateObjectUTC3', dateObjectPlus3.c)
    context.set(`date${timeFormat}`, timeNow.toFormat(timeFormat))
  } else {
    if (parameterValidToAdd.indexOf(timeChangeParameter) > -1) {
      timePeriodResult = addTime(timeNow, timeFormat, timeChangeParameter, timeToAddSubtract)
    }

    if (parameterValidToSubtract.indexOf(timeChangeParameter) > -1) {
      timePeriodResult = subtractTime(timeNow, timeFormat, timeChangeParameter, timeToAddSubtract)
    }
  }

  logger.debug('resolver', 'getTime() Date returned:', timePeriodResult)

  return timePeriodResult
}
