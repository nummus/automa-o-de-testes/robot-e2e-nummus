import logger from '../utils/logger.js'

export default function orderArrayBy (array, by = 'DESC') {
  if (by === 'DESC') return array.sort((a, b) => { return b - a })

  const orderArray = array.sort((a, b) => { return a - b })

  logger.debug('resolver', 'orderArrayBy() array ordered:', orderArray)

  return array.sort((a, b) => { return a - b })
}
