import logger from '../utils/logger.js'
import filterJson from './filterJson.js'
import getJsonFileData from './getJsonFileData.js'

export default async function getJsonFileDataFilter (jsonPath, filter) {
  const jsonData = getJsonFileData(jsonPath)

  logger.debug('resolver', 'getJsonFileDataFilter() json data:', jsonData)

  const jsonFiltered = filterJson(jsonData, filter)

  logger.debug('resolver', 'getJsonFileDataFilter() json data filtered:', jsonFiltered)

  return jsonFiltered
}
