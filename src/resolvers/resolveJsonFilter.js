import logger from '../utils/logger.js'
import filterJson from './filterJson.js'
import resolveJson from './resolveJson.js'

export default async function resolveJsonFilter (jsonPath, filter = '$.*') {
  const jsonResolved = await resolveJson(jsonPath)

  logger.debug('resolver', 'resolveJsonFilter() json resolved:', jsonResolved)

  return filterJson(jsonResolved, filter)
}
