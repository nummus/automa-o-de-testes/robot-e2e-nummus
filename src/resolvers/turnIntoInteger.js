import logger from '../utils/logger.js'

export default function turnIntoInteger (value) {
  logger.debug('resolver', 'turnIntoInteger() ✔')

  return parseInt(value)
}
