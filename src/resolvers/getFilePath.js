import { join, resolve } from 'path'
import { getConfig } from '../utils/base.utils.js'
import logger from '../utils/logger.js'

export default function getFilePath (fileName) {
  logger.debug('resolver', 'getFilePath() File name sent:', fileName)

  const filesPath = join(resolve(getConfig('filesPath')), fileName)

  logger.debug('resolver', 'getFilePath() File path obtained:', filesPath)

  return filesPath
}
