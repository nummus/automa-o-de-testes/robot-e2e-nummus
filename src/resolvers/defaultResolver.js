import Axios from 'axios'
import { properties } from '../config.js'
import logger from '../utils/logger.js'

export default async function defaultResolver (url, filter) {
  logger.debug('resolver', 'defaultResolver() Default url sent:', url)
  logger.debug('resolver', 'defaultResolver() Default filter sent:', filter)

  const { data } = await Axios.get(`${properties.get('url.api')}/${url}`,
    {
      params: { filter }
    }
  )

  logger.debug('resolver', 'defaultResolver() Value obtained:', data)

  return data
}
