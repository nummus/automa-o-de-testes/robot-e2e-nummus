import { properties } from '../config.js'
import { doGet, doGetFilter } from '../services/get.core.js'
import { robotErrorHandler } from '../utils/report.utils.js'
import { textResolver } from '../utils/resolver.utils.js'

export default async function getAllTheIds (url, filter = null, value = null, limit = 1000) {
  /**
   * Get the url from properties
   */
  const resolvedUrl = await textResolver(url.toLowerCase())
  const urlMap = properties.get(resolvedUrl) || resolvedUrl

  if (filter && !value) {
    const error = 'Obrigatório informar o valor para o filtro'

    const err = { error, functionName: 'getAllTheIds' }

    await robotErrorHandler(err)
  }

  if (!filter && value) {
    const error = 'Obrigatório informar o filtro para o valor'

    const err = { error, functionName: 'getAllTheIds' }

    await robotErrorHandler(err)
  }

  const result = !filter && !value
    ? await doGet(urlMap, limit)
    : await doGetFilter(urlMap, filter, value, limit)

  if (!result) return []

  if (result?.content?.length) {
    return result.content.map(object => object.id)
  }

  if (result?.length) {
    return result.map(object => object.id)
  }

  return []
}
