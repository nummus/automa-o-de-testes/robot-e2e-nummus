import Axios from 'axios'
import { properties } from '../config.js'
import resolvePlainJson from '../resolvers/resolvePlainJson.js'
import logger from '../utils/logger.js'
import { robotErrorHandler } from '../utils/report.utils.js'
import { textResolver } from '../utils/resolver.utils.js'
import { doGet, doGetFilter } from './get.core.js'

export const doPutIds = async (url, ids, body) => {
  const IdOrder = await ids.sort((a, b) => { return b - a })

  logger.debug('post', 'putAll() Body:', body)

  for (const id of IdOrder) {
    const urlWithId = `${properties.get('url.api')}${url}/${id}`

    logger.debug('post', 'putAll() URL:', urlWithId)

    try {
      await Axios.put(urlWithId, body, {
        headers: { empresa: properties.get('i.codigo') }
      })
    } catch (error) {
      const err = { error, functionName: 'putAll' }

      await robotErrorHandler(err)
    }
  }
}

export const doPutNoIds = async (url, body) => {
  const urlToPut = `${properties.get('url.api')}${url}`

  logger.debug('post', 'putWithoutId() URL:', urlToPut)
  logger.debug('post', 'putWithoutId() Body:', body)

  try {
    await Axios.put(urlToPut, body, {
      headers: { empresa: properties.get('i.codigo') }
    })
  } catch (error) {
    const err = { error, functionName: 'putWithoutId' }

    await robotErrorHandler(err)
  }
}

export const doPutFilter = async (urls, filter = '', body, limit = 1000) => {
  urls = await textResolver(urls)

  if (Array.isArray(urls)) {
    for (const url of urls) {
      await performPut(url, filter, body, limit)
    }
  } else {
    await performPut(urls, filter, body, limit)
  }
}

export const performPut = async (urls, filter = '', body, limit = 1000) => {
  let ids
  let contentFilter
  let data = null

  const jsonObject = JSON.parse(body)
  const jsonSolved = await resolvePlainJson(jsonObject)

  if (filter === '') {
    data = await doGet(urls, limit)
  } else {
    data = await doGetFilter(urls, filter, limit)
  }

  if (data.hasNext === undefined) {
    if (Array.isArray(data)) {
      ids = data.map((item) => item.id)
    } else {
      ids = [data].map((item) => item.id)
    }
  }

  if (data.hasNext !== undefined) {
    contentFilter = data.content
    ids = await contentFilter.map((item) => item.id)
  }

  await doPutIds(urls, ids, jsonSolved)

  if (data.hasNext) {
    await doPutFilter(urls, filter, jsonSolved)
  }
}
