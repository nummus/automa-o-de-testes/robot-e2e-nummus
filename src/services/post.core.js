import Axios from 'axios'
import { properties } from '../config.js'
import getJsonFileData from '../resolvers/getJsonFileData.js'
import resolvePlainJson from '../resolvers/resolvePlainJson.js'
import logger from '../utils/logger.js'
import { robotErrorHandler } from '../utils/report.utils.js'
import { textResolver } from '../utils/resolver.utils.js'
import { resolveUrl } from '../utils/service.utils.js'

export const doPostJsonString = async (url, json) => {
  const urlResolved = await textResolver(url.toLowerCase())
  const urlMap = properties.get(urlResolved) || urlResolved
  const urlToPost = `${properties.get('url.api')}${urlMap}`

  logger.debug('post', 'postJsonString() URL:', urlToPost)

  const records = JSON.parse(json)

  for (const record of records) {
    logger.debug('post', 'postJsonString() Body:', record)

    const jsonSolved = await resolvePlainJson(record)

    try {
      await Axios.post(urlToPost, jsonSolved, {
        headers: { empresa: properties.get('i.codigo') }
      })
    } catch (error) {
      const err = { error, functionName: 'postJsonString' }

      await robotErrorHandler(err)
    }
  }
}

export const doPostObject = async (url, body) => {
  const urlResolved = await textResolver(url.toLowerCase())
  const urlMap = properties.get(urlResolved) || urlResolved
  const urlToPost = `${properties.get('url.api')}${urlMap}`

  logger.debug('post', 'postGeneric() URL:', urlToPost)

  try {
    await Axios.post(urlToPost, body, {
      headers: { empresa: properties.get('i.codigo') }
    })
  } catch (error) {
    const err = { error, functionName: 'postGeneric' }

    await robotErrorHandler(err)
  }
}

export const doPostJson = async (url, json) => {
  const urlToPost = `${properties.get('url.api')}${url}`

  logger.debug('post', 'postJsonNotInArray() URL:', urlToPost)

  const records = []

  records.push(JSON.parse(json))

  for (const record of records) {
    logger.debug('post', 'postJsonNotInArray() Body:', record)

    try {
      const json = await resolvePlainJson(record)

      await Axios.post(urlToPost, json, {
        headers: { empresa: properties.get('i.codigo') }
      })
    } catch (error) {
      const err = { error, functionName: 'postJsonNotInArray' }

      await robotErrorHandler(err)
    }
  }
}

export const doPostJsonArray = async (url, jsonData) => {
  const urlToPost = `${properties.get('url.api')}${url}`

  logger.debug('post', 'postAllJsonInArray() URL:', urlToPost)

  for (const json of jsonData) {
    const solvedJson = await resolvePlainJson(json)

    logger.debug('post', 'postAllJsonInArray() solvedJson:', solvedJson)

    try {
      await Axios.post(urlToPost, solvedJson, {
        headers: { empresa: properties.get('i.codigo') }
      })
    } catch (error) {
      const err = { error, functionName: 'postAllJsonInArray' }

      await robotErrorHandler(err)
    }
  }
}

export const doPostJsonArrayIndex = async (url, json, index) => {
  const solvedJson = await resolvePlainJson(json[index])
  const urlToPost = `${properties.get('url.api')}${url}`

  logger.debug('post', 'postAllJsonInArray() URL:', urlToPost)
  logger.debug('post', 'postSelectedJsonInArray() Body:', solvedJson)

  try {
    await Axios.post(urlToPost, solvedJson, {
      headers: { empresa: properties.get('i.codigo') }
    })
  } catch (error) {
    const err = { error, functionName: 'postSelectedJsonInArray' }

    await robotErrorHandler(err)
  }
}

export const doPostJsonFile = async (json) => {
  const jsonData = JSON.parse(json)

  for (const pathJson of jsonData) {
    const { url, filePath, index } = pathJson
    const solvedUrl = await resolveUrl(url)
    const jsonData = await getJsonFileData(filePath)

    if (index >= 0) {
      await doPostJsonArrayIndex(solvedUrl, jsonData, index)
    } else {
      await doPostJsonArray(solvedUrl, jsonData)
    }
  }
}
