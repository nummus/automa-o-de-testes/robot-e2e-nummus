import Axios from 'axios'
import jp from 'jsonpath'
import { properties } from '../config.js'
import getJsonFileData from '../resolvers/getJsonFileData.js'
import logger from '../utils/logger.js'
import { robotErrorHandler } from '../utils/report.utils.js'
import { textResolver } from '../utils/resolver.utils.js'
import { resolveUrl } from '../utils/service.utils.js'
import { doGet, doGetFilter } from './get.core.js'

export const doDelete = async (url, ids) => {
  const IdInOrder = await ids.sort((a, b) => { return b - a })

  for (const id of IdInOrder) {
    const urlWithId = `${properties.get('url.api')}${url}/${id}`

    logger.debug('delete', 'doDelete() URL:', urlWithId)

    try {
      await Axios.delete(urlWithId, {
        headers: { empresa: properties.get('i.codigo') }
      })
    } catch (error) {
      const err = { error, functionName: 'doDelete' }

      await robotErrorHandler(err)
    }
  }
}

export const doDeleteWithoutId = async (url) => {
  const urlToDelete = `${properties.get('url.api')}${url}`

  logger.debug('delete', 'doDeleteWithoutId() URL:', urlToDelete)

  try {
    await Axios.delete(urlToDelete, {
      headers: { empresa: properties.get('i.codigo') }
    })
  } catch (error) {
    const err = { error, functionName: 'doDeleteWithoutId', statusSkip: 404 }

    await robotErrorHandler(err)
  }
}

export const doDeleteUrl = async (url) => {
  logger.debug('delete', 'deleteServiceWithNoPropertiesUrl() URL:', url)

  try {
    await Axios.delete(`${url}`, {
      headers: { empresa: properties.get('i.codigo') }
    })
  } catch (error) {
    const err = { error, functionName: 'deleteServiceWithNoPropertiesUrl', statusSkip: 404 }

    await robotErrorHandler(err)
  }
}

const performDelete = async (url, filter, urlGet, limit = 1000) => {
  let data = null
  let contentFilter = null

  urlGet
    ? !filter
        ? (data = await doGet(urlGet, limit))
        : (data = await doGetFilter(urlGet, filter, limit))
    : !filter
        ? (data = await doGet(url, limit))
        : (data = await doGetFilter(url, filter, limit))

  data.content ? (contentFilter = data.content) : (contentFilter = data)

  if (!Array.isArray(contentFilter)) {
    contentFilter = [contentFilter]
  }

  const ids = await contentFilter.map((item) => item.id)
  const idsInOrder = await ids.sort((a, b) => { return a - b })

  const doShiftIds = async (urlTarget) => {
    const urlsToShift = await getJsonFileData('/properties/url_ids_to_shift.json')

    for (const data of urlsToShift) {
      const urlResult = await resolveUrl(data.url)

      if (urlTarget === urlResult) {
        for (let index = 0; index < data.quantity; index++) {
          idsInOrder.shift()
        }
      }
    }
  }

  await doShiftIds(url)

  if (!idsInOrder) return

  await doDelete(url, idsInOrder)

  if (!data.last && data.last !== undefined) {
    await doDeleteFilter({ url, filter, urlGet, limit })
  }
}

const performDeleteJsonPathFilter = async (url, filter, limit) => {
  const data = await doGet(url, limit)

  logger.debug('delete', 'performDeleteLocalFilter() URL:', url)
  logger.debug('delete', 'performDeleteLocalFilter() Filter:', filter)
  logger.debug('delete', 'performDeleteLocalFilter() Content:', data)

  if (!data) return

  if (filter === '') filter = '$.*'

  const contentFilter = await jp.query(data.content, filter)

  if (!contentFilter) return

  const ids = await contentFilter.map((item) => item.id)

  await doDelete(url, ids)
}

export const doDeleteFilter = async (deleteInfo) => {
  const { url, filter, urlGet, limit, message } = deleteInfo
  const urlMap = await resolveUrl(url)

  if (message) {
    logger.info('delete', `Excluindo ${message}`)
  }

  if (urlMap) {
    logger.debug('delete', 'deleteServiceFilterApi() URL:', urlMap)
  }

  if (filter) {
    logger.debug('delete', 'deleteServiceFilterApi() Filter:', filter)
  }

  await performDelete(urlMap, filter, urlGet, limit)
}

export const doDeleteJsonPathFilter = async (urls, filter = '', limit = 1000) => {
  urls = await textResolver(urls)

  if (!Array.isArray(urls)) urls = [urls]

  logger.debug('delete', 'deleteServiceFilterLocal() URL:', urls)
  logger.debug('delete', 'deleteServiceFilterLocal() Filter:', filter)

  for (const url of urls) {
    await performDeleteJsonPathFilter(url, filter, limit)
  }
}
