module.exports = {
  elements: {
    email: {
      selector: '[name="email"]'
    },
    password: {
      selector: '[type="password"]'
    },
    enter: {
      selector: 'button[color="accent"]'
    }
  }
}
