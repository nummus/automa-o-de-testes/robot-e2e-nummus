const chalk = require('chalk')
const BaseWait = require('./_baseWait.js')

/**
 * @global
 */

/**
 * @name waitForElementValue()
 *
 * @description
 *
 * 1. Waits a given time in milliseconds (default 5000ms) for an element with form element's value
 * to be present in the page before performing any other commands or assertions.
 *
 * @param {string} selector The selector (CSS) used to locate the element.
 * @param {string} value The form element's value that  will be searched
 * @param {number} index Index of the element in the page
 * @param {boolean} isEquals true for NON PARTIAL value validation or false for PARTIAL
 * @param {boolean} abortOnFailure true to fail the test if element is no found or false if you wish for the test to continue.
 * @param {number} timeout The total number of milliseconds to wait before failing
 * @param {number} retryInterval The total number of milliseconds to wait before retry the command if it fails
 * @param {boolean} formatting true to ENABLE formatting or false do DISABLE formatting in value comparison
 * @param {boolean} typeIndex FALSE to use global index validation or TRUE to use only with text index validation
 *
 * @example
 * await browser.waitForElementValue('label', 'value')
 * await browser.waitForElementValue('label', 'value', callback = () => {})
 *
 * Using options:
 *
 * await browser.waitForElementValue({
 *                  selector: 'label',
 *                  value: 'value',
 *                  index: 1
 *               })
 *
 * await browser.waitForElementValue({
 *                  selector: 'label',
 *                  value: 'value',
 *                  index: 1
 *               }, , callback = () => {})
 *
 * await browser.waitForElementValue({
 *                  selector: 'label',
 *                  value: 'Salvar',
 *                  quantity: 2
 *               }, , callback = () => {})
 */
class waitForElementValue extends BaseWait {
  setArguments () {
    const firstArgIsObject =
      this.args[0] !== undefined &&
      typeof this.args[0] === 'object' &&
      this.args[0].name === undefined

    const hasValidNoObjectArgs = (this.args.length === 2 || this.args.length === 3)

    if (firstArgIsObject) {
      const optionsReceived = this.args.shift()
      this.options = JSON.stringify(optionsReceived)

      const {
        selector, value, index, timeout, quantity, typeIndex,
        retryInterval, message, abortOnFailure, isEquals,
        formatting, locateStrategy, notFoundErrors, retryAction
      } = optionsReceived

      if (message) this.message = message
      if (value !== undefined) this.value = value
      if (selector !== undefined) this.selector = selector
      if (index !== undefined) this.index = parseInt(index)
      if (isEquals !== undefined) this.isEquals = isEquals
      if (formatting !== undefined) this.formatting = formatting
      if (quantity !== undefined) this.quantity = parseInt(quantity)
      if (typeIndex !== undefined) this.typeIndex = typeIndex

      if (timeout !== undefined) this.ms = this.setMilliseconds(timeout)
      if (retryInterval !== undefined) this.setRescheduleInterval(retryInterval)
      if (abortOnFailure !== undefined) this.abortOnFailure = abortOnFailure
      if (locateStrategy) this.setStrategy(locateStrategy)
      if (retryAction !== undefined) this.retryOnFailure = retryAction
      if (notFoundErrors !== undefined) this.suppressNotFoundErrors = notFoundErrors
    } else if (!firstArgIsObject && hasValidNoObjectArgs) {
      this.selector = this.args.shift()
      this.value = this.args.shift()
    } else {
      const error = `"${this.commandName}" method expects a css selector and a element (or a object).`

      console.error(chalk.red(error))
    }

    this.setOutputMessage()
    this.setCallback()

    return this
  }

  async findElement () {
    const { element, commandName } = this

    try {
      const result = await this.elementLocator.findElement({
        element, commandName, returnSingleElement: false, cacheElementId: false
      })

      if (result.error) throw result

      const resultAfterTryFindValue = await this.findElementWithValue(result)

      if (resultAfterTryFindValue.WebdriverElementId) return resultAfterTryFindValue

      throw this.noSuchElementError(resultAfterTryFindValue)
    } catch (error) {
      if (error.name === 'ReferenceError' || error.name === 'TypeError') throw error

      throw this.noSuchElementError(error)
    }
  }

  elementFound (result) {
    let message = `Element <%s> with property value "${this.value}"`

    if (this.index) message += ` in index ${this.index} was present after`
    if (this.quantity) message += ` appeared ${this.quantity} times after`

    message += ` ${this.executor.elapsedTime} milliseconds.`

    return this.pass(result, message)
  }

  elementNotFound (result) {
    let message = this.abortOnFailure
      ? chalk.red('Timed out while waiting for element')
      : chalk.green('Timed out while waiting for element')

    message += chalk.magenta(' <%s> ')

    if (this.index) {
      message += this.abortOnFailure
        ? chalk.red('in index ')
        : chalk.green('in index ')

      message += `${this.index} `
    }

    if (this.quantity) {
      message += this.abortOnFailure
        ? chalk.red(`appear ${this.quantity} times`)
        : chalk.green(`appear ${this.quantity} times`)
    }

    message += this.abortOnFailure
      ? chalk.red('\nwith property value ')
      : chalk.green('with property value ')

    message += chalk.blue(`"${this.value}" `)
    message += chalk.red(`to be present for ${this.ms} milliseconds.`)

    message += this.abortOnFailure
      ? chalk.red('\nElement can not be located, please verify parameters and your system behavior.\n')
      : ''

    return this.fail(result, `not ${this.expectedValue}`, this.expectedValue, message)
  }
}

module.exports = { waitForElementValue }
