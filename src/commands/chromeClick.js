const logger = require('../utils/logger.js')

/**
 * @global
 */

/**
 * @name chromeClick()
 *
 * @description
 *
 * 1. Click using chrome console in the element on the page.
 *
 * @param {string} selector The selector (CSS) used to locate the element.
 * @param {number} index Index of the element in the page
 *
 * @example
 * await browser.chromeClick('label')
 *
 * await browser.chromeClick('label', 2)
 *
 * await browser.chromeClick('label', 2, callback = () => {})
 */

export const command = function (selector, index = 0, callback = () => { }) {
  this.execute((selector, index) => {
    const elements = document.querySelectorAll(selector)

    return elements[index].click()
  },
  [selector, index],
  value => {
    logger.debug('command', 'chromeClick() ✔')

    callback(value)
  })

  return this
}

module.exports = { logger }
