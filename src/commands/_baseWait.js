const { Utils, PeriodicPromise } = require('nightwatch/lib/utils')
const { extractWebdriverId } = require('../utils/element.utils.js')
const logger = require('../utils/logger.js')
const { removeSpacesAndSemicolons } = require('../utils/step.utils.js')
const Base = require('./_base.js')

/*!
 * Base class for custom commands that wait the element.
 *
 * @constructor
 */
class BaseWait extends Base {
  constructor ({ nightwatchInstance, commandName, args } = {}) {
    super({ nightwatchInstance, commandName, args })
    this.ms = this.setMilliseconds()
    this.setRescheduleInterval()
    this.setupExecutor()
    this.setupActions()
  }

  setRescheduleInterval (intervalMs = this.api.globals.waitForConditionPollInterval || 500) {
    Utils.enforceType(intervalMs, Utils.PrimitiveTypes.NUMBER)
    this.rescheduleIntervalMs = intervalMs

    return this
  }

  setMilliseconds (timeout) {
    if (timeout && typeof timeout === 'number') {
      return timeout
    }

    const globalTimeout = this.api.globals.waitForConditionTimeout

    if (typeof globalTimeout !== 'number') {
      throw new Error(
        'waitForElement expects second parameter to have a global default (waitForConditionTimeout) ' +
                    'to be specified if not passed as the second parameter'
      )
    }

    return globalTimeout
  }

  setupExecutor () {
    logger.debug('command', `${this.log[0]} BEGIN >>`)

    this.executor = new PeriodicPromise({
      rescheduleInterval: this.rescheduleInterval,
      timeout: this.ms
    })
  }

  command () {
    return this.executor.run()
  }

  setupActions () {
    const validate = (result) => this.isResultSuccess(result)
    const successHandler = (result) => this.elementFound(result)

    this.executor.queueAction({
      action: async () => await this.findElement(),
      retryOnSuccess: this.retryOnSuccess,
      retryOnFailure: !this.retryOnSuccess,
      validate,
      successHandler,
      errorHandler: (err) => {
        if (err.name !== 'TimeoutError') {
          throw err
        }

        const { response } = err

        if (response && response.error instanceof Error) {
          if (response.error.name === 'NoSuchElementError') {
            return this.elementNotFound(response)
          }

          this.reporter.registerTestError(response.error)

          return this.elementNotFound(response)
        }

        if (response && response.value) {
          return this.elementFound(response)
        }

        return this.elementNotFound(response)
      }
    })
  }

  async findElementWithText (result) {
    result.WebdriverElementId = null
    result.extractedValue = extractWebdriverId(result)
    const { text, isEquals, formatting, index } = this

    for (const elementId of result.extractedValue) {
      const { value } = await this.transportActions.getElementText(elementId)

      if (!value || !text) continue

      let textFound = `${value}`
      let textReceived = `${text}`

      if (formatting) {
        textFound = removeSpacesAndSemicolons(textFound)
        textReceived = removeSpacesAndSemicolons(textReceived)
      }

      logger.debug('command', `${this.log[1]}`, `Text received: ${textReceived}`)
      logger.debug('command', `${this.log[1]}`, `Text found: ${textFound}`)

      const isExpectedText = isEquals
        ? textFound === textReceived
        : textFound.includes(textReceived)

      if (!isExpectedText) continue

      const isElementAlreadyFound = this.elementsWithText.includes(elementId)

      if (!isElementAlreadyFound) {
        this.elementsWithText.push(elementId)
      }

      const indexResult = this.typeIndex
        ? (this.elementsWithText[index])
        : (result.extractedValue[index])

      const expectedValidation = this.quantity
        ? (this.quantity === this.elementsWithText.length)
        : indexResult

      if (!expectedValidation) continue

      result.elementsWithText = this.elementsWithText
      result.WebdriverElementId = this.elementsWithText[index]

      break
    }

    return result
  }

  async findElementWithValue (result) {
    result.WebdriverElementId = null
    result.extractedValue = extractWebdriverId(result)
    const { value, isEquals, formatting, index } = this

    for (const elementId of result.extractedValue) {
      const elementValue = await this.transportActions
        .getElementValue(elementId, 'value')

      if (!elementValue.value || !this.value) continue

      let valueFound = `${elementValue.value}`
      let valueReceived = `${value}`

      if (formatting) {
        valueFound = removeSpacesAndSemicolons(valueFound)
        valueReceived = removeSpacesAndSemicolons(valueReceived)
      }

      logger.debug('command', `${this.log[1]}`, `Value received: ${valueReceived}`)
      logger.debug('command', `${this.log[1]}`, `Value found: ${valueFound}`)

      const isExpectedValue = isEquals
        ? valueFound === valueReceived
        : valueFound.includes(valueReceived)

      if (!isExpectedValue) continue

      const isElementAlreadyFound = this.elementsWithValue.includes(elementId)

      if (!isElementAlreadyFound) {
        this.elementsWithValue.push(elementId)
      }

      const indexResult = this.typeIndex
        ? (this.elementsWithValue[index])
        : (result.extractedValue[index])

      const expectedValidation = this.quantity
        ? (this.quantity === this.elementsWithValue.length)
        : indexResult

      if (!expectedValidation) continue

      result.elementsWithValue = this.elementsWithValue
      result.WebdriverElementId = indexResult

      break
    }

    return result
  }
}

module.exports = { BaseWait }
