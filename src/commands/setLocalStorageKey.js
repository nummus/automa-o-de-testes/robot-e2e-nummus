const logger = require('../utils/logger.js')

/**
 * @global
 */

/**
 * @name setLocalStorageKey()
 *
 * @description
 *
 * 1. Set a browser local storage key and value.
 *
 * @param {string} key key name to set in the local storage
 * @param {string} value key value to set in the local storage
 *
 * @example
 * await browser.setLocalStorageKey('teste', true)
 *
 * await browser.setLocalStorageKey('teste', true, callback = () => {})
 */

export const command = function (key, value, callback = () => { }) {
  logger.debug('command', 'setLocalStorageKey()', `Key sent: ${key}`)

  this.execute((key, value) => {
    return window.localStorage.setItem(`${key}`, `${value}`)
  },
  [key, value],
  () => {
    callback()
  })

  return this
}

module.exports = { command }
