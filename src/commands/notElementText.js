const chalk = require('chalk')
const Utils = require('nightwatch/lib/utils')
const { extractWebdriverId } = require('../utils/element.utils.js')
const logger = require('../utils/logger.js')
const { removeSpacesAndSemicolons } = require('../utils/step.utils.js')
const BaseAssert = require('./_baseAssert.js')

/**
 * @global
 */

/**
 * @name notElementText()
 *
 * @description
 *
 * 1. Assert if an element with the text is present in the page.
 *
 * @param {string} selector The selector (CSS) used to locate the element.
 * @param {string} text The text of the element that will be searched
 * @param {number} index Index of the element in the page
 * @param {boolean} isEquals true for NON PARTIAL text validation or false for PARTIAL
 * @param {boolean} abortOnFailure true to fail the test if element is no found or false if you wish for the test to continue.
 * @param {boolean} formatting true to ENABLE formatting or false do DISABLE formatting in text comparison
 *
 * @example
 * await browser.notElementText('label', 'Salvar')
 * await browser.notElementText('label', 'Salvar', callback = () => {})
 *
 * Using options:
 *
 * await browser.notElementText({
 *                  selector: 'label',
 *                  text: 'Salvar',
 *                  index: 1
 *               })
 *
 * await browser.notElementText({
 *                  selector: 'label',
 *                  text: 'Salvar',
 *                  index: 1
 *               }, , callback = () => {})
 */
class notElementText extends BaseAssert {
  setArguments () {
    const firstArgIsObject = Utils.isObject(this.args[0]) && Utils.isUndefined(this.args[0].name)
    const hasValidNoObjectArgs = this.args.length === 2 || this.args.length === 3

    if (firstArgIsObject) {
      const optionsReceived = this.args.shift()
      this.options = JSON.stringify(optionsReceived)

      const {
        selector, text, index, abortOnFailure, isEquals, formatting
      } = optionsReceived

      if (!Utils.isUndefined(selector)) this.selector = selector
      if (!Utils.isUndefined(text)) this.text = text
      if (!Utils.isUndefined(index)) this.index = parseInt(index)
      if (!Utils.isUndefined(isEquals)) this.isEquals = isEquals
      if (!Utils.isUndefined(formatting)) this.formatting = formatting

      if (!Utils.isUndefined(abortOnFailure)) this.abortOnFailure = abortOnFailure
    } else if (!firstArgIsObject && hasValidNoObjectArgs) {
      this.selector = this.args.shift()
      this.text = this.args.shift()
    } else {
      const error = `"${this.commandName}" method expects a css selector and a element (or a object).`

      console.error(chalk.red(error))
    }

    this.setOutputMessage()
    this.setCallback()

    return this
  }

  async findElement () {
    const { element, commandName } = this

    try {
      const result = await this.elementLocator.findElement({
        element, commandName, returnSingleElement: false, cacheElementId: false
      })

      if (result.error) throw result

      result.WebdriverElementId = null
      result.extractedValue = extractWebdriverId(result)
      const { text, isEquals, formatting, index } = this

      for (const elementId of result.extractedValue) {
        const elementText = await this.transportActions.getElementText(elementId)

        if (!elementText.value || !text) continue

        let textFound = `${elementText.value}`
        let textReceived = `${text}`

        if (formatting) {
          textFound = removeSpacesAndSemicolons(textFound)
          textReceived = removeSpacesAndSemicolons(textReceived)
        }

        logger.debug('command', `${this.log[1]}`, `Text received: ${textReceived}`)
        logger.debug('command', `${this.log[1]}`, `Text found: ${textFound}`)

        const isExpectedText = isEquals
          ? textFound === textReceived
          : textFound.includes(textReceived)

        if (!isExpectedText) continue

        this.elementsWithText.push(elementId)

        if (!this.elementsWithText[index]) continue

        result.elementsWithText = this.elementsWithText
        result.WebdriverElementId = this.elementsWithText[index]

        break
      }

      if (!result.WebdriverElementId) return result

      throw this.noSuchElementError(result)
    } catch (error) {
      if (error.name === 'ReferenceError' || error.name === 'TypeError') throw error

      throw this.noSuchElementError(error)
    }
  }

  elementFound (result) {
    let message = `Element <%s> with text "${this.text}"`

    if (this.index) message += ` in index ${this.index}`

    message += ' was not present'

    return this.pass(result, message)
  }

  elementNotFound (result) {
    let message = this.abortOnFailure
      ? chalk.red('Located the element')
      : chalk.green('Located the element')
    message += chalk.magenta(' <%s> ')

    if (this.index) {
      message += this.abortOnFailure
        ? chalk.red('in index ')
        : chalk.green('in index ')

      message += `${this.index} `
    }

    message += this.abortOnFailure
      ? chalk.red('\nwith text ')
      : chalk.green('with text ')

    message += chalk.blue(`"${this.text}" `)

    message += this.abortOnFailure
      ? chalk.red('\nElement located, please verify parameters and your system behavior.\n')
      : ''

    return this.fail(result, `not ${this.expectedValue}`, this.expectedValue, message)
  }
}

module.exports = { notElementText }
