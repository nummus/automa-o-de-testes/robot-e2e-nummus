const chalk = require('chalk')
const Element = require('nightwatch/lib/element')
const Utils = require('nightwatch/lib/utils')
const { extractWebdriverId } = require('../utils/element.utils.js')
const logger = require('../utils/logger.js')
const { removeSpacesAndSemicolons } = require('../utils/step.utils.js')
const BaseWait = require('./_baseWait.js')

/**
 * @global
 */

/**
 * @name getIdByParentText()
 *
 * @description
 *
 * 1. Waits a given time in milliseconds (default 5000ms) for an element inside parent with the text
 * to be present in the page before performing any other commands or assertions.
 *
 * @param {string} sonSelector Name of an object property in Page.Js where the element's css selector is stored
 * @param {string} selector The selector (CSS) used to locate the element.
 * @param {string} text Text of the element that will be searched
 * @param {number} index Index of the element in the page
 * @param {boolean} isEquals true for NON PARTIAL text validation or false for PARTIAL
 * @param {boolean} abortOnFailure true to fail the test if element is no found or false if you wish for the test to continue.
 * @param {number} timeout The total number of milliseconds to wait before failing
 * @param {number} retryInterval The total number of milliseconds to wait before retry the command if it fails
 * @param {boolean} formatting true to ENABLE formatting or false do DISABLE formatting in text comparison
 *
 * @example
 * await browser.getIdByParentText('button', 'list, 'Teste', result () => {
 *     console.log(result)
 * })
 *
 * Using options:
 *
 * await browser.getIdByParentText({
 *                  sonSelector: 'button',
 *                  selector: 'list',
 *                  text: 'Teste',
 *                  index: 1
 *               })
 *
 * await browser.getIdByParentText({
 *                  sonSelector: 'button',
 *                  selector: 'list',
 *                  text: 'Teste',
 *                  index: 1
 *               } , callback = () => {})
 */
class getIdByParentText extends BaseWait {
  constructor ({ nightwatchInstance, commandName, args } = {}) {
    super({ nightwatchInstance, commandName, args })
    this.createParentElement()
  }

  createParentElement () {
    this.sonElement = Element.createFromSelector(this.sonSelector, this.strategy)

    return this
  }

  setArguments () {
    const firstArgIsObject =
      Utils.isObject(this.args[0]) &&
      Utils.isUndefined(this.args[0].name)

    const hasValidNoObjectArgs = this.args.length === 3 || this.args.length === 4

    if (firstArgIsObject) {
      const optionsReceived = this.args.shift()
      this.options = JSON.stringify(optionsReceived)

      const {
        sonSelector, selector, text, index, timeout,
        retryInterval, message, abortOnFailure, isEquals,
        formatting, locateStrategy, suppressNotFoundErrors, retryAction
      } = optionsReceived

      if (message) this.message = message
      if (!Utils.isUndefined(selector)) this.selector = selector
      if (!Utils.isUndefined(sonSelector)) this.sonSelector = sonSelector
      if (!Utils.isUndefined(text)) this.text = text
      if (!Utils.isUndefined(index)) this.index = parseInt(index)
      if (!Utils.isUndefined(timeout)) this.ms = this.setMilliseconds(timeout)
      if (!Utils.isUndefined(isEquals)) this.isEquals = isEquals
      if (!Utils.isUndefined(formatting)) this.formatting = formatting

      if (!Utils.isUndefined(retryInterval)) this.setRescheduleInterval(retryInterval)
      if (!Utils.isUndefined(abortOnFailure)) this.abortOnFailure = abortOnFailure
      if (locateStrategy) this.setStrategy(locateStrategy)
      if (!Utils.isUndefined(suppressNotFoundErrors)) this.suppressNotFoundErrors = suppressNotFoundErrors
      if (!Utils.isUndefined(retryAction)) this.retryOnFailure = retryAction
    } else if (!firstArgIsObject && hasValidNoObjectArgs) {
      this.sonSelector = this.args.shift()
      this.selector = this.args.shift()
      this.text = this.args.shift()
    } else {
      const error = `"${this.commandName}" method expects a css selector and a element (or a object).`

      console.error(chalk.red(error))
    }

    this.setOutputMessage()
    this.setCallback()

    return this
  }

  async findElement () {
    const { element, sonElement, commandName } = this

    try {
      const result = await this.elementLocator.findElement({
        element, commandName, returnSingleElement: false, cacheElementId: false
      })

      if (result.error) throw result

      result.WebdriverElementId = null
      result.extractedValue = extractWebdriverId(result)
      const { text, formatting, index } = this

      for (const elementId of result.extractedValue) {
        const { value } = await this.transportActions.getElementText(elementId)

        if (!value || !text) continue

        let textFound = `${value}`
        let textReceived = `${text}`

        if (formatting) {
          textFound = removeSpacesAndSemicolons(textFound)
          textReceived = removeSpacesAndSemicolons(textReceived)
        }

        logger.debug('command', `${this.log[1]}`, `Text received: ${textReceived}`)
        logger.debug('command', `${this.log[1]}`, `Text found: ${textFound}`)

        if (!textFound.includes(textReceived)) continue

        this.elementsWithText.push(elementId)

        if (!this.elementsWithText[index]) continue

        result.elementsWithText = this.elementsWithText
        result.WebdriverElementId = this.elementsWithText[index]

        break
      }

      const sonResult = await this.transportActions
        .locateMultipleElementsByElementId({
          id: result.WebdriverElementId,
          using: 'css selector',
          value: sonElement.selector
        })

      if (!result.WebdriverElementId || !sonResult.value) {
        throw this.noSuchElementError(result)
      }

      const sonsExtractedValue = extractWebdriverId(sonResult)

      result.value = sonsExtractedValue
      result.sonExtractedValue = sonsExtractedValue[0]

      return result
    } catch (error) {
      if (error.name === 'ReferenceError' || error.name === 'TypeError') throw error

      throw this.noSuchElementError(error)
    }
  }

  elementFound (result) {
    let message = `Get the element "${this.sonSelector}"`

    message += ` in the parent <%s> with text "${this.text}"`

    if (this.index) message += ` in index ${this.index}`

    message += ` after ${this.executor.elapsedTime} milliseconds.`

    return this.pass(result, message)
  }

  elementNotFound (result) {
    let message = this.abortOnFailure
      ? chalk.red('Timed out while waiting for element')
      : chalk.green('Timed out while waiting for element')

    message += chalk.magenta(` "${this.sonSelector}" `)

    message += 'in the parent <%s> '

    if (this.index) {
      message += this.abortOnFailure
        ? chalk.red('on index ')
        : chalk.green('on index ')

      message += `${this.index} `
    }

    message += this.abortOnFailure
      ? chalk.red('\nwith text ')
      : chalk.green('with text ')

    message += chalk.blue(`"${this.text}" `)

    message += chalk.red(`to be present for ${this.ms} milliseconds.`)

    message += this.abortOnFailure
      ? chalk.red('\nElement can not be located, please verify parameters and your system behavior.\n')
      : ''

    return this.fail(result, `not ${this.expectedValue}`, this.expectedValue, message)
  }
}

module.exports = { getIdByParentText }
