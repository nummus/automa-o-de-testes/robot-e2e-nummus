const logger = require('../utils/logger.js')

/**
 * @global
 */

/**
 * @name removeLocalStorageKey()
 *
 * @description
 *
 * 1. Remove a browser local storage key with given name.
 *
 * @param {string} key key name to remove in the local storage
 *
 * @example
 * await browser.removeLocalStorageKey('teste')
 *
 * await browser.removeLocalStorageKey('teste', callback = () => {})
 */

const command = function (key, callback = () => { }) {
  logger.debug('command', 'removeLocalStorageKey()', `Key obtained: ${key}`)

  this.execute((key) => {
    return window.localStorage.removeItem(`${key}`)
  },
  [key],
  () => {
    callback()
  })

  return this
}

module.exports = { command }
