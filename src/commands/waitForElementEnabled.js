const chalk = require('chalk')
const Utils = require('nightwatch/lib/utils')
const { extractWebdriverId } = require('../utils/element.utils.js')
const logger = require('../utils/logger.js')
const BaseWait = require('./_baseWait.js')

/**
 * @global
 */

/**
 * @name waitForElementEnabled()
 *
 * @description
 *
 * 1. Waits a given time in milliseconds (default 5000ms) for an element enabled to be present in the page.
 *
 * @param {string} selector The selector (CSS) used to locate the element.
 * @param {number} index Index of the element in the page
 * @param {boolean} abortOnFailure true to fail the test if element is no found or false if you wish for the test to continue.
 * @param {number} timeout The total number of milliseconds to wait before failing
 * @param {number} retryInterval The total number of milliseconds to wait before retry the command if it fails
 *
 * @example
 * await browser.waitForElementEnabled('label')
 * await browser.waitForElementEnabled('label', callback = () => {})
 *
 * Using options:
 *
 * await browser.waitForElementEnabled({
 *                  selector: 'label',
 *                  index: 1
 *               })
 *
 * await browser.waitForElementEnabled({
 *                  selector: 'label',
 *                  index: 1
 *               }, , callback = () => {})
 */
class waitForElementEnabled extends BaseWait {
  setArguments () {
    const firstArgIsObject =
      Utils.isObject(this.args[0]) &&
      Utils.isUndefined(this.args[0].name)

    const hasValidNoObjectArgs = this.args.length === 1

    if (firstArgIsObject) {
      const optionsReceived = this.args.shift()
      this.options = JSON.stringify(optionsReceived)

      const {
        selector, index, timeout, retryInterval, message,
        abortOnFailure, locateStrategy, suppressNotFoundErrors, retryAction
      } = optionsReceived

      if (message) this.message = message
      if (!Utils.isUndefined(selector)) this.selector = selector
      if (!Utils.isUndefined(index)) this.index = parseInt(index)

      if (!Utils.isUndefined(timeout)) this.ms = this.setMilliseconds(timeout)
      if (!Utils.isUndefined(retryInterval)) this.setRescheduleInterval(retryInterval)
      if (!Utils.isUndefined(abortOnFailure)) this.abortOnFailure = abortOnFailure
      if (locateStrategy) this.setStrategy(locateStrategy)
      if (!Utils.isUndefined(suppressNotFoundErrors)) this.suppressNotFoundErrors = suppressNotFoundErrors
      if (!Utils.isUndefined(retryAction)) this.retryOnFailure = retryAction
    } else if (!firstArgIsObject && hasValidNoObjectArgs) {
      this.selector = this.args.shift()
    } else {
      const error = `"${this.commandName}" method expects a css selector and a element (or a object).`

      console.error(chalk.red(error))
    }

    this.setOutputMessage()
    this.setCallback()

    return this
  }

  async findElement () {
    const { element, commandName } = this

    let result
    const elementWithState = []

    try {
      result = await this.elementLocator.findElement({
        element, commandName, returnSingleElement: false, cacheElementId: false
      })

      if (result.error) throw result

      result.WebdriverElementId = null
      result.extractedValue = extractWebdriverId(result)

      for (let index = 0; index < result.extractedValue.length; index++) {
        if (this.index !== index) continue

        await this.transportActions.isElementDisplayed(result.extractedValue[index])

        result.WebdriverElementId = elementWithState[this.index]

        break
      }

      if (!result.WebdriverElementId) {
        throw this.noSuchElementError(result)
      }

      logger.debug('command', `${this.log[1]} ✔`)

      return result
    } catch (error) {
      if (error.name === 'ReferenceError' || error.name === 'TypeError') throw error

      throw this.noSuchElementError(error)
    }
  }

  elementFound (result) {
    let message = 'Element <%s>'

    if (this.index) message += ` in index ${this.index}`

    message += ` was present after ${this.executor.elapsedTime} milliseconds.`

    return this.pass(result, message)
  }

  elementNotFound (result) {
    let message = this.abortOnFailure
      ? chalk.red('Timed out while waiting for element')
      : chalk.green('Timed out while waiting for element')

    message += chalk.magenta(' <%s> ')

    if (this.index) {
      message += this.abortOnFailure
        ? chalk.red('in index ')
        : chalk.green('in index ')

      message += `${this.index} `
    }

    message += chalk.red(`to be present for ${this.ms} milliseconds.`)
    message += this.abortOnFailure
      ? chalk.red('\nElement can not be located, please verify parameters and your system behavior.\n')
      : ''

    return this.fail(result, `not ${this.expectedValue}`, this.expectedValue, message)
  }
}

module.exports = { waitForElementEnabled }
