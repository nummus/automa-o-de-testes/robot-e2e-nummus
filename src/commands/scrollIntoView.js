const logger = require('../utils/logger.js')

/**
 * @global
 */

/**
 * @name scrollIntoView()
 *
 * @description
 *
 * 1. Scroll into view a element present in the page.
 *
 * @param {string} selector The selector (CSS) used to locate the element.
 * @param {number} index Index of the element in the page
 *
 * @example
 * await browser.scrollIntoView('label')
 *
 * await browser.scrollIntoView('label', 2)
 *
 * await browser.scrollIntoView('label', 2, callback = () => {})
 */

const command = function (selector, index = 0, callback = () => { }) {
  this.execute((selector, index) => {
    const elements = document.querySelectorAll(selector)

    return elements[index].scrollIntoView()
  },
  [selector, index],
  value => {
    logger.debug('command', 'scrollIntoView() ✔')

    callback(value)
  })

  return this
}

module.exports = { command }
