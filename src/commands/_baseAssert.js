import logger from '../utils/logger.js'
import Base from './_base.js'

/*!
 * Base class for custom commands that not wait the element.
 *
 * @constructor
 */
class BaseAssert extends Base {
  command () {
    return this.action()
  }

  async action () {
    logger.debug('command', `${this.log[0]} BEGIN >>`)

    try {
      const response = await this.findElement()

      return this.elementFound(response)
    } catch (error) {
      return this.elementNotFound(error)
    }
  }
}

module.exports = { BaseAssert }
