const context = require('../context.js')
const logger = require('../utils/logger.js')

/**
 * @global
 */

/**
 * @name getBearerAuthToken()
 *
 * @description
 *
 * 1. Get in the browser local storage the X-AUTH-TOKEN and set in bearerTokenHash context.
 *
 * @example
 * await browser.getBearerAuthToken()
 *
 * await browser.getBearerAuthToken(callback = () => {})
 */

const command = function (callback = () => {}) {
  this.execute(() => {
    return window.localStorage['@Nummus:token']
  },
  [],
  ({ value }) => {
    context.set('bearerTokenHash', value)

    logger.debug('command', 'Token encontrado', value)
    logger.info('action', 'Token capturado com sucesso')

    callback(value)
  })

  return this
}

module.exports = { command }
