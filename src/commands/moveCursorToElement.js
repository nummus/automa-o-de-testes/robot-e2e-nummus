const chalk = require('chalk')
const Utils = require('nightwatch/lib/utils')
const { extractWebdriverId } = require('../utils/element.utils.js')
const logger = require('../utils/logger.js')
const BaseWait = require('./_baseWait.js')

/**
 * @global
 */

/**
 * @name moveCursorToElement()
 *
 * @description
 *
 * 1. Waits a given time in milliseconds (default 5000ms) for an element to move the cursor in the middle of it.
 *
 * @param {string} selector The selector (CSS) used to locate the element
 * @param {number} index Index of the element in the page
 * @param {number} x Offset to move to, relative to the center of the element
 * @param {number} y Offset to move to, relative to the center of the element
 * @param {boolean} abortOnFailure true to fail the test if element is no found or false if you wish for the test to continue
 * @param {number} timeout The total number of milliseconds to wait before failing
 * @param {number} retryInterval The total number of milliseconds to wait before retry the command if it fails
 *
 * @example
 * await browser.moveCursorToElement('label')
 * await browser.moveCursorToElement('label', 0, 0)
 * await browser.moveCursorToElement('label', 0, 0, callback = () => {})
 *
 * Using options:
 *
 * await browser.moveCursorToElement({
 *                  selector: 'label',
 *                  x: 1,
 *                  y: 1
 *               })
 *
 * await browser.moveCursorToElement({
 *                  selector: 'label',
 *                  x: 1,
 *                  y: 1,
 *                  index: 1
 *               }, callback = () => {})
 */
class moveCursorToElement extends BaseWait {
  setArguments () {
    const firstArgIsObject =
      Utils.isObject(this.args[0]) &&
      Utils.isUndefined(this.args[0].name)

    const hasValidNoObjectArgs = this.args.length === 1 || this.args.length === 3

    if (firstArgIsObject) {
      const optionsReceived = this.args.shift()
      this.options = JSON.stringify(optionsReceived)

      const {
        selector, y, x, index, timeout, retryInterval, message,
        abortOnFailure, locateStrategy, suppressNotFoundErrors, retryAction
      } = optionsReceived

      if (message) this.message = message
      if (!Utils.isUndefined(x)) this.x = parseInt(x)
      if (!Utils.isUndefined(y)) this.y = parseInt(y)
      if (!Utils.isUndefined(selector)) this.selector = selector
      if (!Utils.isUndefined(index)) this.index = parseInt(index)

      if (locateStrategy) this.setStrategy(locateStrategy)
      if (!Utils.isUndefined(timeout)) this.ms = this.setMilliseconds(timeout)
      if (!Utils.isUndefined(retryInterval)) this.setRescheduleInterval(retryInterval)
      if (!Utils.isUndefined(abortOnFailure)) this.abortOnFailure = abortOnFailure
      if (!Utils.isUndefined(suppressNotFoundErrors)) this.suppressNotFoundErrors = suppressNotFoundErrors
      if (!Utils.isUndefined(retryAction)) this.retryOnFailure = retryAction
    } else if (!firstArgIsObject && hasValidNoObjectArgs) {
      this.selector = this.args.shift()

      if (this.args.length > 1) {
        this.x = this.args.shift()
        this.y = this.args.shift()
      }
    } else {
      const error = `"${this.commandName}" method expects a css selector and a element (or a object).`

      console.error(chalk.red(error))
    }

    this.setOutputMessage()
    this.setCallback()

    return this
  }

  async findElement () {
    const { element, commandName } = this

    let result

    try {
      result = await this.elementLocator.findElement({
        element, commandName, returnSingleElement: false, cacheElementId: false
      })

      if (result.error) throw result

      result.WebdriverElementId = null
      result.extractedValue = extractWebdriverId(result)

      for (let index = 0; index < result.extractedValue.length; index++) {
        if (this.index !== index) continue

        result.WebdriverElementId = result.extractedValue[index]

        break
      }

      if (!result.WebdriverElementId) {
        throw this.noSuchElementError(result)
      }

      await this.transportActions.moveTo(result.WebdriverElementId, this.x, this.y)

      logger.debug('command', `${this.log[1]} ✔`)

      return result
    } catch (error) {
      if (error.name === 'ReferenceError' || error.name === 'TypeError') throw error

      throw this.noSuchElementError(error)
    }
  }

  elementFound (result) {
    let message = 'Moved the cursor to element <%s>'

    if (this.index) message += ` in index ${this.index}`

    if (this.x || this.y) message += ` with coordinates x ${this.x}, y ${this.y}`

    message += ` after ${this.executor.elapsedTime} milliseconds.`

    return this.pass(result, message)
  }

  elementNotFound (result) {
    let message = this.abortOnFailure
      ? chalk.red('Timed out while waiting for element')
      : chalk.green('Timed out while waiting for element')

    message += chalk.magenta(' <%s> ')

    if (this.index) {
      message += this.abortOnFailure
        ? chalk.red('in index ')
        : chalk.green('in index ')

      message += `${this.index} `
    }

    if (this.x || this.y) message += ` with coordinates x ${this.x}, y ${this.y} `

    message += chalk.red(`to move the cursor for ${this.ms} milliseconds.`)

    message += this.abortOnFailure
      ? chalk.red('\nElement can not be located, please verify parameters and your system behavior.\n')
      : ''

    return this.fail(result, `not ${this.expectedValue}`, this.expectedValue, message)
  }
}

module.exports = { moveCursorToElement }
