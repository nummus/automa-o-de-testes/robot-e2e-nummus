import EventEmitter from 'events'
import AssertionRunner from 'nightwatch/lib/assertion/assertion-runner'
import Element from 'nightwatch/lib/element'
import Utils, { LocateStrategy } from 'nightwatch/lib/utils'
import logger from '../utils/logger.js'
const { NoSuchElementError } = require('nightwatch/lib/element/locator')

/*!
 * Base class for custom commands.
 *
 * @constructor
 */
class Base extends EventEmitter {
  constructor ({ nightwatchInstance, commandName, args } = {}) {
    super()
    // custom commands don't get the nightwatchInstance
    this.nightwatchInstance = nightwatchInstance || this.client
    this.commandName = commandName || this.commandFileName
    this.transport.registerLastError(null)
    this.setStrategy()
    this.x = 0
    this.y = 0
    this.index = 0
    this.typeIndex = true
    this.isFocus = false
    this.isAutocomplete = false
    this.formatting = true
    this.isEquals = true
    this.logText = []
    this.elementsWithText = []
    this.logValue = []
    this.elementsWithValue = []
    this.options = 'No options'
    this.expectedValue = 'found'
    this.abortOnFailure = this.api.globals.abortOnAssertionFailure
    this.throwOnMultipleElementsReturned = this.api.globals.throwOnMultipleElementsReturned
    this.args = (Array.isArray(args) && args.slice(0)) || this.commandArgs || []
    this.setArguments()
    this.createElement()
    this.__log = [
      `${this.commandName}(${this.__options || `${this.element.__selector}, ${this.__text}`})`,
      `${this.commandName}()`
    ]
  }

  get x () { return this.__x }
  set x (value) { this.__x = value }

  get y () { return this.__y }
  set y (value) { this.__y = value }

  get log () { return this.__log }
  set log (value) { this.__log = value }

  get text () { return this.__text }
  set text (value) { this.__text = value }

  get value () { return this.__value }
  set value (value) { this.__value = value }

  get index () { return this.__index }
  set index (value) { this.__index = value }

  get typeIndex () { return this.__typeIndex }
  set typeIndex (value) { this.__typeIndex = value }

  get attribute () { return this.__attribute }
  set attribute (value) { this.__attribute = value }

  get options () { return this.__options }
  set options (value) { this.__options = value }

  get page () { return this.__page }
  set page (value) { this.__page = value }

  get selector () { return this.__selector }
  set selector (value) { this.__selector = value }

  get element () { return this.__element }
  set element (value) { this.__element = value }

  get sonElement () { return this.__sonElement }
  set sonElement (value) { this.__sonElement = value }

  get isFocus () { return this.__isFocus }
  set isFocus (value) { this.__isFocus = value }

  get quantity () { return this.__quantity }
  set quantity (value) { this.__quantity = value }

  get isAutocomplete () { return this.__isAutocomplete }
  set isAutocomplete (value) { this.__isAutocomplete = value }

  get sonPage () { return this.__sonPage }
  set sonPage (value) { this.__sonPage = value }

  get sonSelector () { return this.__sonSelector }
  set sonSelector (value) { this.__sonSelector = value }

  get formatting () { return this.__formatting }
  set formatting (value) { this.__formatting = value }

  get commandName () { return this.__commandName }
  set commandName (value) { this.__commandName = value }

  get isEquals () { return this.__isEquals }
  set isEquals (value) { this.__isEquals = value }

  get logText () { return this.__logText }
  set logText (value) { this.__logText = value }

  get logValue () { return this.__logValue }
  set logValue (value) { this.__logValue = value }

  get elementsWithText () { return this.__elementsWithText }
  set elementsWithText (value) { this.__elementsWithText = value }

  get elementsWithValue () { return this.__elementsWithValue }
  set elementsWithValue (value) { this.__elementsWithValue = value }

  get callback () { return this.__callback }
  set callback (value) { this.__callback = value }

  get strategy () { return this.__strategy }
  set strategy (value) { this.__strategy = value }

  get retryOnFailure () { return this.__retryOnFailure }
  set retryOnFailure (value) { this.__retryOnFailure = value }

  get rescheduleIntervalMs () { return this.__rescheduleIntervalMs }
  set rescheduleIntervalMs (value) { this.__rescheduleIntervalMs = value }

  get retryOnSuccess () { return false }
  get transport () { return this.nightwatchInstance.transport }
  get rescheduleInterval () { return this.__rescheduleInterval }
  get elementLocator () { return this.nightwatchInstance.elementLocator }
  get defaultUsing () { return this.nightwatchInstance.locateStrategy || LocateStrategy.getDefault() }

  createElement () {
    this.element = Element
      .createFromSelector(this.selector, this.strategy)

    if (!this.isFocus) return this

    this.element.__selector = this.isAutocomplete
      ? `${this.element.__selector} input:focus`
      : `${this.element.__selector}:focus`

    return this
  }

  setStrategy (val = this.defaultUsing) {
    this.strategy = val

    return this
  }

  setOutputMessage () {
    this.message = null

    if (this.args.length > 0 && Utils.isString(this.args[this.args.length - 1])) {
      this.message = this.args.pop()
    }

    return this
  }

  setCallback () {
    let callback = null

    if (this.args.length && Utils.isFunction(this.args[this.args.length - 1])) {
      callback = this.args.pop()
    }

    this.callback = callback || function () {}

    return this
  }

  noSuchElementError (err) {
    const { element, ms, abortOnFailure } = this
    const { retries } = err

    return new NoSuchElementError({ element, ms, abortOnFailure, retries })
  }

  isResultSuccess (result) {
    return this.transport.isResultSuccess(result) && result.value !== null
  }

  formatMessage (defaultMsg) {
    return Utils.format(this.message || defaultMsg, this.element.__selector)
  }

  pass (result, defaultMsg) {
    this.message = this.formatMessage(defaultMsg)

    return this.assert({
      result,
      passed: true,
      err: { expected: this.expectedValue }
    })
  }

  fail (result, actual, expected, defaultMsg) {
    this.message = this.formatMessage(defaultMsg)

    return this.assert({
      result,
      passed: false,
      err: { actual, expected }
    })
  }

  assert ({ result, passed, err }) {
    this.elapsedTime = this.executor ? this.executor.elapsedTime : 0

    const { reporter } = this.client

    const { elapsedTime, message, abortOnFailure, stackTrace } = this

    const runner = new AssertionRunner({
      passed,
      err,
      message,
      abortOnFailure,
      stackTrace,
      reporter,
      elapsedTime
    })

    return runner
      .run(result)
      .catch((err) => err)
      .then(async (err) => {
        if (Utils.isObject(result.value) && !Array.isArray(result.value)) {
          result.value = [result.value]
        }

        if (err instanceof Error) {
          err.abortOnFailure = this.abortOnFailure
          err.waitFor = true

          return this.complete(err, result)
        }

        return this.complete(null, result)
      })
  }

  async complete (err, response) {
    try {
      await this.callback.call(this.api, response, this)

      logger.debug('command', `${this.__log[0]} END <<`)

      if (err instanceof Error) {
        this.emit('error', err)

        return this
      }

      this.emit('complete', response)

      return this
    } catch (cbErr) {
      this.emit('error', cbErr)

      return this
    }
  }
}

module.exports = { Base }
