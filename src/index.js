import properties from './config.js'
import context from './context.js'

import defaultResolver from './resolvers/defaultResolver.js'
import filterJson from './resolvers/filterJson.js'
import getAllTheIds from './resolvers/getAllTheIds.js'
import getContext from './resolvers/getContext.js'
import getFilePath from './resolvers/getFilePath.js'
import getId from './resolvers/getId.js'
import getIdPosition from './resolvers/getIdPosition.js'
import getJsonFileData from './resolvers/getJsonFileData.js'
import getJsonFileDataFilter from './resolvers/getJsonFileDataFilter.js'
import getProperties from './resolvers/getProperties.js'
import getTime from './resolvers/getTime.js'
import orderArrayBy from './resolvers/orderArrayBy.js'
import resolveJson from './resolvers/resolveJson.js'
import resolveJsonFilter from './resolvers/resolveJsonFilter.js'
import resolvePlainJson from './resolvers/resolvePlainJson.js'
import setDoubleQuotes from './resolvers/setDoubleQuotes.js'
import tunIntoNull from './resolvers/tunIntoNull.js'
import tunIntoArray from './resolvers/turnIntoArray.js'
import turnIntoInteger from './resolvers/turnIntoInteger.js'

import { clearOptionSelector, findByPropertyValue, loaderSelector, moveToElementWithText, pressLeftMouseButton, pressReleaseKey, releaseLeftMouseButton, selectOptionSelector, selectText, setAndSelectText, tryClickAndClickText, updateFieldWithMask, waitForLoadingNotPresent } from './utils/action.utils.js'
import { getConfig, getOperatingSystem, getProperty, ifIsJSONParse, ifIsObjectStringify, isBoolean, isFunction, isJSON, isNumber, isObject, isString, isSymbol, isUndefined, setOperatingSystemKeys } from './utils/base.utils.js'
import { countScenariosInFeature, initializeContext, shouldLockPool, tableDrivenToObjects, tableDrivenToQuery, updateCurrentScenarioData, updateFeatureAndScenarioData } from './utils/cucumber.utils.js'
import { extractWebdriverId, getPageSelector, getWebdriverIdByPageElement, getWebdriverIdBySonElementValue, normalizeElement, normalizeId, simplifyPage } from './utils/element.utils.js'
import logger from './utils/logger.js'
import { createDiR, deleteDiR, formatNanoseconds, robotErrorHandler } from './utils/report.utils.js'
import { executeResolver, getResolver, plainJsonResolver, resolveAllText, textResolver } from './utils/resolver.utils.js'
import { filterBuilder, readFileToFormData, resolveUrl } from './utils/service.utils.js'
import { checkElementText, currencyConverter, firstLetterUpperCase, removeSpacesAndSemicolons, replaceWithOsKey, returnElementPropertyValue, returnElementText, updateTextSelector } from './utils/step.utils.js'
import { addTime, parameterValidTimeType, parameterValidToAdd, parameterValidToSubtract, setAppointmentTime, setRepeatingAppointmentTime, subtractTime } from './utils/time.utils.js'

module.exports = {
  context,
  properties,
  utils: {
    action: {
      pressReleaseKey,
      clearOptionSelector,
      loaderSelector,
      moveToElementWithText,
      pressLeftMouseButton,
      releaseLeftMouseButton,
      selectOptionSelector,
      tryClickAndClickText,
      updateFieldWithMask,
      waitForLoadingNotPresent,
      findByPropertyValue,
      selectText,
      setAndSelectText
    },
    base: {
      getConfig,
      getOperatingSystem,
      getProperty,
      ifIsJSONParse,
      ifIsObjectStringify,
      isBoolean,
      isFunction,
      isJSON,
      isNumber,
      isObject,
      isString,
      isSymbol,
      isUndefined,
      setOperatingSystemKeys
    },
    cucumber: {
      countScenariosInFeature,
      initializeContext,
      shouldLockPool,
      tableDrivenToObjects,
      tableDrivenToQuery,
      updateCurrentScenarioData,
      updateFeatureAndScenarioData
    },
    element: {
      extractWebdriverId,
      getWebdriverIdByPageElement,
      getPageSelector,
      getWebdriverIdBySonElementValue,
      normalizeElement,
      normalizeId,
      simplifyPage
    },
    resolver: {
      executeResolver,
      getResolver,
      plainJsonResolver,
      resolveAllText,
      textResolver
    },
    report: {
      formatNanoseconds,
      createDiR,
      deleteDiR,
      robotErrorHandler
    },
    service: {
      filterBuilder,
      readFileToFormData,
      resolveUrl
    },
    step: {
      checkElementText,
      currencyConverter,
      firstLetterUpperCase,
      removeSpacesAndSemicolons,
      replaceWithOsKey,
      returnElementText,
      returnElementPropertyValue,
      updateTextSelector
    },
    time: {
      addTime,
      parameterValidTimeType,
      parameterValidToAdd,
      parameterValidToSubtract,
      setAppointmentTime,
      setRepeatingAppointmentTime,
      subtractTime
    },
    logger
  },
  resolvers: {
    defaultResolver,
    filterJson,
    getContext,
    getFilePath,
    getTime,
    getId,
    getAllTheIds,
    getProperties,
    getIdPosition,
    getJsonFileData,
    getJsonFileDataFilter,
    orderArrayBy,
    resolveJson,
    resolveJsonFilter,
    resolvePlainJson,
    setDoubleQuotes,
    tunIntoNull,
    tunIntoArray,
    turnIntoInteger
  }
}
