import { Before, BeforeAll, setDefaultTimeout } from '@cucumber/cucumber'
import context from '../context.js'
import { setOperatingSystemKeys, startTest } from '../utils/base.utils.js'
import {
  countScenariosInFeature, initializeContext,
  updateCurrentScenarioData, updateFeatureAndScenarioData
} from '../utils/cucumber.utils.js'

setDefaultTimeout(-1)

BeforeAll(async function () {
  try {
    await initializeContext()
  } catch (error) {
    throw new Error(`🔺 ~ BeforeAll Core ~ error ${error.message}`)
  }
})

Before(async function (scenario) {
  const { gherkinDocument, pickle } = scenario
  const feature = updateFeatureAndScenarioData(gherkinDocument, pickle)

  if (context.get('isNewFeature')) {
    const scenariosInFeature = countScenariosInFeature(feature)

    context.set('totalScenariosInFeature', scenariosInFeature)
    context.set('currentScenarioInFeature', 0)
  }

  const totalScenariosInScenario = updateCurrentScenarioData(feature, pickle)

  if (context.get('isNewScenario')) {
    context.set('currentScenarioNumber', 1)
    context.set('tags', pickle.tags.map(tag => tag.name))
  } else {
    context.set('currentScenarioNumber', context.get('currentScenarioNumber') + 1)
  }

  const currentTotalInFeature = context.get('currentScenarioInFeature') + 1

  context.set('currentScenarioInFeature', currentTotalInFeature)

  const currentScenarioNumber = context.get('currentScenarioNumber')

  context.set('isFirstScenario', currentScenarioNumber === 1)
  context.set('isLastScenario', currentScenarioNumber === totalScenariosInScenario)

  const totalScenariosInFeature = context.get('totalScenariosInFeature')
  const currentScenarioInFeature = context.get('currentScenarioInFeature')
  const featureLastScenario = currentScenarioInFeature === totalScenariosInFeature

  context.set('isFeatureLastScenario', featureLastScenario)

  if (!context.get('isNewFeature')) return

  const beginAutomation = async () => {
    await startTest(this, pickle)

    setOperatingSystemKeys()

    if (context.get('isCoreBeforeAllCalled')) return

    context.set('isCoreBeforeAllCalled', true)
  }

  try {
    await beginAutomation()
  } catch (error) {
    const chromeMessage = 'An error occurred while creating a new ChromeDriver session: [Error] Server terminated early with status 1'

    if (error.message === chromeMessage && !context.get('isCoreBeforeAllCalled')) {
      context.set('isCoreBeforeAllCalled', true)

      await beginAutomation()
    } else {
      throw new Error(`🔺 ~ Before Core ~ error ${error.message}`)
    }
  }
})
