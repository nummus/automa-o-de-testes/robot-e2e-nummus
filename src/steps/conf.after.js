import { After, AfterAll, AfterStep } from '@cucumber/cucumber'
import reporter from 'cucumber-html-reporter'
import { join, resolve } from 'path'
import { properties } from '../config.js'
import context from '../context.js'
import logger from '../utils/logger.js'

const env = process.env.NODE_ENV || 'development'
const chromedriver = require('chromedriver')
const robotConfig = require(resolve('./nummusbot.conf.js'))
const reportsPath = join(resolve(robotConfig.reportsPath))
const packageJson = require(resolve('./package.json'))

AfterStep(async function ({ result, pickleStep }) {
  const isFailedStep = result.status === 'FAILED'

  if (!isFailedStep || !browser) return

  await browser
    .saveScreenshot(`./reports/screenshots/steps/${pickleStep.id}.png`)
    .then(screenshot => this.attach(screenshot, 'image/png'))
})

After(async function ({ result, pickle }) {
  if (!context.get('isFeatureLastScenario')) return

  context.set('isFeatureLastScenario', false)

  const isFailedFeature = result.status === 'FAILED'

  if (!browser) return

  if (isFailedFeature) {
    context.set('isFailedFeature', isFailedFeature)

    await browser
      .saveScreenshot(`./reports/screenshots/feature/${pickle.id}.png`)
      .then(screenshot => this.attach(screenshot, 'image/png'))
  }

  if (this.client && result) {
    const error = result.status === 'FAILED' ? new Error(result.message) : null

    await this.client.transport.testSuiteFinished(error)
  }

  logger.info('action', 'Quitting Google Chrome after feature')

  context.set('isQuitting', true)

  await browser.quit()
})

AfterAll(async function () {
  if (!context.get('tags').length) return

  if (!context.get('isQuitting')) {
    logger.info('action', 'Quitting Google Chrome after feature')

    if (!browser) return

    await browser.quit()
  }

  const isFailedFeature = context.get('isFailedFeature')
  const isDev = process.env.NODE_ENV === 'development'

  const reportTimeout = isFailedFeature || isDev ? 1500 : 3000

  setTimeout(() => {
    reporter.generate({
      theme: 'bootstrap',
      jsonFile: `${reportsPath}/cucumber-report.json`,
      output: `${reportsPath}/cucumber-report.html`,
      reportSuiteAsScenarios: true,
      ignoreBadJsonFile: true,
      launchReport: properties.get('launch.report'),
      columnLayout: 1,
      brandTitle: 'Nummus Bot CucumberJs Report',
      metadata: {
        'Nummus Bot version': `${packageJson.version}`,
        'Test Environment': `${env}`,
        Browser: `Chrome ${chromedriver.version}`,
        Platform: `${context.get('os')}`
      }
    })
  }, reportTimeout)
})
