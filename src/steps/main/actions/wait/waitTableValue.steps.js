import { When } from '@cucumber/cucumber'
import context from '../../../../context.js'
import { tableDrivenToObjects } from '../../../../utils/cucumber.utils.js'
import { getPageSelector } from '../../../../utils/element.utils.js'
import { resolveAllText } from '../../../../utils/resolver.utils.js'
import { updateTextSelector } from '../../../../utils/step.utils.js'

/**
 * @module steps/actions/waitTableValue
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Assigning the needed information in a table driven, wait for the element eith given form value to be present using the
 * condition.timeout in config.properties as default.
 *
 * 2. **About passing multiple resolvers in a value:**
 *
 * - You can inform multiple values splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When aguardo estar presente os elementos com os seguintes valores na propriedade value (iguais|contendo):
 * @param {string} option type of action that will be executed
 * @param {dataTable} rawTable Table with the element page, selector and value
 * @example When aguardo estar presente os elementos com os seguintes valores na propriedade value iguais:
 * | page     | selector | value    |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 * @example When aguardo estar presente os elementos com os seguintes valores na propriedade value contendo:
 * | page     | selector | value    |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 */
When(/^aguardo estar presente os elementos com os seguintes valores na propriedade value (iguais|contendo):$/,
  async (option, { rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)

    let isEquals = true

    if (option === 'contendo') isEquals = false

    for (const line of objectTable) {
      const valueResolved = await resolveAllText(line.value)

      if (!valueResolved) continue

      const selector = getPageSelector(line.page, line.selector)
      const updatedSelector = await updateTextSelector(selector)

      await browser.waitForElementValue({ selector: updatedSelector, value: valueResolved, isEquals })
    }
  })

/**
 * @instance
 * @name When aguardo estar presente os elementos com os seguintes valores na propriedade value (iguais|contendo) no início (da feature|do scenario):
 * @param {string} option type of action that will be executed
 * @param {string} startOption When the action will be executed
 * @param {dataTable} rawTable  Table with the element page, selector and value
 * @example When aguardo estar presente os elementos com os seguintes valores na propriedade value iguais no início da feature:
 * | page     | selector | value    |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 * @example When aguardo estar presente os elementos com os seguintes valores na propriedade value iguais no início do scenario:
 * | page     | selector | value    |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 *  @example When aguardo estar presente os elementos com os seguintes valores na propriedade value contendo no início da feature:
 * | page     | selector | value    |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 * @example When aguardo estar presente os elementos com os seguintes valores na propriedade value contendo no início do scenario:
 * | page     | selector | value    |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 */
When(/^aguardo estar presente os elementos com os seguintes valores na propriedade value (iguais|contendo) no início (da feature|do scenario):$/,
  async (option, startOption, { rawTable }) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType) return

    let isEquals = true

    if (option === 'contendo') isEquals = false

    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      const valueResolved = await resolveAllText(line.value)

      if (!valueResolved) continue

      const selector = getPageSelector(line.page, line.selector)
      const updatedSelector = await updateTextSelector(selector)

      await browser.waitForElementValue({ selector: updatedSelector, value: valueResolved, isEquals })
    }
  })

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" com os seguintes valores na propriedade value (iguais|contendo):
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} option type of action that will be executed
 * @param {dataTable} rawTable Cucumber table with the form element value
 * @example When aguardo estar presente o "@commons"."list" com os seguintes valores na propriedade value iguais:
 * | value                   |
 * | value de uma lista aqui |
 * | value de uma lista aqui |
 * @example When aguardo estar presente o "@commons"."list" com os seguintes valores na propriedade value contendo:
 * | value                   |
 * | value de uma lista aqui |
 * | value de uma lista aqui |
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" com os seguintes valores na propriedade value (iguais|contendo):$/,
  async (page, element, option, { rawTable }) => {
    if (!page || !element) return

    let isEquals = true
    const selector = getPageSelector(page, element)
    const updatedSelector = await updateTextSelector(selector)
    const objectTable = await tableDrivenToObjects(rawTable)

    if (option === 'contendo') isEquals = false

    for (const line of objectTable) {
      const valueResolved = await resolveAllText(line.value)

      if (!valueResolved) continue

      await browser.waitForElementValue({ selector: updatedSelector, value: valueResolved, isEquals })
    }
  })
