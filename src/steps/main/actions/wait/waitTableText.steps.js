import { When } from '@cucumber/cucumber'
import context from '../../../../context.js'
import { tableDrivenToObjects } from '../../../../utils/cucumber.utils.js'
import { getPageSelector } from '../../../../utils/element.utils.js'
import { resolveAllText } from '../../../../utils/resolver.utils.js'

/**
 * @module steps/actions/waitTableText
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Assigning the needed information in a table driven, wait for the given element with text to be present using the
 * condition.timeout in config.properties as default.
 *
 * 2. **About passing multiple resolvers in a text:**
 *
 * - You can inform multiple texts splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When aguardo estar presente os elementos com os seguintes textos (iguais|contendo):
 * @param {string} option type of action that will be executed
 * @param {dataTable} rawTable Table with the element page, selector and text
 * @example When aguardo estar presente os elementos com os seguintes textos iguais:
 * | page     | selector | text     |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 * @example When aguardo estar presente os elementos com os seguintes textos contendo:
 * | page     | selector | text     |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 */
When(/^aguardo estar presente os elementos com os seguintes textos (iguais|contendo):$/,
  async (option, { rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)

    let isEquals = true

    if (option === 'contendo') isEquals = false

    for (const line of objectTable) {
      const textResolved = await resolveAllText(line.text)

      if (!textResolved) continue

      const selector = getPageSelector(line.page, line.selector)

      await browser.waitForElementText({ selector, text: textResolved, isEquals })
    }
  })

/**
 * @instance
 * @name When aguardo estar presente os elementos com os seguintes textos (iguais|contendo) no início (da feature|do scenario):
 * @param {string} option type of action that will be executed
 * @param {string} startOption When the action will be executed
 * @param {dataTable} rawTable Table with the element page, selector and text
 * @example When aguardo estar presente os elementos com os seguintes textos iguais no início da feature:
 * | page     | selector | text     |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 * @example When aguardo estar presente os elementos com os seguintes textos iguais no início do scenario:
 * | page     | selector | text     |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 *  @example When aguardo estar presente os elementos com os seguintes textos contendo no início da feature:
 * | page     | selector | text     |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 * @example When aguardo estar presente os elementos com os seguintes textos contendo no início do scenario:
 * | page     | selector | text     |
 * | @commons | button   | Salvar   |
 * | @commons | button   | Cancelar |
 */
When(/^aguardo estar presente os elementos com os seguintes textos (iguais|contendo) no início (da feature|do scenario):$/,
  async (option, startOption, { rawTable }) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType) return

    let isEquals = true

    if (option === 'contendo') isEquals = false

    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      const textResolved = await resolveAllText(line.text)

      if (!textResolved) continue

      const selector = getPageSelector(line.page, line.selector)

      await browser.waitForElementText({ selector, text: textResolved, isEquals })
    }
  })

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo):
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} option type of action that will be executed
 * @param {dataTable} rawTable Table with the text
 * @example When aguardo estar presente o "@commons"."list" com os seguintes textos iguais:
 * | text                    |
 * | Texto de uma lista aqui |
 * | Texto de uma lista aqui |
 * @example When aguardo estar presente o "@commons"."list" com os seguintes textos contendo:
 * | text                    |
 * | Texto de uma lista aqui |
 * | Texto de uma lista aqui |
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo):$/,
  async (page, element, option, { rawTable }) => {
    if (!page || !element) return

    let isEquals = true
    const selector = getPageSelector(page, element)
    const objectTable = await tableDrivenToObjects(rawTable)

    if (option === 'contendo') isEquals = false

    for (const line of objectTable) {
      const textResolved = await resolveAllText(line.text)

      if (!textResolved) continue

      await browser.waitForElementText({ selector, text: textResolved, isEquals })
    }
  })

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo) no início (da feature|do scenario):
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} option type of action that will be executed
 * @param {string} startOption When the action will be executed
 * @param {dataTable} rawTable Table with the text
 * @example When aguardo estar presente o "@commons"."list" com os seguintes textos iguais no início da feature:
 * | text                    |
 * | Texto de uma lista aqui |
 * | Texto de uma lista aqui |
 * @example When aguardo estar presente o "@commons"."list" com os seguintes textos contendo no início da feature:
 * | text                    |
 * | Texto de uma lista aqui |
 * | Texto de uma lista aqui |
 * @example When aguardo estar presente o "@commons"."list" com os seguintes textos iguais no início do scenario:
 * | text                    |
 * | Texto de uma lista aqui |
 * | Texto de uma lista aqui |
 * @example When aguardo estar presente o "@commons"."list" com os seguintes textos contendo no início do scenario:
 * | text                    |
 * | Texto de uma lista aqui |
 * | Texto de uma lista aqui |
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo) no início (da feature|do scenario):$/,
  async (page, element, option, startOption, { rawTable }) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (!page || !element || startType) return

    let isEquals = true
    const selector = getPageSelector(page, element)
    const objectTable = await tableDrivenToObjects(rawTable)

    if (option === 'contendo') isEquals = false

    for (const line of objectTable) {
      const textResolved = await resolveAllText(line.text)

      if (!textResolved) continue

      await browser.waitForElementText({ selector, text: textResolved, isEquals })
    }
  })

/**
 * @instance
 * @name When aguardo estar presente os elementos dentro dos seguintes elementos contendo os textos:
 * @param {dataTable} rawTable Table with the element page, selector and text
 * @example When aguardo estar presente os elementos dentro dos seguintes elementos contendo os textos:
 * | sonPage  | sonElement | page     | element | text     |
 * | @commons | maisOpcoes | @commons | lista   | Crédito  |
 */
When(/^aguardo estar presente os elementos dentro dos seguintes elementos contendo os textos:$/,
  async ({ rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      const { sonPage, sonElement, page, element, text } = line

      if (!text || !sonPage || !sonElement || !page || !element) continue

      let targetWebdriverId = null
      const selector = getPageSelector(page, element)
      const sonSelector = getPageSelector(sonPage, sonElement)

      const textResolved = await resolveAllText(text)

      if (!textResolved) continue

      await browser.getIdByParentText(sonSelector, selector, textResolved, result => {
        targetWebdriverId = result.sonExtractedValue
      })

      if (targetWebdriverId) continue

      throw new Error(`Element ${sonElement} not found inside the element ${element}!`)
    }
  })
