import { When } from '@cucumber/cucumber'
import context from '../../../../context.js'
import { tableDrivenToObjects } from '../../../../utils/cucumber.utils.js'
import { getPageSelector } from '../../../../utils/element.utils.js'

/**
 * @module steps/actions/waitTable
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Assigning the needed information in a table driven, wait for the given element to be present or visible, using the
 * condition.timeout in config.properties as default.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When aguardo estar presente os elementos:
 * @param {dataTable} rawTable Cucumber table with the element page and selector
 * @example When aguardo estar presente os elementos:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | button   |
 */
When(/^aguardo estar presente os elementos:$/,
  async ({ rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      const selector = getPageSelector(line.page, line.selector)

      await browser.waitForElementPresent(selector)
    }
  })

/**
 * @instance
 * @name When aguardo estar visível os elementos:
 * @param {dataTable} rawTable Cucumber table with the element page and selector
 * @example When aguardo estar visível os elementos:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | button   |
 */
When(/^aguardo estar visível os elementos:$/,
  async ({ rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      const selector = getPageSelector(line.page, line.selector)

      await browser.waitForElementVisible(selector)
    }
  })

/**
 * @instance
 * @name When aguardo estar presente os elementos no início (da feature|do scenario):
 * @param {string} startOption When the action will be executed
 * @param {dataTable} rawTable Cucumber table with the element page and selector
 * @example When aguardo estar presente os elementos no início do scenario:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | button   |
 *  @example When aguardo estar presente os elementos no início da feature:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | button   |
 */
When(/^aguardo estar presente os elementos no início (da feature|do scenario):$/,
  async (startOption, { rawTable }) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType) return

    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      const selector = getPageSelector(line.page, line.selector)

      await browser.waitForElementPresent(selector)
    }
  })

/**
 * @instance
 * @name When aguardo estar visível os elementos no início (da feature|do scenario):
 * @param {string} startOption When the action will be executed
 * @param {dataTable} rawTable Cucumber table with the element page and selector
 * @example When aguardo estar visível os elementos no início do scenario:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | button   |
 *  @example When aguardo estar visível os elementos no início da feature:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | button   |
 */
When(/^aguardo estar visível os elementos no início (da feature|do scenario):$/,
  async (startOption, { rawTable }) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType) return

    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      const selector = getPageSelector(line.page, line.selector)

      await browser.waitForElementVisible(selector)
    }
  })
