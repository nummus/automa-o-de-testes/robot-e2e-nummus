import { When } from '@cucumber/cucumber'
import properties from '../../../../config.js'
import context from '../../../../context.js'
import { waitForLoadingNotPresent } from '../../../../utils/action.utils.js'
import { getPageSelector } from '../../../../utils/element.utils.js'

/**
 * @module steps/actions/wait
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Wait for the given element to be present using the
 * condition.timeout in config.properties as default, or pause the execution a given time
 * or indefinitely, until resumed by pressing a key in terminal.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When breakpoint
 * @example When breakpoint
 */
When(/^breakpoint$/,
  async () => {
    await browser.pause()
  })

/**
 * @instance
 * @name When breakpoint se "([^"]*)" for verdadeiro
 * @param {string} shouldContinue Enable or disable the step if a scenario do not need it
 * @example When breakpoint se "Sim" for verdadeiro
 */
When(/^breakpoint se "([^"]*)" for verdadeiro$/,
  async shouldContinue => {
    if (!shouldContinue) return

    await browser.pause()
  })

/**
 * @instance
 * @name When aguardo indefinidamente
 * @example When aguardo indefinidamente
 */
When(/^aguardo indefinidamente$/,
  async () => {
    await browser.pause()
  })

/**
 * @instance
 * @name When aguardo indefinidamente se "([^"]*)" for verdadeiro
 * @param {string} shouldContinue Enable or disable the step if a scenario do not need it
 * @example When aguardo indefinidamente se "Sim" for verdadeiro
 */
When(/^aguardo indefinidamente se "([^"]*)" for verdadeiro$/,
  async shouldContinue => {
    if (!shouldContinue) return

    await browser.pause()
  })

/**
 * @instance
 * @name When aguardo por (\d+) segundos
 * @param {number} time The number of seconds to pause the execution
 * @example When aguardo por 2 segundos
 */
When(/^aguardo por (\d+) segundos$/,
  async time => {
    if (!time) return

    await browser.pause(parseInt(time) * 1000)
  })

/**
 * @instance
 * @name When aguardo por (\d+) segundos no início (da feature|do scenario)
 * @param {number} time The number of seconds to pause the execution
 * @param {string} startOption When the action will be executed
 * @example When aguardo por 2 segundos no início da feature
 * @example When aguardo por 2 segundos no início do scenario
 */
When(/^aguardo por (\d+) segundos no início (da feature|do scenario)$/,
  async (time, startOption) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType) return

    if (!context.get('isNewFeature') || !time) return

    await browser.pause(parseInt(time) * 1000)
  })

/**
 * @instance
 * @name When aguardo o carregamento da página
 * @example When aguardo o carregamento da página
 */
When(/^aguardo o carregamento da página$/,
  async () => {
    await waitForLoadingNotPresent()

    await browser.pause(2000)
  })

/**
 * @instance
 * @name When aguardo o carregamento da página no início (da feature|do scenario)
 * @param {string} startOption When the action will be executed
 * @example When aguardo o carregamento da página no início da feature
 * @example When aguardo o carregamento da página no início do scenario
 */
When(/^aguardo o carregamento da página no início (da feature|do scenario)$/,
  async (startOption) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType) return

    await waitForLoadingNotPresent()

    await browser.pause(2000)
  })

/**
 * @instance
 * @name When aguardo (estar presente|não estar presente) o "([^"]*)"."([^"]*)"
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When aguardo estar presente o "@commons"."button"
 * @example When aguardo não estar presente o "@commons"."button"
 */
When(/^aguardo (estar|não estar) presente o "([^"]*)"."([^"]*)"$/,
  async (option, page, element) => {
    const selector = getPageSelector(page, element)

    option === 'estar'
      ? await browser.waitForElementPresent(selector)
      : await browser.waitForElementNotPresent(selector)
  })

/**
 * @instance
 * @name When aguardo (estar presente|não estar presente) o "([^"]*)"."([^"]*)" se "([^"]*)" for verdadeiro
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} shouldContinue Enable or disable the step if a scenario do not need it
 * @example When aguardo estar presente o "@commons"."button" se "Sim" for verdadeiro
 * @example When aguardo não estar presente o "@commons"."button" se "Sim" for verdadeiro
 */
When(/^aguardo (estar|não estar) presente o "([^"]*)"."([^"]*)" se "([^"]*)" for verdadeiro$/,
  async (option, page, element, shouldContinue) => {
    if (!shouldContinue) return

    const selector = getPageSelector(page, element)

    option === 'estar'
      ? await browser.waitForElementPresent(selector)
      : await browser.waitForElementNotPresent(selector)
  })

/**
 * @instance
 * @name When aguardo estar (habilitado|visível|não visível) o "([^"]*)"."([^"]*)"
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When aguardo estar habilitado o "@commons"."button"
 * @example When aguardo estar visível o "@commons"."button"
 * @example When aguardo estar não visível o "@commons"."button"
 */
When(/^aguardo estar (habilitado|visível|não visível) o "([^"]*)"."([^"]*)"$/,
  async (option, page, element) => {
    const selector = getPageSelector(page, element)

    option === 'habilitado'
      ? await browser.waitForElementEnabled(selector)
      : await option === 'visível'
        ? await browser.waitForElementVisible(selector)
        : await browser.waitForElementNotVisible(selector)
  })

/**
 * @instance
 * @name When aguardo estar (habilitado|visível|não visível) o "([^"]*)"."([^"]*)" se "([^"]*)" for verdadeiro
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} shouldContinue Enable or disable the step if a scenario do not need it
 * @example When aguardo estar habilitado o "@commons"."button" se "Sim" for verdadeiro
 * @example When aguardo estar visível o "@commons"."button" se "Sim" for verdadeiro
 * @example When aguardo estar não visível o "@commons"."button" se "Sim" for verdadeiro
 */
When(/^aguardo estar (habilitado|visível|não visível) o "([^"]*)"."([^"]*)" se "([^"]*)" for verdadeiro$/,
  async (option, page, element, shouldContinue) => {
    if (!shouldContinue) return

    const selector = getPageSelector(page, element)

    option === 'habilitado'
      ? await browser.waitForElementEnabled(selector)
      : await option === 'visível'
        ? await browser.waitForElementVisible(selector)
        : await browser.waitForElementNotVisible(selector)
  })

/**
 * @instance
 * @name When aguardo o foco em algum campo
 * @example When aguardo o foco em algum campo
 */
When(/^aguardo o foco em algum campo$/,
  async () => {
    await browser.elementActive()
  })

/**
 * @instance
 * @name When aguardo o foco no (campo|campo com autocomplete) "([^"]*)"."([^"]*)"
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When aguardo o foco no campo "@commons"."input"
 * @example When aguardo o foco no campo com autocomplete "@commons"."input"
 */
When(/^aguardo o foco no (campo|campo com autocomplete) "([^"]*)"."([^"]*)"$/,
  async (option, page, element) => {
    const selector = getPageSelector(page, element)

    option === 'campo'
      ? await browser.waitForElementFocus(selector)
      : await browser.waitForElementFocus({ selector, isAutocomplete: true })
  })

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" um total de (\d+) vezes(| no início do scenario)
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} quantity Number of times a element must be present in the page
 * @param {string} option Type of action that will be executed
 * @example When aguardo estar presente o "([^"]*)"."([^"]*)" um total de (\d+) vezes
 * @example When aguardo estar presente o "([^"]*)"."([^"]*)" um total de (\d+) vezes no início do scenario
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" um total de (\d+) vezes(| no início do scenario)$/,
  async (page, element, quantity, option) => {
    const notStart = option === ' no início do scenario' ? !context.get('isNewScenario') : false

    if (notStart) return

    const timeToAwait = properties.get('condition.timeout')
    const selector = getPageSelector(page, element)

    await browser.expect
      .elements(selector).count.to.equal(quantity).before(timeToAwait)
  })
