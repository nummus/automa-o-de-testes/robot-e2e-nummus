import { When } from '@cucumber/cucumber'
import getJsonFileData from '../../../../resolvers/getJsonFileData.js'
import { getPageSelector } from '../../../../utils/element.utils.js'
import { resolveAllText } from '../../../../utils/resolver.utils.js'

/**
 * @module steps/actions/waitFileText
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Wait for the given element with alls the texts inside a .json file to be present using the
 * condition.timeout in config.properties as default. The file must be a array of texts.
 *
 * 2. **Example of the .json file with texts:**
 *
 * [
 * "O dia de hoje é ; @getTime(D)"
 * "Teste sobre o dia atual aqui"
 * "O dia e um cpf aqui ; @gerarCpf() ; @getTime(D)"
 * ]
 *
 * 3. **About passing multiple resolvers in a text:**
 *
 * - You can inform multiple texts splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 4. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" (com as informações iguais|contendo as informações) da lista "(.*)"
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} option type of action that will be executed
 * @param {string} filePath Path to a .json file with a array of texts to be validated using the given element
 * @example When aguardo estar presente o "@commons"."list" com as informações iguais da lista "/json/paciente/debitos/listaRepeticao1.json"
 * @example When aguardo estar presente o "@commons"."list" contendo as informações da lista "/json/paciente/debitos/listaRepeticao1.json"
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" (com as informações iguais|contendo as informações) da lista "(.*)"$/,
  async (page, element, option, filePath) => {
    if (!filePath) return

    let isEquals = true

    if (option.indexOf('contendo') !== -1) isEquals = false

    const list = await getJsonFileData(filePath)
    const selector = getPageSelector(page, element)

    for (const line of list) {
      const textResolved = await resolveAllText(line)

      if (!textResolved) continue

      await browser
        .waitForElementPresent(selector)
        .waitForElementText({ selector, text: textResolved, isEquals })
    }
  })
