import { When } from '@cucumber/cucumber'
import context from '../../../../context.js'
import { getPageSelector } from '../../../../utils/element.utils.js'
import { resolveAllText } from '../../../../utils/resolver.utils.js'
import { updateTextSelector } from '../../../../utils/step.utils.js'

/**
 * @module steps/actions/waitValue
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Wait for the given element with value to be present using the
 * condition.timeout in config.properties as default.
 *
 * 2. **About passing multiple resolvers in a value:**
 *
 * - You can inform multiple texts splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" com a propriedade value (igual|contendo) "([^"]*)"
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} value Complete value of the element that will receive the interaction
 * @example When aguardo estar presente o "@commons"."list" com a propriedade value igual "Teste"
 * @example When aguardo estar presente o "@commons"."list" com a propriedade value contendo "Teste"
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" com a propriedade value (igual|contendo) "([^"]*)"$/,
  async (page, element, option, value) => {
    if (!value) return

    let isEquals = true
    const valueResolved = await resolveAllText(value)

    if (!valueResolved) return

    if (option === 'contendo') isEquals = false

    const selector = getPageSelector(page, element)
    const updatedSelector = await updateTextSelector(selector)

    await browser.waitForElementValue({ selector: updatedSelector, value: valueResolved, isEquals })
  })

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" com a propriedade value (igual|contendo) "([^"]*)" no início (da feature|do scenario)
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} value Complete value of the element that will receive the interaction
 * @example When aguardo estar presente o "@commons"."list" com a propriedade value igual "Teste" no início da feature
 * @example When aguardo estar presente o "@commons"."list" com a propriedade value contendo "Teste" no início da feature
 * @example When aguardo estar presente o "@commons"."list" com a propriedade value igual "Teste" no início do scenario
 * @example When aguardo estar presente o "@commons"."list" com a propriedade value contendo "Teste" no início do scenario
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" com a propriedade value (igual|contendo) "([^"]*)" no início (da feature|do scenario)$/,
  async (page, element, option, value, startOption) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType || !value) return

    let isEquals = true
    const valueResolved = await resolveAllText(value)

    if (!valueResolved) return

    if (option === 'contendo') isEquals = false

    const selector = getPageSelector(page, element)
    const updatedSelector = await updateTextSelector(selector)

    await browser.waitForElementValue({ selector: updatedSelector, value: valueResolved, isEquals })
  })
