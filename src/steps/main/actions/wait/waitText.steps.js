import { When } from '@cucumber/cucumber'
import context from '../../../../context.js'
import resolveJson from '../../../../resolvers/resolveJson.js'
import { getPageSelector } from '../../../../utils/element.utils.js'
import { resolveAllText } from '../../../../utils/resolver.utils.js'

/**
 * @module steps/actions/waitText
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Wait for the given element with text to be present using the
 * condition.timeout in config.properties as default.
 *
 * 2. **About passing multiple resolvers in a text:**
 *
 * - You can inform multiple texts splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" com o texto (igual|contendo) "([^"]*)"
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} text Complete text of the element that will receive the interaction
 * @example When aguardo estar presente o "@commons"."list" com o texto igual "Teste"
 * @example When aguardo estar presente o "@commons"."list" com o texto contendo "Teste"
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" com o texto (igual|contendo) "([^"]*)"$/,
  async (page, element, option, text) => {
    if (!text) return

    let isEquals = true
    const textResolved = await resolveAllText(text)

    if (!textResolved) return

    if (option === 'contendo') isEquals = false

    const selector = getPageSelector(page, element)

    await browser.waitForElementText({ selector, text: textResolved, isEquals })
  })

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" com o texto (igual|contendo) "([^"]*)" no início (da feature|do scenario)
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} text Complete text of the element that will receive the interaction
 * @example When aguardo estar presente o "@commons"."list" com o texto igual "Teste" no início da feature
 * @example When aguardo estar presente o "@commons"."list" com o texto contendo "Teste" no início da feature
 * @example When aguardo estar presente o "@commons"."list" com o texto igual "Teste" no início do scenario
 * @example When aguardo estar presente o "@commons"."list" com o texto contendo "Teste" no início do scenario
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" com o texto (igual|contendo) "([^"]*)" no início (da feature|do scenario)$/,
  async (page, element, option, text, startOption) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType || !text) return

    let isEquals = true
    const textResolved = await resolveAllText(text)

    if (!textResolved) return

    if (option === 'contendo') isEquals = false

    const selector = getPageSelector(page, element)

    await browser.waitForElementText({ selector, text: textResolved, isEquals })
  })

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" (com os textos iguais ao|contendo os textos) do arquivo "([^"]*)"
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} option Type of action that will be executed
 * @param {string} filePath Path to a json file with an array of strings
 * @example When aguardo estar presente o "@commons"."list" com os textos iguais ao do arquivo "/json/teste/arquivoTexto.json"
 * @example When aguardo estar presente o "@commons"."list" contendo os textos do arquivo "/json/teste/arquivoTexto.json"
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" (com os textos iguais ao|contendo os textos) do arquivo "([^"]*)"$/,
  async (page, element, option, filePath) => {
    if (!page || !element) return

    let isEquals = true
    const selector = getPageSelector(page, element)
    const fileData = await resolveJson(filePath)

    if (option.includes('contendo')) isEquals = false

    for (const item of fileData) {
      const textResolved = await resolveAllText(item)

      if (!textResolved) continue

      await browser.waitForElementText({ selector, text: textResolved, isEquals })
    }
  })

/**
 * @instance
 * @name When aguardo estar presente o "([^"]*)"."([^"]*)" dentro de "([^"]*)"."([^"]*)" com o texto contendo "([^"]*)"
 * @param {string} sonPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} sonElement Name of an object property in .page.js where the element's css selector is stored
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} text Complete text of the element that will receive the interaction
 * @example When aguardo estar presente o "@commons"."button" dentro de "@commons"."list" com o texto contendo "Teste"
 */
When(/^aguardo estar presente o "([^"]*)"."([^"]*)" dentro de "([^"]*)"."([^"]*)" com o texto contendo "([^"]*)"$/,
  async (sonPage, sonElement, page, element, text) => {
    if (!text) return

    let targetWebdriverId
    const selector = getPageSelector(page, element)
    const sonSelector = getPageSelector(sonPage, sonElement)
    const textResolved = await resolveAllText(text)

    if (!textResolved) return

    await browser.getIdByParentText(sonSelector, selector, textResolved, result => {
      targetWebdriverId = result.sonExtractedValue
    })

    if (targetWebdriverId) return

    throw new Error(`Element ${sonElement} not found inside the element ${element}!`)
  })
