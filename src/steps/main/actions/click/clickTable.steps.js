import { When } from '@cucumber/cucumber'
import { tableDrivenToObjects } from '../../../../utils/cucumber.utils.js'
import { getPageSelector } from '../../../../utils/element.utils.js'
import { resolveAllText } from '../../../../utils/resolver.utils.js'

/**
 * @module steps/actions/clickTable
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Assigning the needed information in a table driven, execute a click or doubleClick event on the element with text.
 *
 * 2. **About passing multiple resolvers in a text:**
 *
 * - You can inform multiple texts splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When (clico|clico duas vezes) em todos os "([^"]*)"."([^"]*)" dentro de "([^"]*)"."([^"]*)" com os seguintes textos contendo:
 * @param {string} action type of action that will be executed
 * @param {string} sonPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} sonElement Name of an object property in .page.js where the element's css selector is stored
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {dataTable} rawTable Cucumber table with the element complete text that is passed under the step
 * @example When clico em todos os "@commons"."button" dentro de "@commons"."list" com os seguintes textos contendo:
 * | text     |
 * | Salvar   |
 * | Cancelar |
 * @example When clico duas vezes em todos os "@commons"."button" dentro de "@commons"."list" com os seguintes textos contendo:
 * | text     |
 * | Salvar   |
 * | Cancelar |
 */
When(/^(clico|clico duas vezes) em todos os "([^"]*)"."([^"]*)" dentro de "([^"]*)"."([^"]*)" com os seguintes textos contendo:$/,
  async (action, sonPage, sonElement, page, element, { rawTable }) => {
    const selector = getPageSelector(page, element)
    const sonSelector = getPageSelector(sonPage, sonElement)

    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      if (!line.text) continue

      let targetWebdriverId

      const textResolved = await resolveAllText(line.text)

      if (!textResolved) continue

      await browser.getIdByParentText(sonSelector, selector, textResolved, result => {
        targetWebdriverId = result.sonExtractedValue
      })

      action === 'clico'
        ? await browser.elementIdClick(targetWebdriverId)
        : await browser.elementIdDoubleClick(targetWebdriverId)
    }
  })
