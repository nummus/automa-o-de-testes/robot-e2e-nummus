import { When } from '@cucumber/cucumber'
import { extractWebdriverId, getPageSelector } from '../../../../utils/element.utils.js'

/**
 * @module steps/actions/click
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Click or doubleClick on the given element. The element is scrolled into view if it is not already pointer-interactable.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)"
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When clico no "@commons"."button"
 * @example When clico duas vezes no "@commons"."button"
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)"$/,
  async (action, page, element) => {
    const selector = getPageSelector(page, element)

    await browser.waitForElementPresent(selector)

    action === 'clico'
      ? await browser.click(selector)
      : await browser.doubleClick(selector)
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" na posição (\d+)
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @example When clico no "@commons"."button" na posição 1
 * @example When clico duas vezes no "@commons"."button" na posição 1
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" na posição (\d+)$/,
  async (action, page, element, index) => {
    const selector = getPageSelector(page, element)

    await browser.waitForElementPresent(selector)

    action === 'clico'
      ? await browser.click({ selector, index })
      : await browser.doubleClick({ selector, index })
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" se "([^"]*)" for verdadeiro
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @param {string} shouldContinue Enable or disable the step if a scenario do not need it
 * @example When clico no "@commons"."button" se "Sim" for verdadeiro
 * @example When clico duas vezes no "@commons"."button" se "Sim" for verdadeiro
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" se "([^"]*)" for verdadeiro$/,
  async (action, page, element, shouldContinue) => {
    if (!shouldContinue) return

    const selector = getPageSelector(page, element)

    await browser.waitForElementPresent(selector)

    action === 'clico'
      ? await browser.click({ selector, abortOnFailure: true })
      : await browser.doubleClick(selector)
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) em todos os "([^"]*)"."([^"]*)" com intervalo de (\d+) segundos
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} time The number of seconds to wait between checks
 * @example When clico em todos os "@commons"."button" com intervalo de 1 segundos
 * @example When clico duas vezes em todos os "@commons"."button" com intervalo de 1 segundos
 */
When(/^(clico|clico duas vezes) em todos os "([^"]*)"."([^"]*)" com intervalo de (\d+) segundos$/,
  async (action, page, element, time) => {
    if (!time) time = 0.5

    const selector = getPageSelector(page, element)
    const resultElements = await browser.elements('css selector', selector)

    for (let index = 0; index < resultElements.length; index++) {
      await browser.waitForElementPresent(selector)

      action === 'clico'
        ? await browser.click(selector)
        : await browser.doubleClick(selector)

      await browser.pause(time * 1000)
    }
  })

/**
 * @instance
 * @name When realizo scroll na posição (\d+) e (clico|clico duas vezes) em todos os "([^"]*)"."([^"]*)" com intervalo de (\d+) segundos
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} time The number of seconds to wait between checks
 * @param {number} indexToScroll Index of the selector where a scroll action will be performed before continuing
 * @example When realizo scroll na posição 2 e clico em todos os "@commons"."button" com intervalo de 1 segundos
 * @example When realizo scroll na posição 3 e clico duas vezes em todos os "@commons"."button" com intervalo de 1 segundos
 */
When(/^realizo scroll na posição (\d+) e (clico|clico duas vezes) em todos os "([^"]*)"."([^"]*)" com intervalo de (\d+) segundos$/,
  async (indexToScroll, action, page, element, time) => {
    if (!time) time = 0.5

    const selector = getPageSelector(page, element)
    const resultElements = await browser.elements('css selector', selector)

    for (let index = 0; index < resultElements.length; index++) {
      if (index === indexToScroll) {
        await browser.scrollIntoView(selector, index)
      }

      await browser.waitForElementPresent(selector)

      action === 'clico'
        ? await browser.click(selector)
        : await browser.doubleClick(selector)

      await browser.pause(time * 1000)
    }
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" por (\d+) vezes
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} quantity Number of times that the interaction will happen
 * @example When clico no "@commons"."button" por 2 vezes
 * @example When clico duas vezes no "@commons"."button" por 2 vezes
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" por (\d+) vezes$/,
  async (action, page, element, quantity) => {
    if (!quantity) return

    const selector = getPageSelector(page, element)

    for (let index = 0; index < quantity; index++) {
      await browser.waitForElementPresent(selector)

      action === 'clico'
        ? await browser.click(selector)
        : await browser.doubleClick(selector)
    }
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" por (\d+) vezes com intervalo de (\d+) segundos
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} quantity Number of times that the interaction will happen
 * @param {number} time The number of seconds to wait between checks
 * @example When clico no "@commons"."button" por 2 vezes com intervalo de 1 segundos
 * @example When clico duas vezes no "@commons"."button" por 2 vezes com intervalo de 1 segundos
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" por (\d+) vezes com intervalo de (\d+) segundos$/,
  async (action, page, element, quantity, time) => {
    if (!quantity) return

    if (!time) time = 0.5

    const selector = getPageSelector(page, element)

    for (let index = 0; index < quantity; index++) {
      await browser.waitForElementPresent(selector)

      action === 'clico'
        ? await browser.click(selector)
        : await browser.doubleClick(selector)

      await browser.pause(time * 1000)
    }
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" caso esteja presente
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When clico no "@commons"."button" caso esteja presente
 * @example When clico duas vezes no "@commons"."button" caso esteja presente
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" caso esteja presente$/,
  async (action, page, element) => {
    const selector = getPageSelector(page, element)
    const resultElements = await browser.elements('css selector', selector)

    if (!resultElements.length) return

    const webdriverId = extractWebdriverId(resultElements, true)

    action === 'clico'
      ? await browser.elementIdClick(webdriverId)
      : await browser.elementIdDoubleClick(webdriverId)
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) em todos os "([^"]*)"."([^"]*)" caso estejam presentes com intervalo de (\d+) segundos
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} time The number of seconds to wait between checks
 * @example When clico em todos os "@commons"."button" caso estejam presentes com intervalo de 2 segundos
 * @example When clico duas vezes em todos os "@commons"."button" caso estejam presentes com intervalo de 2 segundos
 */
When(/^(clico|clico duas vezes) em todos os "([^"]*)"."([^"]*)" caso estejam presentes com intervalo de (\d+) segundos$/,
  async (action, page, element, time) => {
    if (!time) time = 0.5

    const selector = getPageSelector(page, element)
    const resultElements = await browser.elements('css selector', selector)

    if (!resultElements.length) return

    const webdriverIds = extractWebdriverId(resultElements)

    for (const id of webdriverIds) {
      action === 'clico'
        ? await browser.elementIdClick(id)
        : await browser.elementIdDoubleClick(id)

      await browser.pause(time * 1000)
    }
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" se estiver presente o "([^"]*)"."([^"]*)"
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} pageClick Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} elementClick Name of an object property in .page.js where the element's css selector is stored
 * @example When clico no "@commons"."button" se estiver presente o "@commons"."list"
 * @example When clico duas vezes no "@commons"."button" se estiver presente o "@commons"."list"
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" se estiver presente o "([^"]*)"."([^"]*)"$/,
  async (action, pageClick, elementClick, page, element) => {
    const selector = getPageSelector(page, element)
    const clickSelector = getPageSelector(pageClick, elementClick)

    const resultElements = await browser.elements('css selector', selector)

    if (!resultElements.length) return

    await browser.waitForElementPresent(clickSelector)

    action === 'clico'
      ? await browser.click(clickSelector)
      : await browser.doubleClick(clickSelector)
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" após ser habilitado pela automação
 * @param {string} action type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When clico no "@commons"."button" após ser habilitado pela automação
 * @example When clico duas vezes no "@commons"."button" após ser habilitado pela automação
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" após ser habilitado pela automação$/,
  async (action, page, element) => {
    const selector = getPageSelector(page, element)

    await browser
      .waitForElementPresent(selector)
      .setAttribute(selector, 'disabled', 'false')

    action === 'clico'
      ? await browser.click(selector)
      : await browser.doubleClick(selector)
  })
