import { When } from '@cucumber/cucumber'
import context from '../../../../context.js'
import { getPageSelector } from '../../../../utils/element.utils.js'
import { resolveAllText } from '../../../../utils/resolver.utils.js'

/**
 * @module steps/actions/clickText
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Click or double click on the given element with the given text.
 *
 * 2. **About passing multiple resolvers in a text:**
 *
 * - You can inform multiple texts splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When clico no botão "([^"]*)"
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} text Complete text of the element that will receive the interaction
 * @example When clico no botão "Salvar"
 */
When(/^clico no botão "([^"]*)"$/,
  async text => {
    if (!text) return

    const textResolved = await resolveAllText(text)

    await browser
      .clickElementText('button', textResolved)
      .pause(1000)
  })

/**
 * @instance
 * @name When clico no botão "([^"]*)" na posição (\d+)
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} text Complete text of the element that will receive the interaction
 * @param {number} index Index of the element in the page
 * @example When clico no botão "Salvar" na posição 1
 */
When(/^clico no botão "([^"]*)" na posição (\d+)$/,
  async (text, index) => {
    if (!text) return

    const textResolved = await resolveAllText(text)

    await browser
      .clickElementText({ selector: 'button', text: textResolved, index })
      .pause(1000)
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" com o (texto igual|texto contendo) "([^"]*)"
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} text Complete text of the element that will receive the interaction
 * @example When clico no "@commons"."button" com o texto igual "Salvar"
 * @exemple When clico duas vezes no "@commons"."button" com o texto igual "Salvar"
 * @example When clico no "@commons"."button" com o texto contendo "Salvar"
 * @exemple When clico duas vezes no "@commons"."button" com o texto contendo "Salvar"
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" com o texto (igual|contendo) "([^"]*)"$/,
  async (action, page, element, type, text) => {
    if (!text) return

    let isEquals = true
    const selector = getPageSelector(page, element)
    const textResolved = await resolveAllText(text)

    if (!textResolved) return

    if (type === 'contendo') isEquals = false

    action === 'clico'
      ? await browser.clickElementText({ selector, text: textResolved, isEquals })
      : await browser.doubleClickElementText({ selector, text: textResolved, isEquals })
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" com o (texto igual|texto contendo) "([^"]*)" (no início da feature|na posição \d+)
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} text Complete text of the element that will receive the interaction
 * @param {number} index Index of the element in the page
 * @example When clico no "@commons"."button" com o texto igual "Salvar" no início da feature
 * @example When clico duas vezes no "@commons"."button" com o texto igual "Salvar" no início da feature
 * @example When clico no "@commons"."button" com o texto contendo "Salvar" no início da feature
 * @example When clico duas vezes no "@commons"."button" com o texto contendo "Salvar" no início da feature
 * @example When clico no "@commons"."button" com o texto igual "Salvar" na posição 1
 * @example When clico duas vezes no "@commons"."button" com o texto igual "Salvar" na posição 1
 * @example When clico no "@commons"."button" com o texto contendo "Salvar" na posição 1
 * @example When clico duas vezes no "@commons"."button" com o texto contendo "Salvar" na posição 1
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" com o texto (igual|contendo) "([^"]*)" (no início da feature|na posição \d+)$/,
  async (action, page, element, type, text, whenDo) => {
    if (!text) return

    if (whenDo === 'no início da feature' && !context.get('isNewFeature')) return

    let index = 0

    if (whenDo.includes('na posição')) {
      index = parseInt(whenDo.match(/\d+/)[0])
    }

    let isEquals = true
    const selector = getPageSelector(page, element)
    const textResolved = await resolveAllText(text)

    if (!textResolved) return

    if (type === 'contendo') isEquals = false

    action === 'clico'
      ? await browser.clickElementText({ selector, text: textResolved, index, isEquals })
      : await browser.doubleClickElementText({ selector, text: textResolved, index, isEquals })
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" dentro de "([^"]*)"."([^"]*)" com o texto contendo "([^"]*)"
 * @param {string} sonPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} sonElement Name of an object property in .page.js where the element's css selector is stored
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} text Complete text of the element that will receive the interaction
 * @example When clico no "@commons"."button" dentro de "@commons"."list" com o texto contendo "Teste"
 * @example When clico duas vezes no "@commons"."button" dentro de "@commons"."list" com o texto contendo "Teste"
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" dentro de "([^"]*)"."([^"]*)" com o texto contendo "([^"]*)"$/,
  async (action, sonPage, sonElement, page, element, text) => {
    if (!text) return

    let targetWebdriverId
    const selector = getPageSelector(page, element)
    const sonSelector = getPageSelector(sonPage, sonElement)
    const textResolved = await resolveAllText(text)

    if (!textResolved) return

    await browser.getIdByParentText(sonSelector, selector, textResolved, result => {
      targetWebdriverId = result.sonExtractedValue
    })

    action === 'clico'
      ? await browser.elementIdClick(targetWebdriverId)
      : await browser.elementIdDoubleClick(targetWebdriverId)
  })

/**
 * @instance
 * @name When (clico|clico duas vezes) no "([^"]*)"."([^"]*)" com o texto igual "([^"]*)" se estiver presente o "([^"]*)"."([^"]*)"
 * @param {string} clickPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} clickElement Name of an object property in .page.js where the element's css selector is stored
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} text Complete text of the element that will receive the interaction
 * @example When clico no "@commons"."button" com o texto igual "Salvar" se estiver presente o "@commons"."list"
 * @example When clico duas vezes no "@commons"."button" com o texto igual "Salvar" se estiver presente o "@commons"."list"
 */
When(/^(clico|clico duas vezes) no "([^"]*)"."([^"]*)" com o texto igual "([^"]*)" se estiver presente o "([^"]*)"."([^"]*)"$/,
  async (action, clickPage, clickElement, text, page, element) => {
    if (!text) return

    await browser.pause(2000)

    const textResolved = await resolveAllText(text)
    const selector = getPageSelector(page, element)
    const webdriverIds = await browser.elements('css selector', selector)

    if (!webdriverIds.length || !textResolved) return

    const clickSelector = getPageSelector(clickPage, clickElement)

    action === 'clico'
      ? await browser.clickElementText(clickSelector, textResolved)
      : await browser.doubleClickElementText(clickSelector, textResolved)
  })
