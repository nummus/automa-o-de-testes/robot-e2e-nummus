import { When } from '@cucumber/cucumber'
import { clickGoogleCaptcha, waitForLoadingNotPresent } from '../../../../utils/action.utils'

/**
 * @module steps/actions/clickCaptcha
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Click on the present google captcha or in the captcha with given index.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When clico no captcha do google presente na tela
 * @example When clico no captcha do google presente na tela
 */
When(/^clico no captcha do google presente na tela$/,
  async () => {
    await clickGoogleCaptcha()
  })

/**
 * @instance
 * @name When clico no captcha do google na posição "([^"]*)"
 * @param {number} index Index of the google captcha in the page
 * @example When clico no captcha do google na posição "0"
 */
When(/^clico no captcha do google na posição "([^"]*)"$/,
  async (index) => {
    await waitForLoadingNotPresent()

    await browser
      .pause(1000)
      .frame(index)
      .pause(1000)
      .click('#recaptcha-anchor')
      .pause(1000)
      .frameParent()
      .pause(1000)
  })
