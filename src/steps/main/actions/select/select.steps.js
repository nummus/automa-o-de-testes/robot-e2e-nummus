import { When } from '@cucumber/cucumber'
import { selectText } from '../../../../utils/action.utils.js'

/**
 * @module steps/actions/select
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Send a package of actions to select options in fields that have such function.
 *
 * 2. **About passing multiple texts with multiple resolvers in a text:**
 *
 * - You can inform multiple texts splitted by "|" and split those texts with ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)|@getTime(dd-LL-yyyy)``` will result into ```865.327.101-56 15/02/2023 15-02-2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When seleciono o texto (igual|contendo) "([^"]*)" no "([^"]*)"."([^"]*)"
 * @param {string} option type of action that will be executed
 * @param {string} text Text that will be selected in the presented options
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When seleciono o texto igual "10" no "@commons"."autocompletePacientes"
 * @example When seleciono o texto contendo "10" no "@commons"."autocompletePacientes"
 */
When(/^seleciono o texto (igual|contendo) "([^"]*)" no "([^"]*)"."([^"]*)"$/,
  async (option, text, page, element) => {
    if (!text) return

    await selectText({ page, element, option, text })
  })

/**
 * @instance
 * @name When seleciono o texto (igual|contendo) "([^"]*)" no "([^"]*)"."([^"]*)" na posição (\d+)
 * @param {string} option type of action that will be executed
 * @param {string} text Text that will be selected in the presented options
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @example When seleciono o texto igual "10" no "@commons"."autocompletePacientes" na posição 1
 * @example When seleciono o texto contendo "10" no "@commons"."autocompletePacientes" na posição 1
 */
When(/^seleciono o texto (igual|contendo) "([^"]*)" no "([^"]*)"."([^"]*)" na posição (\d+)$/,
  async (option, text, page, element, index) => {
    if (!text) return

    await selectText({ page, element, option, text, index })
  })
