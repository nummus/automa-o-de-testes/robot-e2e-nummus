import { When } from '@cucumber/cucumber'
import { getPageSelector } from '../../../../utils/element.utils.js'

/**
 * @module steps/actions/movo
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Move the cursor to a given location or to the element.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When movo o cursor para "([^"]*)"."([^"]*)"
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When movo o cursor para "@commons"."button"
 */
When(/^movo o cursor para "([^"]*)"."([^"]*)"$/,
  async (page, element) => {
    const selector = getPageSelector(page, element)

    await browser.moveCursorToElement(selector)
  })

/**
 * @instance
 * @name When movo o cursor para "([^"]*)"."([^"]*)" na posição (\d+)
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @example When movo o cursor para "([^"]*)"."([^"]*)" na posição 1
 */
When(/^movo o cursor para "([^"]*)"."([^"]*)" na posição (\d+)$/,
  async (page, element, index) => {
    const selector = getPageSelector(page, element)

    await browser.moveCursorToElement({ selector, index })
  })
