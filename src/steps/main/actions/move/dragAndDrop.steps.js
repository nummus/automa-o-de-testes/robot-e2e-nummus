import { When } from '@cucumber/cucumber'
import { moveToElementWithText, pressLeftMouseButton, releaseLeftMouseButton } from '../../../../utils/action.utils.js'
import { getPageSelector } from '../../../../utils/element.utils.js'

/**
 * @module steps/actions/dragAndDrop
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Drag an element to the given coordinates or destination element.
 *
 * 2. **About passing multiple resolvers in a text:**
 *
 * - You can inform multiple texts splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When movo o cursor para "([^"]*)"."([^"]*)"
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When movo o cursor para "@commons"."button"
 */
When(/^movo o cursor para "([^"]*)"."([^"]*)"$/,
  async (page, element) => {
    const selector = getPageSelector(page, element)

    await browser.moveCursorToElement(selector)
  })

/**
 * @instance
 * @name When movo o cursor para "([^"]*)"."([^"]*)" na posição (\d+)
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @example When movo o cursor para "([^"]*)"."([^"]*)" na posição 1
 */
When(/^movo o cursor para "([^"]*)"."([^"]*)" na posição (\d+)$/,
  async (page, element, index) => {
    const selector = getPageSelector(page, element)

    await browser.moveCursorToElement({ selector, index })
  })

/**
 * @instance
 * @name When arrasto o "([^"]*)"."([^"]*)" para o "([^"]*)"."([^"]*)" e solto
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} targetPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} targetElement Name of an object property in .page.js where the element's css selector is stored
 * @example When arrasto o "@commons"."card" para o "@commons"."column" e solto
 */
When(/^arrasto o "([^"]*)"."([^"]*)" para o "([^"]*)"."([^"]*)" e solto$/,
  async (page, element, targetPage, targetElement) => {
    const selector = getPageSelector(page, element)
    const targetSelector = getPageSelector(targetPage, targetElement)

    await browser
      .waitForElementPresent(selector)
      .waitForElementPresent(targetSelector)
      .dragAndDrop(selector, targetSelector)
  })

/**
 * @instance
 * @name When arrasto o "([^"]*)"."([^"]*)" na posição (\d+) para o "([^"]*)"."([^"]*)" e solto
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @param {string} targetPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} targetElement Name of an object property in .page.js where the element's css selector is stored
 * @example When arrasto o "@commons"."card" na posição 1 para o "@commons"."column" e solto
 */
When(/^arrasto o "([^"]*)"."([^"]*)" na posição (\d+) para o "([^"]*)"."([^"]*)" e solto$/,
  async (page, element, index, targetPage, targetElement) => {
    const selector = getPageSelector(page, element)
    const targetSelector = getPageSelector(targetPage, targetElement)

    await browser
      .moveCursorToElement({ selector, index })
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.press()
      })

    await browser
      .moveCursorToElement(targetSelector)
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.release()
      })
  })

/**
 * @instance
 * @name When arrasto o "([^"]*)"."([^"]*)" para o "([^"]*)"."([^"]*)" na posição (\d+) e solto
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} targetPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} targetElement Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @example When arrasto o "@commons"."card" para o "@commons"."column" na posição 1 e solto
 */
When(/^arrasto o "([^"]*)"."([^"]*)" para o "([^"]*)"."([^"]*)" na posição (\d+) e solto$/,
  async (page, element, targetPage, targetElement, index) => {
    const selector = getPageSelector(page, element)
    const targetSelector = getPageSelector(targetPage, targetElement)

    await browser
      .moveCursorToElement(selector)
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.press()
      })

    await browser
      .moveCursorToElement({ selector: targetSelector, index })
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.release()
      })
  })

/**
 * @instance
 * @name When arrasto o "([^"]*)"."([^"]*)" na posição (\d+) para o "([^"]*)"."([^"]*)" na posição (\d+) e solto
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @param {string} targetPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} targetElement Name of an object property in .page.js where the element's css selector is stored
 * @param {number} targetIndex Index of the element in the page
 * @example When arrasto o "@commons"."card" na posição 1 para o "@commons"."column" na posição 1 e solto
 */
When(/^arrasto o "([^"]*)"."([^"]*)" na posição (\d+) para o "([^"]*)"."([^"]*)" na posição (\d+) e solto$/,
  async (page, element, index, targetPage, targetElement, targetIndex) => {
    const selector = getPageSelector(page, element)
    const targetSelector = getPageSelector(targetPage, targetElement)

    await browser
      .moveCursorToElement({ selector, index })
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.press()
      })

    await browser
      .moveCursorToElement({ selector: targetSelector, index: targetIndex })
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.release()
      })
  })

/**
 * @instance
 * @name When arrasto o "([^"]*)"."([^"]*)" coordenada X (\d+) e Y (\d+) para o "([^"]*)"."([^"]*)" e solto
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} x Offset to movo to, relative to the center of the element
 * @param {number} y Offset to movo to, relative to the center of the element
 * @param {string} targetPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} targetElement Name of an object property in .page.js where the element's css selector is stored
 * @example When arrasto o "@commons"."card" coordenada X 10 e Y 10 para o "@commons"."column" e solto
 */
When(/^arrasto o "([^"]*)"."([^"]*)" coordenada X (\d+) e Y (\d+) para o "([^"]*)"."([^"]*)" e solto$/,
  async (page, element, x, y, targetPage, targetElement) => {
    const selector = getPageSelector(page, element)
    const targetSelector = getPageSelector(targetPage, targetElement)

    await browser
      .moveCursorToElement({ selector, x, y })
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.press()
      })

    await browser
      .moveCursorToElement(targetSelector)
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.release()
      })
  })

/**
 * @instance
 * @name When arrasto o "([^"]*)"."([^"]*)" para o "([^"]*)"."([^"]*)" coordenada X (\d+) e Y (\d+) e solto
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} targetPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} targetElement Name of an object property in .page.js where the element's css selector is stored
 * @param {number} x Offset to movo to, relative to the center of the element
 * @param {number} y Offset to movo to, relative to the center of the element
 * @example When arrasto o "@commons"."card" para o "@commons"."column" coordenada X 10 e Y 10 e solto
 */
When(/^arrasto o "([^"]*)"."([^"]*)" para o "([^"]*)"."([^"]*)" coordenada X (\d+) e Y (\d+) e solto$/,
  async (page, element, targetPage, targetElement, x, y) => {
    const selector = getPageSelector(page, element)
    const targetSelector = getPageSelector(targetPage, targetElement)

    await browser
      .moveCursorToElement(selector)
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.press()
      })

    await browser
      .moveCursorToElement({ selector: targetSelector, x, y })
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.release()
      })
  })

/**
 * @instance
 * @name When arrasto o "([^"]*)"."([^"]*)" com o texto "([^"]*)" para o "([^"]*)"."([^"]*)" com o texto "([^"]*)" e solto
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} text Partial text of the element that will receive the interaction
 * @param {string} targetPage Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} targetElement Name of an object property in .page.js where the element's css selector is stored
 * @param {string} textTarget Partial text of the element that will receive the interaction
 * @example When arrasto o "@commons"."card" com o texto "Teste" para o "@commons"."column" com o texto "Finalizado" e solto
 */

When(/^arrasto o "([^"]*)"."([^"]*)" com o texto "([^"]*)" para o "([^"]*)"."([^"]*)" com o texto "([^"]*)" e solto$/,
  async (page, element, text, targetPage, targetElement, textTarget) => {
    if (!text || !textTarget) return

    const selector = getPageSelector(page, element)
    const targetSelector = getPageSelector(targetPage, targetElement)

    await moveToElementWithText(selector, text, false)
    await pressLeftMouseButton()
    await moveToElementWithText(targetSelector, textTarget, false, 2)
    await releaseLeftMouseButton()
  })
