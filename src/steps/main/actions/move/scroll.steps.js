import { When } from '@cucumber/cucumber'
import { getPageSelector } from '../../../../utils/element.utils.js'

/**
 * @module steps/actions/scroll
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Scrolls into view the element.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When realizo scroll até "([^"]*)"."([^"]*)"
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When realizo scroll até "@commons"."button"
 */
When(/^realizo scroll até o "([^"]*)"."([^"]*)"$/,
  async (page, element) => {
    const selector = getPageSelector(page, element)

    await browser.moveCursorToElement(selector)
  })

/**
 * @instance
 * @name When realizo scroll até "([^"]*)"."([^"]*)" se "([^"]*)" for verdadeiro
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {string} shouldContinue Enable or disable the step if a scenario do not need it
 * @example When realizo scroll até "@commons"."button" se "Sim" for verdadeiro
 */
When(/^realizo scroll até o "([^"]*)"."([^"]*)" se "([^"]*)" for verdadeiro$/,
  async (page, element, shouldContinue) => {
    if (!shouldContinue) return

    const selector = getPageSelector(page, element)

    await browser.moveCursorToElement(selector)
  })
