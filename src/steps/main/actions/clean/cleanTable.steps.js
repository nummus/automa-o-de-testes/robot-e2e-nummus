import { When } from '@cucumber/cucumber'
import { tableDrivenToObjects } from '../../../../utils/cucumber.utils.js'

/**
 * @module steps/actions/cleanTable
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Clear a textarea or a text input element's value or clear a key from the local storage.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When limpo as seguintes keys do local storage:
 * @param {dataTable} rawTable Cucumber table with local storage key name to clear
 * @example When limpo as seguintes keys do local storage:
 * | key    |
 * | origem |
 */
When(/^limpo as seguintes keys do local storage:$/,
  async ({ rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      await browser.removeLocalStorageKey(line.key)
    }
  })
