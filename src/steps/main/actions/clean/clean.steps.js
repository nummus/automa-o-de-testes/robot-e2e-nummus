import { When } from '@cucumber/cucumber'
import context from '../../../../context.js'
import { getPageSelector } from '../../../../utils/element.utils.js'

/**
 * @module steps/actions/clean
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Clear a textarea or a text input element's value or clear a key from the local storage.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When limpo a informação do (campo|campo de seleção|campo com máscara) "([^"]*)"."([^"]*)"
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When limpo a informação do campo "@commons"."input"
 * @example When limpo a informação do campo de seleção "@commons"."input"
 * @example When limpo a informação do campo com máscara "@commons"."input"
 */
When(/^limpo a informação do (campo|campo de seleção|campo com máscara) "([^"]*)"."([^"]*)"$/,
  async (option, page, element) => {
    const selector = getPageSelector(page, element)

    if (option === 'campo') {
      await browser
        .waitForElementPresent(selector)
        .clearValue(selector)
    }

    if (option === 'campo de seleção') {
      const clearSelector = `${selector} [aria-label="Clear"]`
      const webdriverIds = await browser.elements('css selector', clearSelector)

      for (let index = 0; index < webdriverIds.length; index++) {
        await browser
          .click(clearSelector)
          .waitForElementPresent(selector)
          .clearValue(selector)
      }
    }

    if (option === 'campo com máscara') {
      const osKey = context.get('osKey')

      await browser
        .waitForElementPresent(selector)
        .click({ selector, abortOnFailure: true })

      await browser.perform(() => {
        const actions = browser.actions({ async: true })

        return actions
          .keyDown(osKey)
          .keyDown('a')
          .keyUp('a')
          .keyUp(osKey)
          .sendKeys(browser.Keys.DELETE)
      })
    }
  })

/**
 * @instance
 * @name When limpo a informação do (campo|campo de seleção|campo com máscara) "([^"]*)"."([^"]*)" na posição (\d+)
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @example When limpo a informação do campo "@commons"."input" na posição 1
 * @example When limpo a informação do campo de seleção "@commons"."input" na posição 1
 * @example When limpo a informação do campo com máscara "@commons"."input" na posição 1
 */
When(/^limpo a informação do (campo|campo de seleção|campo com máscara) "([^"]*)"."([^"]*)" na posição (\d+)$/,
  async (option, page, element, index) => {
    const selector = getPageSelector(page, element)

    if (option === 'campo') {
      await browser
        .waitForElementPresent(selector)
        .clearValue({ selector, index })
    }

    if (option === 'campo de seleção') {
      const clearSelector = `${selector} [aria-label="Clear"]`
      const webdriverIds = await browser.elements('css selector', clearSelector)

      for (let index = 0; index < webdriverIds.length; index++) {
        await browser
          .click({ selector: clearSelector, index })
          .waitForElementPresent(selector)
          .clearValue({ selector, index })
      }
    }

    if (option === 'campo com máscara') {
      const osKey = context.get('osKey')

      await browser
        .waitForElementPresent(selector)
        .click({ selector, index })

      await browser.perform(() => {
        const actions = browser.actions({ async: true })

        return actions
          .keyDown(osKey)
          .keyDown('a')
          .keyUp('a')
          .keyUp(osKey)
          .sendKeys(browser.Keys.DELETE)
      })
    }
  })

/**
 * @instance
 * @name When limpo a informação do (campo|campo de seleção|campo com máscara) "([^"]*)"."([^"]*)" se "([^"]*)" for verdadeiro
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When limpo a informação do campo "@commons"."input" se "Sim" for verdadeiro
 * @example When limpo a informação do campo de seleção "@commons"."input" se "Sim" for verdadeiro
 * @example When limpo a informação do campo com máscara "@commons"."input" se "Sim" for verdadeiro
 */
When(/^limpo a informação do (campo|campo de seleção|campo com máscara) "([^"]*)"."([^"]*)" se "([^"]*)" for verdadeiro$/,
  async (option, page, element, shouldContinue) => {
    if (!shouldContinue) return

    const selector = getPageSelector(page, element)

    if (option === 'campo') {
      await browser
        .waitForElementPresent(selector)
        .clearValue(selector)
    }

    if (option === 'campo de seleção') {
      const clearSelector = `${selector} [aria-label="Clear"]`
      const webdriverIds = await browser.elements('css selector', clearSelector)

      for (let index = 0; index < webdriverIds.length; index++) {
        await browser
          .click(clearSelector)
          .waitForElementPresent(selector)
          .clearValue(selector)
      }
    }

    if (option === 'campo com máscara') {
      const osKey = context.get('osKey')

      await browser
        .waitForElementPresent(selector)
        .click({ selector, abortOnFailure: true })

      await browser.perform(() => {
        const actions = browser.actions({ async: true })

        return actions
          .keyDown(osKey)
          .keyDown('a')
          .keyUp('a')
          .keyUp(osKey)
          .sendKeys(browser.Keys.DELETE)
      })
    }
  })

/**
 * @instance
 * @name When limpo o atributo "([^"]*)" do elemento "([^"]*)"."([^"]*)"
 * @param {string} attribute The form element's attribute
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When limpo o atributo "value" do elemento "@commons"."input"
 */
When(/^limpo o atributo "([^"]*)" do elemento "([^"]*)"."([^"]*)"$/,
  async (attribute, page, element) => {
    if (attribute) return

    const selector = getPageSelector(page, element)

    await browser
      .waitForElementPresent(selector)
      .setAttribute(selector, attribute, '')
  })

/**
 * @instance
 * @name When limpo a key do local storage "([^"]*)"
 * @param {string} key Local storage key name to delete
 * @example When limpo a key do local storage "origem"
 */
When(/^limpo a key do local storage "([^"]*)"$/,
  async (key) => {
    await browser.removeLocalStorageKey(key)
  })
