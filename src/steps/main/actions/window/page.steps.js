import { When } from '@cucumber/cucumber'

/**
 * @module steps/actions/page
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Refresh the current page.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When atualizo a página
 * @example When atualizo a página
 */
When(/^atualizo a página$/,
  async () => {
    await browser
      .refresh()
      .waitForElementNotPresent('sd-context-loader')
  })

/**
 * @instance
 * @name When atualizo (\d+) vezes a página
 * @param {number} quantity The number of times that the the action will happen
 * @example When atualizo 2 vezes a página
 */
When(/^atualizo (\d+) vezes a página$/,
  async (quantity) => {
    for (let index = 0; index < quantity; index++) {
      await browser
        .refresh()
        .waitForElementNotPresent('sd-context-loader')
    }
  })

/**
 * @instance
 * @name When atualizo e aguardo (\d+) vezes por (\d+) segundos a página
 * @param {number} quantity The number of times that the the action will happen
 * @param {number} time The number of seconds to wait between checks
 * @example When atualizo e aguardo 5 vezes por 4 segundos a página
 */
When(/^atualizo e aguardo (\d+) vezes por (\d+) segundos a página$/,
  async (quantity, time) => {
    for (let index = 0; index < quantity; index++) {
      await browser
        .refresh()
        .waitForElementNotPresent('sd-context-loader')
        .pause(parseInt(time) * 1000)
    }
  })

/**
 * @instance
 * @name When realizo screenshot da tela atual
 * @example When realizo screenshot da tela atual
 */
When(/^realizo screenshot da tela atual$/,
  async (name) => {
    if (!name) return

    const filePath = './reports/screenshots/steps/telaAtual.png'

    await browser.saveScreenshot(filePath)
  })

/**
 * @instance
 * @name When realizo screenshot com nome "([^"]*)" da tela atual
 * @param {number} name File name without the extension
 * @example When realizo screenshot com nome "testeTealA" da tela atual
 */
When(/^realizo screenshot com nome "([^"]*)" da tela atual$/,
  async (name) => {
    if (!name) return

    const filePath = `./reports/screenshots/steps/${name}.png`

    await browser.saveScreenshot(filePath)
  })
