import { When } from '@cucumber/cucumber'
import { waitForLoadingNotPresent } from '../../../../utils/action.utils.js'
import { resolveAllText } from '../../../../utils/resolver.utils.js'
import { returnElementPropertyValue, returnElementText } from '../../../../utils/step.utils.js'

/**
 * @module steps/actions/url
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Navigate to a new URL or get the url from a element and navigate to it.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When acesso a url "([^"]*)"
 * @param {string} url Complete url to navigate
 * @example When acesso a url "https://www.google.com/"
 */
When(/^acesso a url "([^"]*)"$/,
  async url => {
    const textResolved = await resolveAllText(url)

    await browser.url(textResolved)
  })

/**
 * @instance
 * @name When acesso a url que está (no texto|na propriedade value) do elemento "([^"]*)"."([^"]*)"
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When acesso a url que está na propriedade value do elemento "@commons"."link"
 * @example When acesso a url que está no texto do elemento "@commons"."link"
 */
When(/^acesso a url que está (no texto|na propriedade value) do elemento "([^"]*)"."([^"]*)"$/,
  async (option, page, element) => {
    if (!page || !element) return

    const urlFound = option === 'no texto'
      ? await returnElementText(page, element)
      : await returnElementPropertyValue(page, element, 'value')

    await browser.url(urlFound)

    await waitForLoadingNotPresent()
  })
