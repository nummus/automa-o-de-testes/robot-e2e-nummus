import { When } from '@cucumber/cucumber'
import context from '../../../../context.js'

/**
 * @module steps/actions/window
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Retrieve the list of all window handles of the session to execute actions.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

const windowWait = 2000

/**
 * @instance
 * @name When acesso a janela (\d+)
 * @param {number} window Number of the window, the main window is recognized as 0, the next will be 1
 * @example When acesso a janela 1
 */
When(/^acesso a janela (\d+)$/,
  async (window) => {
    const windowInt = parseInt(window)

    await browser.pause(windowWait)

    const windowId = await browser.windowHandles()

    if (!windowId.length) return

    await browser
      .switchWindow(windowId[windowInt])
      .pause(windowWait)
      .resizeWindow(1920, 1080)
      .pause(windowWait)
  })

/**
 * @instance
 * @name When acesso a janela (\d+) no início da feature
 * @param {number} window Number of the window, the main window is recognized as 0, the next will be 1
 * @example When acesso a janela 1 no início da feature
 */
When(/^acesso a janela (\d+) no início da feature$/,
  async (window) => {
    if (!context.get('isNewFeature')) return

    const windowInt = parseInt(window)

    await browser.pause(windowWait)

    const windowId = await browser.windowHandles()

    if (!windowId.length) return

    await browser
      .switchWindow(windowId[windowInt])
      .pause(windowWait)
      .resizeWindow(1920, 1080)
      .pause(windowWait)
  })

/**
 * @instance
 * @name When acesso a janela (\d+) após (\d+) segundos
 * @param {number} window Number of the window, the main window is recognized as 0, the next will be 1
 * @param {number} time The number of seconds to wait between checks
 * @example When acesso a janela 1 após 2 segundos
 */
When(/^acesso a janela (\d+) após (\d+) segundos$/,
  async (window, time) => {
    const windowInt = parseInt(window)
    const waitTime = parseInt(time) * 1000

    await browser.pause(waitTime)

    const windowId = await browser.windowHandles()

    if (!windowId.length) return

    await browser
      .switchWindow(windowId[windowInt])
      .pause(waitTime)
      .resizeWindow(1920, 1080)
      .pause(waitTime)
  })

/**
 * @instance
 * @name When fecha a janela atual
 * @example When fecha a janela atual
 */
When(/^fecha a janela atual$/,
  async () => {
    await browser
      .pause(windowWait)
      .closeWindow()
  })

/**
 * @instance
 * @name When fecha a janela atual e acesso a janela (\d+)
 * @param {number} window Number of the window, the main window is recognized as 0, the next will be 1
 * @example When fecha a janela atual e acesso a janela 1
 */
When(/^fecha a janela atual e acesso a janela (\d+)$/,
  async (window) => {
    const windowInt = parseInt(window)

    await browser
      .pause(windowWait)
      .closeWindow()
      .pause(windowWait)

    const windowId = await browser.windowHandles()

    if (!windowId.length) return

    await browser
      .switchWindow(windowId[windowInt])
      .pause(windowWait)
      .resizeWindow(1920, 1080)
      .pause(windowWait)
  })

/**
 * @instance
 * @name When fecha a janela atual e acesso a janela inicial
 * @example When fecha a janela atual e acesso a janela inicial
 */
When(/^fecha a janela atual e acesso a janela inicial$/,
  async () => {
    await browser
      .pause(windowWait)
      .closeWindow()
      .pause(windowWait)

    const windowId = await browser.windowHandles()

    if (!windowId.length) return

    await browser
      .switchWindow(windowId[0])
      .pause(windowWait)
      .resizeWindow(1920, 1080)
      .pause(windowWait)
  })

/**
 * @instance
 * @name When fecha a janela atual e acesso a janela inicial no início da feature
 * @example When fecha a janela atual e acesso a janela inicial no início da feature
 */
When(/^fecha a janela atual e acesso a janela inicial no início da feature$/,
  async () => {
    if (!context.get('isNewFeature')) return

    await browser
      .pause(windowWait)
      .closeWindow()
      .pause(windowWait)

    const windowId = await browser.windowHandles()

    if (!windowId.length) return

    await browser
      .switchWindow(windowId[0])
      .pause(windowWait)
      .resizeWindow(1920, 1080)
      .pause(windowWait)
  })

/**
 * @instance
 * @name When redimensiono a janela para a resolução X (\d+) e Y (\d+)
 * @param {number} x The new window width
 * @param {number} y The new window height
 * @example When redimensiono a janela para a resolução X 1920 e Y 1080
 */
When(/^redimensiono a janela para a resolução X 1920 e Y 1080$/,
  async (x, y) => {
    const horizontal = parseInt(x)
    const vertical = parseInt(y)

    await browser
      .resizeWindow(horizontal, vertical)
      .pause(windowWait)
  })
