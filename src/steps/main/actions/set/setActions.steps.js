import { When } from '@cucumber/cucumber'
import { replaceWithOsKey } from '../../../../utils/step.utils.js'

/**
 * @module steps/actions/setActions
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Inserts a sequence of actions to type the provided key.
 *
 * The following keys names are:
 *
 * NULL, CANCEL, HELP, BACK_SPACE, TAB, CLEAR, RETURN, ENTER, SHIFT,
 * CONTROL, ALT, PAUSE, ESCAPE, SPACE, PAGE_UP, PAGE_DOWN, END, HOME,
 * ARROW_LEFT, LEFT, ARROW_UP, UP, ARROW_RIGHT, RIGHT, ARROW_DOWN, DOWN,
 * INSERT, DELETE, SEMICOLON, EQUALS, NUMPAD0, NUMPAD1, NUMPAD2, NUMPAD3,
 * NUMPAD4, NUMPAD5, NUMPAD6, NUMPAD7, NUMPAD8, NUMPAD9, MULTIPLY, ADD,
 * SEPARATOR, SUBTRACT, DECIMAL, DIVIDE, F1, F2, F3, F4, F5, F6, F7, F8,
 * F9, F10, F11, F12, COMMAND
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When pressiono e libero a tecla "([^"]*)"
 * @param {string} key Available key name that will be typed
 * @example When pressiono e libero a tecla "BACK_SPACE"
 */
When(/^pressiono e libero a tecla "([^"]*)"$/,
  async key => {
    const keyUpperCase = key.toUpperCase()

    await browser.perform(() => {
      const actions = browser.actions({ async: true })

      return actions.sendKeys(browser.Keys[keyUpperCase])
    })
  })

/**
 * @instance
 * @name When pressiono e libero (\d+) vezes a tecla "([^"]*)"
 * @param {number} quantity Number of times that the interaction will happen
 * @param {string} key Available key name that will be typed
 * @example When pressiono e libero 3 vezes a tecla "BACK_SPACE"
 */
When(/^pressiono e libero (\d+) vezes a tecla "([^"]*)"$/,
  async (quantity, key) => {
    const keyUpperCase = key.toUpperCase()

    for (let index = 0; index < quantity; index++) {
      await browser.perform(() => {
        const actions = browser.actions({ async: true })

        return actions.sendKeys(browser.Keys[keyUpperCase])
      })
    }
  })

/**
 * @instance
 * @name When pressiono a tecla "([^"]*)" com a tecla "([^"]*)" e libero as teclas
 * @param {string} key1 Available key name that will be typed
 * @param {string} key2 Available key name that will be typed
 * @example When pressiono a tecla "CONTROL" com a tecla "a" e libero as teclas
 */
When(/^pressiono a tecla "([^"]*)" com a tecla "([^"]*)" e em seguida libero as teclas$/,
  async (key1, key2) => {
    const keyUpperCase1 = replaceWithOsKey(key1)
    const keyUpperCase2 = replaceWithOsKey(key2)

    await browser.perform(() => {
      const actions = browser.actions({ async: true })

      return actions
        .keyDown(keyUpperCase1)
        .keyDown(keyUpperCase2)
        .keyUp(keyUpperCase2)
        .keyUp(keyUpperCase1)
    })
  })
