import { When } from '@cucumber/cucumber'
import { getPageSelector } from '../../../../utils/element.utils.js'

/**
 * @module steps/actions/setAttribute
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Change the element attribute to change the system behavior.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When altero para (habilitado|desabilitado|oculto) o elemento "([^"]*)"."([^"]*)"
 * @param {string} option type of action that will be executed
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When altero para habilitado o elemento "@commons"."input"
 * @example When altero para desabilitado o elemento "@commons"."input"
 * @example When altero para oculto o elemento "@commons"."input"
 */
When(/^altero para (habilitado|desabilitado|oculto) o elemento "([^"]*)"."([^"]*)"$/,
  async (option, page, element) => {
    const selector = getPageSelector(page, element)

    if (option === 'habilitado') {
      await browser
        .waitForElementPresent(selector)
        .setAttribute(selector, 'disabled', 'false')
    }

    if (option === 'desabilitado') {
      await browser
        .waitForElementPresent(selector)
        .setAttribute(selector, 'disabled', 'true')
    }

    if (option === 'oculto') {
      await browser
        .waitForElementPresent(selector)
        .setAttribute(selector, 'class', 'hidden')
    }
  })

/**
 * @instance
 * @name When altero todos os elementos "([^"]*)"."([^"]*)" para oculto
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When altero todos os elementos "@commons"."input" para oculto
 */
When(/^altero todos os elementos "([^"]*)"."([^"]*)" para oculto$/,
  async (page, element) => {
    const selector = getPageSelector(page, element)
    const resultElements = await browser.elements('css selector', selector)

    for (let index = 0; index < resultElements.length; index++) {
      await browser
        .waitForElementPresent(selector)
        .setAttribute({ selector, index }, 'class', 'hidden')
    }
  })

/**
 * @instance
 * @name When altero o atributo "([^"]*)" para o texto "([^"]*)" no elemento "([^"]*)"."([^"]*)"
 * @param {string} attribute The form element's attribute that the text will be set
 * @param {string} text Text that will be informed in the attribute
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When altero o atributo "disabled" para o texto "false" no elemento "@commons"."input"
 */
When(/^altero o atributo "([^"]*)" para o texto "([^"]*)" no elemento "([^"]*)"."([^"]*)"$/,
  async (attribute, text, page, element) => {
    if (!text) return

    const selector = getPageSelector(page, element)

    await browser
      .waitForElementPresent(selector)
      .setAttribute(selector, attribute, text)
  })
