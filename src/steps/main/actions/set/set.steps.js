import { When } from '@cucumber/cucumber'
import context from '../../../../context.js'
import { extractWebdriverId, getPageSelector } from '../../../../utils/element.utils.js'
import { textResolver } from '../../../../utils/resolver.utils.js'
import { updateTextSelector } from '../../../../utils/step.utils.js'

/**
 * @module steps/actions/set
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Sends some text to an element, upload a file or create a key in the local storage.
 *
 * 2. **About passing multiple resolvers in a text:**
 *
 * - You can inform multiple texts splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When realizo o upload do arquivo "([^"]*)" no "([^"]*)"."([^"]*)"
 * @param {string} fileName Name of the file inside '/files'
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When realizo o upload do arquivo "pacientes.png" no "@commons"."fileInput"
 */
When(/^realizo o upload do arquivo "([^"]*)" no "([^"]*)"."([^"]*)"$/,
  async (fileName, page, element) => {
    if (!fileName) return

    const selector = getPageSelector(page, element)
    const filePath = await textResolver(`@getFilePath(${fileName})`)

    await browser
      .waitForElementPresent(selector)
      .uploadFile(selector, filePath)
  })

/**
 * @instance
 * @name When informo o texto "([^"]*)" no "([^"]*)"."([^"]*)"
 * @param {string} text Text that will be informed in the element
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When informo o texto "teste" no "@commons"."input"
 */
When(/^informo o texto "([^"]*)" no "([^"]*)"."([^"]*)"$/,
  async (text, page, element) => {
    if (!text) return

    const textResolved = await textResolver(text)
    const selector = getPageSelector(page, element)

    if (!textResolved) return

    await browser.waitForElementPresent(selector)

    const updatedSelector = await updateTextSelector(selector)

    await browser.updateValue(updatedSelector, textResolved)
  })

/**
 * @instance
 * @name When informo o texto "([^"]*)" no "([^"]*)"."([^"]*)" na posição (\d+)
 * @param {string} text Text that will be informed in the element
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @example When informo o texto "teste" no "@commons"."input" na posição 1
 */
When(/^informo o texto "([^"]*)" no "([^"]*)"."([^"]*)" na posição (\d+)$/,
  async (text, page, element, index) => {
    if (!text) return

    const textResolved = await textResolver(text)
    const selector = getPageSelector(page, element)

    if (!textResolved) return

    await browser.waitForElementPresent(selector)

    const updatedSelector = await updateTextSelector(selector)

    await browser
      .updateValue({ selector: updatedSelector, index }, textResolved)
  })

/**
 * @instance
 * @name When informo e salvo em contexto o texto  "([^"]*)" no "([^"]*)"."([^"]*)"
 * @param {string} text Text that will be informed in the element
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When informo e salvo em contexto o texto  "teste" no "@commons"."input"
 */
When(/^informo e salvo em contexto o texto "([^"]*)" no "([^"]*)"."([^"]*)"$/,
  async (text, page, element) => {
    if (!text) return

    const textResolved = await textResolver(text)
    const selector = getPageSelector(page, element)

    if (!textResolved) return

    context.set(`${text}`, textResolved)

    await browser.waitForElementPresent(selector)

    const updatedSelector = await updateTextSelector(selector)

    await browser.updateValue(updatedSelector, textResolved)
  })

/**
 * @instance
 * @name When informo o texto "([^"]*)" no "([^"]*)"."([^"]*)" e pressiono a tecla TAB
 * @param {string} text Text that will be informed in the element
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When informo o texto "teste" no "@commons"."input" e pressiono a tecla TAB
 */
When(/^informo o texto "([^"]*)" no "([^"]*)"."([^"]*)" e pressiono a tecla TAB$/,
  async (text, page, element) => {
    if (!text) return

    const textResolved = await textResolver(text)
    const selector = getPageSelector(page, element)

    if (!textResolved) return

    await browser.waitForElementPresent(selector)

    const updatedSelector = await updateTextSelector(selector)

    await browser
      .updateValue(updatedSelector, textResolved)
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.sendKeys(browser.Keys.TAB)
      })
  })

/**
 * @instance
 * @name When informo o texto "([^"]*)" no "([^"]*)"."([^"]*)" na posição (\d+) e pressiono TAB
 * @param {string} text Text that will be informed in the element
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @example When informo o texto "teste" no "@commons"."input" na posição 1 e pressiono TAB
 */
When(/^informo o texto "([^"]*)" no "([^"]*)"."([^"]*)" na posição (\d+) e pressiono TAB$/,
  async (text, page, element, index) => {
    if (!text) return

    const textResolved = await textResolver(text)
    const selector = getPageSelector(page, element)

    if (!textResolved) return

    await browser.waitForElementPresent(selector)

    const updatedSelector = await updateTextSelector(selector)

    await browser
      .updateValue({ selector: updatedSelector, index }, textResolved)
      .perform(() => {
        const actions = browser.actions({ async: true })

        return actions.sendKeys(browser.Keys.TAB)
      })
  })

/**
 * @instance
 * @name When informo o texto "([^"]*)" no "([^"]*)"."([^"]*)" caso esteja presente
 * @param {string} text Text that will be informed in the element
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @example When informo o texto "teste" no "@commons"."input" caso esteja presente
 */
When(/^informo o texto "([^"]*)" no "([^"]*)"."([^"]*)" caso esteja presente$/,
  async (text, page, element) => {
    if (!text) return

    const selector = getPageSelector(page, element)

    const updatedSelector = await updateTextSelector(selector)

    const resultElements = await browser.elements('css selector', updatedSelector)

    if (resultElements.length) return

    const textResolved = await textResolver(text)

    if (!textResolved) return

    const webdriverId = extractWebdriverId(resultElements, true)

    await browser.elementIdValue(webdriverId, textResolved)
  })

/**
 * @instance
 * @name When informo o texto "([^"]*)" no "([^"]*)"."([^"]*)" na posição (\d+) caso esteja presente
 * @param {string} text Text that will be informed in the element
 * @param {string} page Name of the .page.js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in .page.js where the element's css selector is stored
 * @param {number} index Index of the element in the page
 * @example When informo o texto "teste" no "@commons"."input" na posição 1 caso esteja presente
 */
When(/^informo o texto "([^"]*)" no "([^"]*)"."([^"]*)" na posição (\d+) caso esteja presente$/,
  async (text, page, element, index) => {
    if (!text) return

    const selector = getPageSelector(page, element)

    const updatedSelector = await updateTextSelector(selector)

    const resultElements = await browser.elements('css selector', updatedSelector)

    if (!resultElements[index]) return

    const textResolved = await textResolver(text)

    if (!textResolved) return

    const webdriverId = extractWebdriverId(resultElements)

    await browser.elementIdValue(webdriverId[index], textResolved)
  })

/**
 * @instance
 * @name When crio a key do local storage "([^"]*)" com o valor "([^"]*)"
 * @param {string} key Local storage key name to set
 * @param {string} value Local storage value to the key
 * @example When crio a key do local storage "origem" com o valor "direct"
 */
When(/^crio a key do local storage "([^"]*)" com o valor "([^"]*)"$/,
  async (key, value) => {
    await browser.setLocalStorageKey(key, value)
  })
