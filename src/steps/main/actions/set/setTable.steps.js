import { When } from '@cucumber/cucumber'
import { tableDrivenToObjects } from '../../../../utils/cucumber.utils.js'
import { getPageSelector } from '../../../../utils/element.utils.js'

/**
 * @module steps/actions/setTable
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Assigning the needed information in a table driven, execute a event on the element.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name When oculto todos os seguintes elementos:
 * @param {dataTable} rawTable Cucumber table with the name of the .page.js and the object property where the element's css selector is stored
 * @example When oculto todos os seguintes elementos:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | list     |
 */
When(/^oculto todos os seguintes elementos:$/,
  async ({ rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      const cssSelector = getPageSelector(line.page, line.selector)

      await browser
        .waitForElementPresent(cssSelector)
        .setAttribute(cssSelector, 'class', 'hidden')
    }
  })

/**
 * @instance
 * @name When crio as seguintes keys com os valores do local storage:
 * @param {dataTable} rawTable Cucumber table with local storage key name to set and value to the key
 * @example When crio as seguintes keys com os valores do local storage:
 * | key    | value  |
 * | origem | direct |
 */
When(/^crio as seguintes keys com os valores do local storage:$/,
  async ({ rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      await browser.setLocalStorageKey(line.key, line.value)
    }
  })
