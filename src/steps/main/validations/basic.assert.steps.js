import { Then } from '@cucumber/cucumber'
import { getPageSelector } from '../../../utils/element.utils.js'

/**
 * @module steps/validations/basic/assert
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Assert if a element is present or not present in the page with the desired state.
 *
 * 2. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name Then (não está|está) presente o "([^"]*)"."([^"]*)"
 * @param {string} option type of validation that will be executed
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @example Then está presente o "@commons"."list"
 * @example Then não está presente o "@commons"."list"
 */
Then(/^(não está|está) presente o "([^"]*)"."([^"]*)"$/,
  async (option, page, element) => {
    const selector = getPageSelector(page, element)

    option === 'não está'
      ? await browser.assert.not.elementPresent(selector)
      : await browser.assert.elementPresent(selector)
  })

/**
 * @instance
 * @name Then (não está|está) presente o "([^"]*)"."([^"]*)" (habilitado|visível)
 * @param {string} option type of validation that will be executed
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @param {string} state type of the validation that will be executed
 * @example Then está presente o "@commons"."list" visível
 * @example Then não está presente o "@commons"."list" visível
 * @example Then está presente o "@commons"."list" habilitado
 * @example Then não está presente o "@commons"."list" habilitado
 */
Then(/^(não está|está) presente o "([^"]*)"."([^"]*)" (habilitado|visível)$/,
  async (option, page, element, state) => {
    const selector = getPageSelector(page, element)
    const assertType = state === 'habilitado' ? 'enabled' : 'visible'

    option === 'não está'
      ? await browser.assert.not[assertType](selector)
      : await browser.assert[assertType](selector)
  })

/**
 * @instance
 * @name Then está presente o "([^"]*)"."([^"]*)" um total de (\d+) vezes
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @param {number} quantity Number of times a element must be present in the page
 * @example Then está presente o "@commons"."list" um total de 2 vezes
 */
Then(/^está presente o "([^"]*)"."([^"]*)" um total de (\d+) vezes$/,
  async (page, element, quantity) => {
    const selector = getPageSelector(page, element)

    if (quantity) {
      await browser
        .waitForElementPresent(selector)
        .assert.elementsCount(selector, quantity)
    } else {
      await browser.waitForElementNotPresent(selector)
    }
  })
