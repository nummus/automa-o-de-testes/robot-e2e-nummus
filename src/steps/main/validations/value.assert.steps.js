import { Then } from '@cucumber/cucumber'
import { getPageSelector } from '../../../utils/element.utils.js'
import { resolveAllText } from '../../../utils/resolver.utils.js'

/**
 * @module steps/validations/value/assert
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Assert if a element is present or not in the page with the desired state and form element's value.
 *
 * 2. **About passing multiple resolvers in a value:**
 *
 * - You can inform multiple values splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name Then (não está|está) presente o "([^"]*)"."([^"]*)" com a propriedade value (igual|contendo) "(.*)"
 * @param {string} option type of validation that will be executed
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @param {string} type type of text validation that will be executed
 * @param {string} value Complete form element's value that will receive the interaction
 * @example Then está presente o "@commons"."list" com a propriedade value igual "(.*)"
 * @example Then não está presente o "@commons"."list" com a propriedade value igual "(.*)"
 * @example Then está presente o "@commons"."list" com a propriedade value contendo "(.*)"
 * @example Then não está presente o "@commons"."list" com a propriedade value contendo "(.*)"
 */
Then(/^(não está|está) presente o "([^"]*)"."([^"]*)" com a propriedade value (igual|contendo) "(.*)"$/,
  async (option, page, element, type, value) => {
    if (!value) return

    const isEquals = type !== 'contendo'
    const selector = getPageSelector(page, element)
    const valueResolved = await resolveAllText(value)

    if (!valueResolved) return

    option === 'não está'
      ? await browser.notElementValue({ selector, value: valueResolved, isEquals })
      : await browser.elementValue({ selector, value: valueResolved, isEquals })
  })

/**
 * @instance
 * @name Then (não está|está) presente o "([^"]*)"."([^"]*)" com a propriedade "([^"]*)" (igual|contendo) "(.*)"
 * @param {string} option type of validation that will be executed
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @param {string} property The form element's property
 * @param {string} type type of text validation that will be executed
 * @param {string} value Partial form element's value that will receive the interaction
 * @example Then está presente o "@commons"."button" com a propriedade "Value" igual "Salvar"
 * @example Then não está presente o "@commons"."button" com a propriedade "Value" igual "Salvar"
 * @example Then está presente o "@commons"."button" com a propriedade "Value" contendo "Salvar"
 * @example Then não está presente o "@commons"."button" com a propriedade "Value" contendo "Salvar"
 */
Then(/^(não está|está) presente o "([^"]*)"."([^"]*)" com a propriedade "([^"]*)" (igual|contendo) "(.*)"$/,
  async (option, page, element, property, type, value) => {
    if (!property || !value) return

    const selector = getPageSelector(page, element)
    const valueResolved = await resolveAllText(value)
    const assertType = type === 'contendo' ? 'domPropertyContains' : 'domPropertyEquals'

    if (!valueResolved) return

    option === 'não está'
      ? await browser.assert.not[assertType](selector, property, valueResolved)
      : await browser.assert[assertType](selector, property, valueResolved)
  })
