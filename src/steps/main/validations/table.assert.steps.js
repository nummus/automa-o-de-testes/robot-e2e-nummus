import { Then } from '@cucumber/cucumber'
import { tableDrivenToObjects } from '../../../utils/cucumber.utils.js'
import { getPageSelector } from '../../../utils/element.utils.js'
import { resolveAllText } from '../../../utils/resolver.utils.js'

/**
 * @module steps/validations/table/assert
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Assigning the needed information in a table driven,
 * than assert if a element is present or not in the page with the desired state and texts.
 *
 * 2. **About passing multiple resolvers in a text or value:**
 *
 * - You can inform multiple texts or values splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name Then (não está|está) presente os seguintes elementos:
 * @param {string} option type of validation that will be executed
 * @param {dataTable} rawTable Cucumber table with the name of the Page.Js and the object property where the element's css selector is stored
 * @example Then está presente os seguintes elementos:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | list     |
 * @example Then não está presente os seguintes elementos:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | list     |
 */
Then(/^(não está|está) presente os seguintes elementos:$/,
  async (option, { rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      const selector = getPageSelector(line.page, line.selector)

      option === 'não está'
        ? await browser.assert.not.elementPresent(selector)
        : await browser.assert.elementPresent(selector)
    }
  })

/**
 * @instance
 * @name Then (não está|está) presente os seguintes campos com alerta de obrigatório:
 * @param {string} option type of validation that will be executed
 * @param {dataTable} rawTable Cucumber table with the name of the Page.Js and the object property where the element's css selector is stored
 * @example Then está presente os seguintes campos com alerta de obrigatório:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | list     |
 * @example Then não está presente os seguintes campos com alerta de obrigatório:
 * | page     | selector |
 * | @commons | button   |
 * | @commons | list     |
 */
Then(/^(não está|está) presente os seguintes campos com alerta de obrigatório:$/,
  async (option, { rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)

    for (const line of objectTable) {
      const selector = getPageSelector(line.page, line.selector)

      option === 'não está'
        ? await browser.assert.not.elementPresent(`${selector}.field-error`)
        : await browser.assert.elementPresent(`${selector}.field-error`)
    }
  })

/**
 * @instance
 * @name Then (não está|está) presente os elementos com os seguintes textos (iguais|contendo):
 * @param {string} option type of validation that will be executed
 * @param {string} type type of text validation that will be executed
 * @param {dataTable} rawTable Cucumber table with the page name, element name and complete text
 * @example Then está presente os elementos com os seguintes textos iguais:
 * | page     | selector | text   |
 * | @commons | button   | Salvar |
 * | @commons | list     | Teste  |
 * @example Then não está presente os elementos com os seguintes textos iguais:
 * | page     | selector | text   |
 * | @commons | button   | Salvar |
 * | @commons | list     | Teste  |
 * @example Then está presente os elementos com os seguintes textos contendo:
 * | page     | selector | text   |
 * | @commons | button   | Salvar |
 * | @commons | list     | Teste  |
 * @example Then não está presente os elementos com os seguintes textos contendo:
 * | page     | selector | text   |
 * | @commons | button   | Salvar |
 * | @commons | list     | Teste  |
 */
Then(/^(não está|está) presente os elementos com os seguintes textos (iguais|contendo):$/,
  async (option, type, { rawTable }) => {
    const objectTable = await tableDrivenToObjects(rawTable)
    const isEquals = type !== 'contendo'

    for (const line of objectTable) {
      const selector = getPageSelector(line.page, line.selector)
      const textResolved = await resolveAllText(line.text)

      if (!textResolved) continue

      option === 'não está'
        ? await browser.notElementText({ selector, text: textResolved, isEquals })
        : await browser.elementText({ selector, text: textResolved, isEquals })
    }
  })

/**
 * @instance
 * @name Then (não está|está) presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo):
 * @param {string} option type of validation that will be executed
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @param {string} type type of text validation that will be executed
 * @param {dataTable} rawTable Cucumber table with the element complete text that is passed under the step
 * @example Then está presente o "@commons"."list" com os seguintes textos iguais:
 * | text   |
 * | Salvar |
 * | Teste  |
 * @example Then não está presente o "@commons"."list" com os seguintes textos iguais:
 * | text   |
 * | Salvar |
 * | Teste  |
 * @example Then está presente o "@commons"."list" com os seguintes textos contendo:
 * | text   |
 * | Salvar |
 * | Teste  |
 * @example Then não está presente o "@commons"."list" com os seguintes textos contendo:
 * | text   |
 * | Salvar |
 * | Teste  |
 */
Then(/^(não está|está) presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo):$/,
  async (option, page, element, type, { rawTable }) => {
    const selector = getPageSelector(page, element)
    const objectTable = await tableDrivenToObjects(rawTable)
    const isEquals = type !== 'contendo'

    for (const line of objectTable) {
      const textResolved = await resolveAllText(line.text)

      if (!textResolved) continue

      option === 'não está'
        ? await browser.notElementText({ selector, text: textResolved, isEquals })
        : await browser.elementText({ selector, text: textResolved, isEquals })
    }
  })

/**
 * @instance
 * @name Then (não está|está) presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo) se "([^"]*)" for verdadeiro:
 * @param {string} option type of validation that will be executed
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @param {string} type type of text validation that will be executed
 * @param {string} shouldContinue Enable or disable the step if a scenario do not need it
 * @param {dataTable} rawTable Cucumber table with the element complete text that is passed under the step
 * @example Then está presente o "@commons"."list" com os seguintes textos iguais se "Sim" for verdadeiro:
 * | text   |
 * | Salvar |
 * | Teste  |
 * @example Then não está presente o "@commons"."list" com os seguintes textos iguais se "Sim" for verdadeiro:
 * | text   |
 * | Salvar |
 * | Teste  |
 * @example Then está presente o "@commons"."list" com os seguintes textos contendo se "Sim" for verdadeiro:
 * | text   |
 * | Salvar |
 * | Teste  |
 * @example Then não está presente o "@commons"."list" com os seguintes textos contendo se "Sim" for verdadeiro:
 * | text   |
 * | Salvar |
 * | Teste  |
 */
Then(/^(não está|está) presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo) se "([^"]*)" for verdadeiro:$/,
  async (option, page, element, type, shouldContinue, { rawTable }) => {
    if (!shouldContinue) return

    const selector = getPageSelector(page, element)
    const objectTable = await tableDrivenToObjects(rawTable)
    const isEquals = type !== 'contendo'

    for (const line of objectTable) {
      const textResolved = await resolveAllText(line.text)

      if (!textResolved) continue

      option === 'não está'
        ? await browser.notElementText({ selector, text: textResolved, isEquals })
        : await browser.elementText({ selector, text: textResolved, isEquals })
    }
  })

/**
 * @instance
 * @name Then está presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo) na ordem:
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @param {string} type type of text validation that will be executed
 * @param {dataTable} rawTable Cucumber table with the element complete text that is passed under the step
 * @example Then está presente o "@commons"."list" com os seguintes textos iguais na ordem:
 * | text     |
 * | Salvar   |
 * | Cancelar |
 * @example Then está presente o "@commons"."list" com os seguintes textos contendo na ordem:
 * | text     |
 * | Salvar   |
 * | Cancelar |
 */
Then(/^está presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo) na ordem:$/,
  async (page, element, type, { rawTable }) => {
    let isEquals = true
    const selector = getPageSelector(page, element)
    const objectTable = await tableDrivenToObjects(rawTable)

    if (type === 'contendo') isEquals = false

    for (let index = 0; index < objectTable.length; index++) {
      const textResolved = await resolveAllText(objectTable[index].text)

      if (!textResolved) continue

      await browser.elementText({ selector, text: textResolved, index, isEquals })
    }
  })

/**
 * @instance
 * @name Then (não está|está) presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo) na propriedade value:
 * @param {string} option type of validation that will be executed
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @param {string} type type of text validation that will be executed
 * @param {dataTable} rawTable Cucumber table with form element's complete value that is passed under the step
 * @example Then está presente o "@commons"."list" com os seguintes textos iguais na propriedade value:
 * | value    |
 * | Salvar   |
 * | Cancelar |
 * @example Then não está presente o "@commons"."list" com os seguintes textos iguais na propriedade value:
 * | value    |
 * | Salvar   |
 * | Cancelar |
 * @example Then está presente o "@commons"."list" com os seguintes textos contendo na propriedade value:
 * | value    |
 * | Salvar   |
 * | Cancelar |
 * @example Then não está presente o "@commons"."list" com os seguintes textos contendo na propriedade value:
 * | value    |
 * | Salvar   |
 * | Cancelar |
 */
Then(/^(não está|está) presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo) na propriedade value:$/,
  async (option, page, element, type, { rawTable }) => {
    const selector = getPageSelector(page, element)
    const objectTable = await tableDrivenToObjects(rawTable)
    const isEquals = type !== 'contendo'

    for (const line of objectTable) {
      const valueResolved = await resolveAllText(line.value)

      if (!valueResolved) continue

      option === 'não está'
        ? await browser.notElementValue({ selector, value: valueResolved, isEquals })
        : await browser.elementValue({ selector, value: valueResolved, isEquals })
    }
  })

/**
 * @instance
 * @name Then está presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo) na propriedade value em ordem:
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @param {string} type type of text validation that will be executed
 * @param {dataTable} rawTable Cucumber table with form element's partial value that is passed under the step
 * @example Then está presente o "@commons"."list" com os seguintes textos iguais na propriedade value em ordem:
 * | value    |
 * | Salvar   |
 * | Cancelar |
 * @example Then está presente o "@commons"."list" com os seguintes textos contendo na propriedade value em ordem:
 * | value    |
 * | Salvar   |
 * | Cancelar |
 */
Then(/^está presente o "([^"]*)"."([^"]*)" com os seguintes textos (iguais|contendo) na propriedade value em ordem:$/,
  async (page, element, type, { rawTable }) => {
    let isEquals = true
    const selector = getPageSelector(page, element)
    const objectTable = await tableDrivenToObjects(rawTable)

    if (type === 'contendo') isEquals = false

    for (let index = 0; index < objectTable.length; index++) {
      const valueResolved = await resolveAllText(objectTable[index].value)

      if (!valueResolved) continue

      await browser.elementValue({ selector, value: valueResolved, index, isEquals })
    }
  })
