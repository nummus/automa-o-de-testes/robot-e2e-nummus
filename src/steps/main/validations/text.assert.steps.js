import { Then } from '@cucumber/cucumber'
import { getPageSelector } from '../../../utils/element.utils.js'
import { resolveAllText } from '../../../utils/resolver.utils.js'

/**
 * @module steps/validations/text/assert
 * @description
 *
 * 1. **What is the purpose of these steps?**
 *
 * - Assert if a element is present or not in the page with the desired state and text.
 *
 * 2. **About passing multiple resolvers in a text:**
 *
 * - You can inform multiple texts splitted by ";",
 * the robot will resolve then separately, like the following example:
 *
 * - ```@gerarCpf();@getTime(D)``` will result into ```865.327.101-56 15/02/2023```
 *
 * 3. **About cucumber steps keywords:**
 *
 * - {@link https://cucumber.io/docs/gherkin/reference/#keywords}
 */

/**
 * @instance
 * @name Then (não está|está) presente o "([^"]*)"."([^"]*)" com o texto (igual|contendo) "(.*)"
 * @param {string} option type of validation that will be executed
 * @param {string} page Name of the Page.Js located in the pages folder where the element's css selector is located
 * @param {string} element Name of an object property in Page.Js where the element's css selector is stored
 * @param {string} type type of text validation that will be executed
 * @param {string} text Complete text of the element that will receive the interaction
 * @example Then está presente o "@commons"."button" com o texto igual "Salvar"
 * @example Then não está presente o "@commons"."button" com o texto igual "Salvar"
 * @example Then está presente o "@commons"."button" com o texto contendo "Salvar"
 * @example Then não está presente o "@commons"."button" com o texto contendo "Salvar"
 */
Then(/^(não está|está) presente o "([^"]*)"."([^"]*)" com o texto (igual|contendo) "(.*)"$/,
  async (option, page, element, type, text) => {
    if (!text) return

    const selector = getPageSelector(page, element)
    const textResolved = await resolveAllText(text)

    if (!textResolved) return

    const isEquals = type !== 'contendo'

    option === 'não está'
      ? await browser.notElementText({ selector, text: textResolved, isEquals })
      : await browser.elementText({ selector, text: textResolved, isEquals })
  })
