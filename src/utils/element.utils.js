import { globSync } from 'glob'
import objectPath from 'object-path'
import { join, resolve } from 'path'
import context from '../context.js'
import { getConfig } from './base.utils.js'
import { robotErrorHandler } from './report.utils.js'
import { removeSpacesAndSemicolons } from './step.utils.js'

export const normalizeId = id => {
  return id.replace(/:/g, '\\:')
}

export const normalizeElement = (element) => {
  return element.match(/^@/) ? element : `@${element}`
}

export const extractWebdriverId = (result, returnSingle = false) => {
  const webdriverIds = result.value || result

  if (!webdriverIds) return result

  if (Array.isArray(webdriverIds) && webdriverIds.length) {
    const webdriverIdsArray = webdriverIds
      .filter(Boolean)
      .map((element) => Object.values(element)[0])

    return returnSingle ? webdriverIdsArray[0] : webdriverIdsArray
  }

  return Object.values(webdriverIds).toString()
}

const getPage = (browser, page) => {
  if (!page.startsWith('@')) return browser.page[page]()

  const pagesPath = resolve(getConfig('pagesPath'))
  const fileName = `${page.slice(1)}.page.js`
  const joinPath = join(pagesPath, '**', fileName)
  const filesPaths = globSync(joinPath.replace(/\\/g, '/'))

  if (!filesPaths.length) {
    throw new Error(`Page not found: ${page}`)
  }

  if (filesPaths.length > 1) {
    throw new Error(`More than one page found with the same name: ${page}`)
  }

  let pathInArray = []
  const filePath = filesPaths[0]
  const pageName = fileName.replace(/\/|\.js/g, '')
  const folderName = filePath.replace(pagesPath, '').replace(fileName, '')

  if (folderName) {
    let typeSplit = '/'

    if (context.get('os') === 'windows') {
      typeSplit = '\\'
    }

    pathInArray = folderName.split(typeSplit).filter(item => item !== '')
  }

  pathInArray.push(pageName)

  return objectPath.get(browser.page, pathInArray)()
}

export const getPageSelector = (page, targetElement) => {
  if (!page || !targetElement) {
    throw new Error('You need to inform page and element!')
  }

  const pageBrowser = getPage(browser, page)
  const elementKey = targetElement.replace('@', '')
  const element = pageBrowser.elements[elementKey]

  if (!element) {
    throw new Error(`No element found with the key: ${elementKey}`)
  }

  return element.selector
}

export const simplifyPage = pageElements => {
  return Object.fromEntries(
    Object.entries(pageElements)
      .map(([key, val]) => [key, Object.values(val)[0]])
  )
}

export const getWebdriverIdByPageElement = async (page, element, returnSingle = true) => {
  try {
    if (!page) throw new Error('Page is mandatory')
    if (!element) throw new Error('Element is mandatory')

    const selector = getPageSelector(page, element)

    await browser.waitForElementPresent(selector)

    const webdriverIds = await browser.elements('css selector', selector)

    const webdriverId = extractWebdriverId(webdriverIds, returnSingle)

    return webdriverId
  } catch (error) {
    const err = { error, functionName: 'getWebdriverIdByPageElement' }

    await robotErrorHandler(err)
  }
}

export const getWebdriverIdCssSelector = async (selector, returnSingle = true) => {
  try {
    if (!selector) throw new Error('Selector is mandatory')

    await browser.waitForElementPresent(selector)

    const webdriverIds = await browser.elements('css selector', selector)

    const webdriverId = extractWebdriverId(webdriverIds, returnSingle)

    return webdriverId
  } catch (error) {
    const err = { error, functionName: 'getWebdriverIdCssSelector' }

    await robotErrorHandler(err)
  }
}

export const getWebdriverIdBySonElementValue = async (fatherSelector, sonSelector, targetSelector, value) => {
  try {
    if (!fatherSelector) throw new Error('Father selector is mandatory')
    if (!sonSelector) throw new Error('Son selector is mandatory')
    if (!targetSelector) throw new Error('Target selector is mandatory')
    if (!value) throw new Error('Value is mandatory')

    let targetFatherId = null
    const fatherIds = await getWebdriverIdCssSelector(fatherSelector, false)

    for (const id of fatherIds) {
      let sonId = await browser.elementIdElements(id, 'css selector', sonSelector)
      sonId = await extractWebdriverId(sonId, true)

      const sonValue = await browser.elementIdProperty(sonId, 'value')

      const valueFound = removeSpacesAndSemicolons(sonValue)
      const valueReceived = removeSpacesAndSemicolons(value)

      if (valueFound === valueReceived) {
        targetFatherId = id

        break
      }
    }

    let targetId = await browser.elementIdElements(targetFatherId, 'css selector', targetSelector)

    targetId = await extractWebdriverId(targetId, true)

    return targetId
  } catch (error) {
    const err = { error, functionName: 'getWebdriverIdBySonElementValue' }

    await robotErrorHandler(err)
  }
}
