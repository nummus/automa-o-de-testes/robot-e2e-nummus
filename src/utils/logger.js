import chalk from 'chalk'
import { properties } from '../config.js'

class Logger {
  constructor () {
    this.logLevel = 'warn'
    this.typeLoFormat = [
      `${chalk.blue.bold('RESOLVER')}`,
      `${chalk.cyan.bold('HOOKS')}`,
      `${chalk.green.bold('POST')}`,
      `${chalk.hex('#FF8800').bold('PUT')}`,
      `${chalk.red.bold('DELETE')}`,
      `${chalk.magenta.bold('GET')}`,
      `${chalk.hex('#CCCCCC').bold('SQL')}`,
      `${chalk.yellow.bold('COMMAND')}`,
      chalk.bold.green('✔'),
      `${chalk.blue.bold('FUNCTION')}`,
      chalk.bold.blue('ℹ'),
      chalk.bold.yellow('!!'),
      chalk.bold.red('✖'),
      chalk.bold.green('✔')
    ]
    this.levels = ['error', 'info', 'warn', 'debug', 'silly']
    this.logMethods = [
      `${chalk.red('ERROR')} `,
      '',
      `${chalk.yellow('WARN')} `,
      `${chalk.hex('#FF8800').bold('DEBUG')} `,
      ''
    ]
    this.typeLog = [
      'resolver', 'hooks', 'post', 'put', 'delete',
      'get', 'sql', 'command', 'action', 'function',
      'info', 'warn', 'error', 'actionNotSpace'
    ]
  }

  setLogLevel () {
    this.logLevel = properties.get('log.level') || this.logLevel
  }

  setFunctionLevel () {
    this.funcLevel = properties.get('log.function') || 'Não deve logar'
  }

  log (type, message, result = '', level = 'warn') {
    this.setLogLevel()
    this.setFunctionLevel()

    const functionLevel = message.includes(this.funcLevel)

    if (functionLevel) this.logLevel = 'debug'

    const indexLevel = this.levels.indexOf(level)
    const currentLevel = this.levels.indexOf(this.logLevel)

    if (indexLevel > currentLevel) return

    let formatMessage = chalk.cyan(`${message}`)
    const indexTypeLog = this.typeLog.indexOf(type)
    const formatPrefix = `${this.logMethods[indexLevel]}${this.typeLoFormat[indexTypeLog]}`

    if (indexLevel === 0) {
      formatMessage = chalk.red(`${message}`)
    }

    if (indexLevel === 2) {
      formatMessage = chalk.yellow(`${message}`)
    }

    if (indexLevel === 3) {
      formatMessage = chalk.hex('#FF8800').bold(`${message}`)
    }

    if (indexTypeLog !== 13) console.log('')
    console.log(formatPrefix, formatMessage)

    if (!result) return

    if (typeof result === 'object') {
      result = `${JSON.stringify(result, null, 3)}`
    }

    console.log(chalk.magenta('Result →→'), chalk.white(`${result}`), chalk.magenta('←← Result'))
  }

  error (type, message, result = '') {
    this.log(type, message, result, 'error')
  }

  info (type, message, result = '') {
    this.log(type, message, result, 'info')
  }

  warn (type, message, result = '') {
    this.log(type, message, result, 'warn')
  }

  debug (type, message, result = '') {
    this.log(type, message, result, 'debug')
  }
}

const logger = new Logger()

export default logger
