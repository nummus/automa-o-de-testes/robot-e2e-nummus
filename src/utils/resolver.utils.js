import fs from 'fs'
import objectPath from 'object-path'
import { join } from 'path'
import { properties } from '../config.js'
import defaultResolver from '../resolvers/defaultResolver.js'

export const getResolver = (name) => {
  const customPath = join(
    properties.getRaw('resolvers.path'), `${name}.js`
  )
  const standardPath = join(
    __dirname, '..', 'resolvers', `${name}.js`
  )

  const isCustom = fs.existsSync(customPath)
  const isStandard = fs.existsSync(standardPath)

  if (isCustom && !isStandard) {
    return require(customPath)
  } else if (isStandard && !isCustom) {
    return require(standardPath)
  }

  return require(customPath)
}

export const executeResolver = async ({ type = '@', name = 'undefined', params = '', path = '' }) => {
  const foundFunction = type === '@' ? getResolver(name) : defaultResolver

  if (!foundFunction) throw new Error(`Resolver ${name} not exist!`)

  const parsedParams = params.split(',').map((param) => param.trim())
  const result = await foundFunction(...parsedParams)

  if (!result) return result

  return objectPath.get(result, path)
}

export const textResolver = async (resolver) => {
  if (!resolver || typeof resolver !== 'string') return resolver

  const match = resolver.match(/^(@|\/)(([\w-/]+)\((.*)?\))(:(.*))?$/i)

  if (!match) return resolver

  const type = match[1] || '@'
  const name = match[3] || 'undefined'
  const params = match[4]
  const path = match[6] || ''

  return await executeResolver({ type, name, params, path })
}

export const resolveAllText = async (value, splitType = ';') => {
  let valueSolved = ''
  const valueList = value.split(splitType)

  for (const value of valueList) {
    const valueResult = value || ''

    valueSolved += (await textResolver(valueResult.trim())) || ''
  }

  return valueSolved
}

export const plainJsonResolver = async (object) => {
  if (Array.isArray(object)) {
    for (let index = 0; index < object.length; index++) {
      if (typeof object[index] === 'string') {
        const value = await textResolver(object[index])
        object[index] = value

        return
      }

      if (typeof object[index] === 'object') {
        await plainJsonResolver(object[index])
      }
    }

    return
  }

  for (const key in object) {
    if (typeof object[key] === 'string') {
      const value = await textResolver(object[key])
      object[key] = value
    }

    if (typeof object[key] === 'object') {
      await plainJsonResolver(object[key])
    }
  }
}
