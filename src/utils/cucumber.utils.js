import context from '../context.js'
import { textResolver } from './resolver.utils.js'

export const tableDrivenToQuery = (tableDriven) => {
  const tableValues = []
  const tableHeader = tableDriven[0]

  for (let index = 1, length = tableDriven.length; index < length; index++) {
    const tableLine = tableDriven[index]
    const row = tableLine.slice(0, Math.min(tableLine.length, tableHeader.length))

    tableValues.push(row.fill(null, row.length, tableHeader.length))
  }

  return { tableHeader, tableValues }
}

export const tableDrivenToObjects = async (tableDriven) => {
  const [headers, ...values] = tableDriven
  const objects = []

  for (const row of values) {
    const object = {}

    for (const [index, value] of row.entries()) {
      object[headers[index]] = await textResolver(value)
    }

    objects.push(object)
  }

  return objects
}

export const initializeContext = async () => {
  context.set('tags', [])
  context.set('totalScenarios', 0)
  context.set('isBeforeAllCalled', false)
  context.set('isCoreBeforeAllCalled', false)
  context.set('isFirstScenario', false)
  context.set('isLastScenario', false)
  context.set('isNewFeature', false)
  context.set('isNewScenario', false)
  context.set('currentFeatureFile', null)
  context.set('currentScenarioName', null)
  context.set('currentScenarioNumber', 0)
  context.set('totalScenariosInFeature', 0)
  context.set('currentScenarioInFeature', 0)
  context.set('isFeatureLastScenario', false)
}

export const updateFeatureAndScenarioData = (gherkinDocument, pickle) => {
  const { uri, feature } = gherkinDocument

  if (uri) {
    context.set('isNewFeature', context.get('currentFeatureFile') !== uri)
    context.set('currentFeatureFile', uri)
  }

  if (pickle?.name) {
    context.set('isNewScenario', context.get('currentScenarioName') !== pickle.name)
    context.set('currentScenarioName', pickle.name)
  }

  if (!feature) {
    throw new Error('conf.before: Feature file not found')
  }

  return feature
}

export const countScenariosInFeature = feature => {
  let totalScenariosInFeature = 0

  for (const child of feature.children) {
    const { scenario } = child

    if (!scenario) continue

    const isScenarioOutline = scenario?.keyword === 'Scenario Outline'

    totalScenariosInFeature += isScenarioOutline
      ? scenario.examples[0].tableBody.length
      : 1
  }

  return totalScenariosInFeature
}

export const updateCurrentScenarioData = (feature, pickle) => {
  let totalScenarios = 0

  for (const child of feature.children) {
    const { scenario } = child

    if (!scenario) continue

    if (scenario.name === pickle.name) {
      totalScenarios = scenario?.keyword === 'Scenario Outline'
        ? scenario.examples[0].tableBody.length
        : 1

      break
    }
  }

  return totalScenarios
}
