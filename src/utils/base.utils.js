import Nightwatch from 'nightwatch'
import os from 'os'
import { resolve } from 'path'
import context from '../context.js'

/* eslint-disable camelcase */
export const startTest = async (client, pickle) => {
  const webdriver = {}

  if (client.parameters['webdriver-host']) {
    webdriver.host = client.parameters['webdriver-host']
  }

  if (client.parameters['webdriver-port']) {
    webdriver.port = client.parameters['webdriver-port']
  }

  if (typeof client.parameters['start-process'] !== 'undefined') {
    webdriver.start_process = client.parameters['start-process']
  }

  let persist_globals

  if (client.parameters['persist-globals']) {
    persist_globals = client.parameters['persist-globals']
  }

  const globals = {}

  if (client.parameters['retry-interval']) {
    globals.waitForConditionPollInterval = client.parameters['retry-interval']
  }

  client.client = Nightwatch.createClient({
    headless: client.parameters.headless,
    env: client.parameters.env,
    timeout: client.parameters.timeout,
    parallel: !!client.parameters.parallel,
    output: !client.parameters['disable-output'],
    enable_global_apis: true,
    silent: !client.parameters.verbose,
    always_async_commands: true,
    webdriver,
    persist_globals,
    config: client.parameters.config,
    test_settings: client.parameters.settings,
    globals
  })

  if (client.client.settings.sync_test_names) {
    const { name } = pickle

    client.client.updateCapabilities({ name })
  }

  console.log('')

  const { options = {} } = client.client.settings.test_runner

  if (options.auto_start_session) {
    return client.client.launchBrowser().then(browser => {
      client.browser = browser
    })
  }
}
/* eslint-enable camelcase */

export const getProperty = (properties, key) => {
  const defaultValueKey = properties.get(`default.${key}`)
  const specificKey = properties.get(`${os.hostname()}.${key}`)

  return (specificKey || defaultValueKey)
}

export const getConfig = (configName) => {
  const projectRootDir = process.cwd()

  const robotConfig = require(resolve(projectRootDir, 'nummusbot.conf.js'))

  return robotConfig[configName] ?? null
}

export const getOperatingSystem = () => {
  const platform = process.platform

  if (platform === 'linux') {
    context.set('os', 'linux')

    return 'linux'
  }

  if (platform === 'darwin') {
    context.set('os', 'macOS')

    return 'macOS'
  }

  if (platform.includes('win')) {
    context.set('os', 'windows')

    return 'windows'
  }

  context.set('os', 'OS not found')

  return 'OS not found'
}

export const setOperatingSystemKeys = () => {
  const operatingSystem = getOperatingSystem()

  const osKey = operatingSystem !== 'macOS'
    ? browser.Keys.CONTROL
    : browser.Keys.COMMAND

  context.set('osKey', osKey)

  return osKey
}

export const isNumber = (value) => typeof value === 'number'

export const isString = (value) => typeof value === 'string'

export const isObject = (value) => typeof value === 'object'

export const isSymbol = (value) => typeof value === 'symbol'

export const isBoolean = (value) => typeof value === 'boolean'

export const isFunction = (value) => typeof value === 'function'

export const isUndefined = (value) => typeof value === 'undefined'

export const isJSON = (value) => {
  try {
    JSON.parse(value)
    return true
  } catch (error) {
    return false
  }
}

export const ifIsJSONParse = (value) => {
  try {
    return JSON.parse(value)
  } catch (error) {
    return false
  }
}

export const ifIsObjectStringify = (value) => {
  try {
    return JSON.stringify(value, null, 3)
  } catch (error) {
    return false
  }
}
