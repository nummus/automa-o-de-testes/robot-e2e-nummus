import { DateTime } from 'luxon'
import context from '../context.js'

export const parameterValidToAdd = ['+DAY', '+MONTH', '+YEAR', '+HOUR', '+MINUTE', '+SECOND']

export const parameterValidToSubtract = ['-DAY', '-MONTH', '-YEAR', '-HOUR', '-MINUTE', '-SECOND']

export const parameterValidTimeType = ['days', 'months', 'years', 'hours', 'minutes', 'seconds']

export const addTime = (timeNow, timeFormat, timeChangeParameter, timeToAdd) => {
  const indexParameter = parameterValidToAdd.indexOf(timeChangeParameter)

  if (indexParameter === -1) return

  const changeType = parameterValidTimeType[indexParameter]
  const periodResult = timeNow.plus({ [changeType]: timeToAdd })
  const periodResultWithFormat = periodResult.toFormat(timeFormat)
  const dateContextObject = DateTime.fromObject(periodResult.c)
  const dateObjectNoUTC = dateContextObject.plus({ hours: 3 })

  context.set('dateObject', periodResult.c)
  context.set('dateObjectUTC3', dateObjectNoUTC.c)
  context.set(`date${timeFormat}`, periodResult.c)
  context.set(`${timeFormat}`, periodResultWithFormat)

  return periodResultWithFormat
}

export const subtractTime = (timeNow, timeFormat, timeChangeParameter, timeToSubtract) => {
  const indexParameter = parameterValidToSubtract.indexOf(timeChangeParameter)

  if (indexParameter === -1) return

  const changeType = parameterValidTimeType[indexParameter]
  const periodResult = timeNow.minus({ [changeType]: timeToSubtract })
  const periodResultWithFormat = periodResult.toFormat(timeFormat)
  const dateContextObject = DateTime.fromObject(periodResult.c)
  const dateObjectPlus3 = dateContextObject.plus({ hours: 3 })

  context.set('dateObject', periodResult.c)
  context.set('dateObjectUTC3', dateObjectPlus3.c)
  context.set(`date${timeFormat}`, periodResult.c)
  context.set(`${timeFormat}`, periodResultWithFormat)

  return periodResultWithFormat
}

export const setAppointmentTime = (minutesToAdd, setText) => {
  const hourContext = context.get('dateObject')
  const hourContextObject = DateTime.fromObject(hourContext)

  const timeNow = hourContextObject.toFormat('T')
  const timeEnd = hourContextObject
    .plus({ minutes: minutesToAdd }).toFormat('T')

  const appointmentTime = `${timeNow.replace(/:/g, 'h')}-${timeEnd.replace(/:/g, 'h')}`

  context.set(setText, appointmentTime)
}

export const setRepeatingAppointmentTime = (minutesToAdd, setTextBegin, setTextEnd) => {
  const hourContext = context.get('dateObject')
  const hourContextObject = DateTime.fromObject(hourContext)

  const timeNow = hourContextObject.toFormat('T')
  const timeEnd = hourContextObject
    .plus({ minutes: minutesToAdd }).toFormat('T')

  const appointmentTimeBegin = `${timeNow.replace(/:/g, 'h')}-22h00`
  const appointmentTimeEnd = `07h00-${timeEnd.replace(/:/g, 'h')}`

  context.set(setTextBegin, appointmentTimeBegin)
  context.set(setTextEnd, appointmentTimeEnd)
}

export const isDateOrFormat = (time) => {
  const formatsToTry = [
    'yyyy-LL-dd',
    'dd/LL/yyyy',
    'dd-LL-yyyy',
    'dd/LL/yy'
  ]

  const shouldVerify = /\d/.test(time)

  if (!shouldVerify) return null

  for (const format of formatsToTry) {
    const parsedDate = DateTime.fromFormat(time, format)

    if (parsedDate.invalid) continue

    return {
      parsedDate,
      format
    }
  }

  return null
}
