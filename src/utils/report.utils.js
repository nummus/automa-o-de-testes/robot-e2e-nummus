import fs from 'fs'
import { sync as rimrafSync } from 'rimraf'
import context from '../context.js'
import { isJSON } from './base.utils.js'
import logger from './logger'

export const deleteDiR = (directories = []) => {
  for (const directory of directories) {
    if (fs.existsSync(directory)) {
      rimrafSync(directory)
    }
  }
}

export const createDiR = (paths = []) => {
  for (const path of paths) {
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true })
    }
  }
}

const shouldSkipError = (statusSkip, messageSkip, statusError, messageError) => {
  const skipIfStatus = statusSkip && statusSkip === statusError
  const skipIfMessage = messageSkip && messageSkip === messageError

  if (skipIfStatus || skipIfMessage) return true

  return false
}

const validateError = async (value) => {
  try {
    const isErrJson = isJSON(value)
    const isErrObject = typeof value === 'object'

    if (!isErrJson && !isErrObject) return value

    if (value instanceof Error) return value

    const json = JSON.stringify(value, null, 3)

    return json
  } catch {
    return value
  }
}

export const robotErrorHandler = async (errorInfo) => {
  const { error, functionName, customMessage, statusSkip, messageSkip } = errorInfo

  if (context.get('functionWithError') === functionName) return

  context.set('functionWithError', functionName)

  if (customMessage) logger.error('error', customMessage)

  const statusError = error?.response?.status
  const messageError = error?.response?.data?.mensagem

  const shouldSkip = await shouldSkipError(statusSkip, messageSkip, statusError, messageError)

  if (shouldSkip) return

  const err = error?.response?.data || error?.error || error

  let errorToLog = err

  errorToLog = await validateError(err)

  throw new Error(`${functionName}: ${errorToLog}`)
}
