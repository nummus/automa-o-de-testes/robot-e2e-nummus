import { properties } from '../config.js'
import { textResolver } from './resolver.utils.js'

const operatorResolver = (operator) => {
  const operatorKey = Object.keys(operator)[0]

  switch (operatorKey) {
    case '$like':
      return `like "%${operator[operatorKey]}%"`

    case 'eq':
      return `= ${operator[operatorKey]}`

    case 'in': {
      const values = operator[operatorKey]

      if (Array.isArray(values)) {
        const inClausule = values.reduce((previous, current) => {
          if (!previous) return current

          return ` ${current}, ${previous}`
        }, '')

        return ` in (${inClausule})`
      }
      return ` in (${operator[operatorKey]})`
    }
  }
}

export const filterBuilder = (params) => {
  if (!params || !params.filter || typeof params.filter !== 'object') return

  const filter = params.filter
  const filterKeys = Object.keys(filter)
  const filterString = filterKeys.map((key, index) => {
    return `${index ? 'and' : ''} ${key} ${operatorResolver(filter[key])}`
  }).join(' ')

  params.filter = `(${filterString})`
}

export const resolveUrl = async (url) => {
  const urlResolved = await textResolver(url.toLowerCase())

  return properties.get(urlResolved) || urlResolved
}
