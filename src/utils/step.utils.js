import { extractWebdriverId, getWebdriverIdByPageElement } from './element.utils.js'
import { resolveAllText } from './resolver.utils.js'

export const currencyConverter = (value, { language = 'pt-BR', currency = 'BRL' } = {}) => {
  const format = new Intl.NumberFormat(language, { style: 'currency', currency })
  const currencyValue = format.format(value)

  return currencyValue
}

export const replaceWithOsKey = (key, context) => {
  const upperCaseKey = key.toUpperCase()
  const osKey = context.get('osKey')

  if (upperCaseKey.includes('CONTROL') || upperCaseKey.includes('COMMAND')) {
    return osKey
  }

  return upperCaseKey
}

export const updateTextSelector = async (selector) => {
  const isInsideInput = (await browser.elements('css selector', `input${selector}`)).length
  const isInsideTextarea = (await browser.elements('css selector', `textarea${selector}`)).length
  const isOutsideInput = (await browser.elements('css selector', `${selector} input`)).length
  const isOutsideTextarea = (await browser.elements('css selector', `${selector} textarea`)).length

  if (!isInsideInput && !isInsideTextarea) {
    if (isOutsideInput && !isOutsideTextarea) {
      selector = `${selector} input`
    } else if (!isOutsideInput && isOutsideTextarea) {
      selector = `${selector} textarea`
    }
  }

  return selector
}

export const removeSpacesAndSemicolons = (value) => {
  const string = `${value}`

  const textFormated = string.toUpperCase().replace(/\s|;/g, '')

  return textFormated
}

export const checkElementText = async (selector, text, isEquals = true) => {
  const textResolved = await resolveAllText(text)
  const received = removeSpacesAndSemicolons(textResolved)
  const webdriverIds = await browser.elements('css selector', selector)
  const extractedWebdriverIds = extractWebdriverId(webdriverIds)

  for (const webdriverId of extractedWebdriverIds) {
    const textFound = await browser.elementIdText(webdriverId)
    const found = removeSpacesAndSemicolons(textFound)

    if (isEquals && found === received) return webdriverId
    if (!isEquals && found.includes(received)) return webdriverId
  }

  return null
}

export const returnElementText = async (page, element) => {
  if (!page || !element) return

  const webdriverId = await getWebdriverIdByPageElement(page, element)

  const textFound = await browser.elementIdText(webdriverId)

  return textFound
}

export const returnElementPropertyValue = async (page, element, property = 'value') => {
  if (!page || !element) return

  const webdriverId = await getWebdriverIdByPageElement(page, element)

  const valueFound = await browser.elementIdProperty(webdriverId, property)

  return valueFound
}

export const firstLetterUpperCase = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1)
}
