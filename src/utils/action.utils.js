import { properties } from '../config.js'
import context from '../context.js'
import { extractWebdriverId, getPageSelector } from './element.utils.js'
import { resolveAllText } from './resolver.utils.js'
import { checkElementText, updateTextSelector } from './step.utils.js'

export const selectOptionSelector = 'mat-option'
export const loaderSelector = 'sd-context-loader'

export const pressReleaseKey = async (key, times = 10) => {
  for (let index = 0; index < times; index++) {
    await browser.perform(() => {
      const actions = browser.actions({ async: true })

      return actions
        .sendKeys(browser.Keys[key])
    })
  }
}

export const updateFieldWithMask = async (selector, value, index = 0) => {
  const osKey = context.get('osKey')

  await browser
    .waitForElementPresent(selector)
    .click({ selector, index })

  await browser
    .perform(() => {
      const actions = browser.actions({ async: true })

      return actions
        .keyDown(osKey)
        .keyDown('a')
        .keyUp('a')
        .keyUp(osKey)
        .sendKeys(browser.Keys.DELETE)
    })

  await browser.setValue({ selector, index }, value)
}

export const clearOptionSelector = async (cssSelector) => {
  const optionCloseSelector = '[svgicon="close"]'
  const optionCancelSelector = '[svgicon="cancel"]'
  const cssClearSelector = `${cssSelector} ${optionCancelSelector}`
  const cssCloseSelector = `${cssSelector} ${optionCloseSelector}`
  const resultCancelElements = await browser.elements('css selector', cssClearSelector)
  const resultCloseElements = await browser.elements('css selector', cssCloseSelector)

  if (resultCancelElements.length) {
    const cancelWebdriverId = extractWebdriverId(resultCancelElements, true)

    await browser.elementIdClick(cancelWebdriverId)
  }

  if (!resultCloseElements.length) return

  const closeWebdriverId = extractWebdriverId(resultCloseElements, true)

  await browser.elementIdClick(closeWebdriverId)
}

export const tryClickAndClickText = async (
  clickSelector, waitSelector, waitText, waitCondition = true, index = 0
) => {
  await browser.click({ selector: clickSelector, index })

  const webDriverIds = await browser.elements('css selector', waitSelector)

  const webDriverId = extractWebdriverId(webDriverIds, true)

  if (webDriverId) {
    await browser.clickElementText(
      { selector: waitSelector, text: waitText, isEquals: waitCondition }
    )
  }

  return webDriverId
}

export const waitForLoadingNotPresent = async () => {
  const cssSelectors = ['sd-context-loader', 'mat-spinner', '.loading-list']

  for (const cssSelector of cssSelectors) {
    await browser.waitForElementNotPresent(cssSelector)
  }
}

export const moveToElementWithText = async (
  selector, text, isEquals, moveTicks = 1
) => {
  const targetWebdriverId = await checkElementText(selector, text, isEquals)

  for (let index = 0; index < moveTicks; index++) {
    await browser.moveTo(targetWebdriverId)
  }
}

export const pressLeftMouseButton = async () => {
  await browser.perform(() => {
    const actions = browser.actions({ async: true })

    return actions.press()
  })
}

export const releaseLeftMouseButton = async () => {
  await browser.perform(() => {
    const actions = browser.actions({ async: true })

    return actions.release()
  })
}

export const findByPropertyValue = async (selectorInfo) => {
  const { selector, property, value } = selectorInfo
  const webDriverIds = await browser.elements('css selector', selector)
  const webdriverElementIds = extractWebdriverId(webDriverIds)

  for (let index = 0; index < webdriverElementIds.length; index++) {
    const webdriverElementId = webdriverElementIds[index]

    const valueFound = await browser.elementIdProperty(webdriverElementId, property)

    if (valueFound !== value) continue

    return { index, webdriverElementId }
  }

  return -1
}

export const selectText = async (selectInfo) => {
  const { page, element, option = 'igual', text, index = 0 } = selectInfo

  const textResolvedStart = await resolveAllText(text, '|')
  let textResolvedEnd = await resolveAllText(textResolvedStart, ';')

  if (!Array.isArray(textResolvedEnd)) {
    textResolvedEnd = [textResolvedEnd]
  }

  if (!textResolvedEnd.length) return

  let isEquals = true
  const selector = page ? getPageSelector(page, element) : element

  await browser.waitForElementPresent(selector)
  await clearOptionSelector(selector)

  if (option === 'contendo') isEquals = false

  const attemptTimes = properties.get('condition.timeout') / 1000

  for (const textResolved of textResolvedEnd) {
    let clickAction

    for (let attemptCount = 0; attemptCount < attemptTimes; attemptCount++) {
      clickAction = await tryClickAndClickText(
        selector, selectOptionSelector, textResolved, isEquals, index
      )

      if (clickAction) break

      await browser.pause(1000)
    }

    if (!clickAction) {
      await browser.waitForElementText(
        { selector: selectOptionSelector, text: textResolved, isEquals }
      )
    }
  }
}

export const setAndSelectText = async (selectInfo) => {
  const { page, element, option = 'igual', text, index = 0, indexSet = 0 } = selectInfo

  const textResolvedStart = await resolveAllText(text, '|')
  let textResolvedEnd = await resolveAllText(textResolvedStart, ';')

  if (!Array.isArray(textResolvedEnd)) {
    textResolvedEnd = [textResolvedEnd]
  }

  if (!textResolvedEnd.length) return

  let isEquals = true
  const selector = page ? getPageSelector(page, element) : element

  await browser.waitForElementPresent(selector)

  const updatedSelector = await updateTextSelector(selector)

  if (option === 'contendo') isEquals = false

  for (const textResolved of textResolvedEnd) {
    const elementTextInfo = { selector: selectOptionSelector, text: textResolved, index, isEquals }

    await browser
      .click(selector)
      .setValue({ selector: updatedSelector, index: indexSet }, textResolved)
      .clickElementText(elementTextInfo)
  }
}
