const path = require('path')
const { spawn } = require('child_process')
const bannerRobot = require('./bannerRobot.js')
const { deleteDiR, createDiR } = require('./lib/utils/report.utils.js')
const { getOperatingSystem } = require('./lib/utils/base.utils.js')
const { DEFAULT_THEME } = require('@cucumber/pretty-formatter')
const logger = require('./lib/utils/logger.js')

/*!
 * All paths needed for automation
 */
let onlyOnStartup = true
const robotConfig = require(path.resolve('./nummusbot.conf.js'))
const nightwatchConfig = path.join(__dirname, './nightwatch.conf.js')
const internalStepsPaths = path.join(__dirname, '/lib/steps')
const externalStepPaths = path.resolve(robotConfig.stepsPath)
const featuresPath = path.resolve(robotConfig.featuresPath)
const reportsPath = path.join(path.resolve(robotConfig.reportsPath))
const cucumberJsonPath = path.join(reportsPath, 'cucumber-report.json')
const screenshotsPath = path.join(path.resolve(robotConfig.screenshotsPath))
const NWCucumberFilePath = path.resolve('./node_modules/nightwatch/cucumber-js')

const cucumberPrettyTheme = {
  ...DEFAULT_THEME,
  'step text': ['green', 'bold'],
  'step keyword': ['cyan', 'bold'],
  tag: ['magenta', 'bold'],
  'scenario keyword': ['blue', 'bold'],
  'scenario name': ['white', 'bold'],
  'feature name': ['white', 'bold']
}

/*!
 * Show banner and update reports directory
 */
const performStartupTasks = () => {
  bannerRobot()

  if (!process.env.NODE_ENV) {
    logger.warn('warning', 'Local test? set env: NODE_ENV: development')
    logger.warn('warning', 'Pipeline test? set env: NODE_ENV: test\n')
  }

  deleteDiR([reportsPath, NWCucumberFilePath])

  createDiR([reportsPath, screenshotsPath])

  onlyOnStartup = false
}

if (onlyOnStartup) performStartupTasks()

/*!
 * Set run args and cucumber-js formatting
 */
const runArgs = [
  '--config',
  nightwatchConfig,
  '--require-module',
  '@babel/register',
  '--require',
  internalStepsPaths,
  '--require',
  featuresPath,
  '--format',
  path.resolve('./node_modules/@cucumber/pretty-formatter'),
  '--format-options',
  `{"colorsEnabled": true, "theme":${JSON.stringify(cucumberPrettyTheme)}}`,
  '--format',
  `json:${cucumberJsonPath}`
]

/*!
 * Require external steps
 */
if (externalStepPaths) runArgs.push('--require', externalStepPaths)

/*!
 * Set CucumberJS tags
 */
process.setMaxListeners(25)
const processArgs = process.argv
const commandTagsIndex = processArgs.indexOf('--tags')

if (commandTagsIndex >= 0) {
  let tagsList = processArgs.slice(commandTagsIndex + 1, processArgs.length)

  runArgs.push(processArgs[commandTagsIndex].replace(/"/g, ''))

  if (tagsList.length) {
    tagsList = tagsList.join(' ')

    runArgs.push(tagsList.replace(/"/g, ''))
  } else {
    logger.error('error', 'Automation need at least one tag!')
  }
}

const os = getOperatingSystem()

/*!
 * Start automation process
 */
const nightwatchPath = os === 'windows'
  ? path.resolve('./node_modules/.bin/nightwatch.cmd')
  : path.resolve('./node_modules/.bin/nightwatch')

const childProcess = spawn(nightwatchPath, runArgs, { stdio: 'inherit' })

/*!
 * Release pool if automation is stopped with CTRL+C
 */
process.on('SIGINT', async (code) => {
  process.removeAllListeners('SIGINT')

  process.exit(code)
})

/*!
 * Release pool if automation is stopped by error or success
 */
childProcess.on('exit', async (code) => {
  if (code === 0) {
    logger.info('action', 'Automation finished with success!')
  } else {
    logger.info('error', 'Automation finished with error!')
  }

  process.exit(code)
})
