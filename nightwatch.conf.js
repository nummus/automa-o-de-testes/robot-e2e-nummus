require('core-js/stable')
const path = require('path')
const { properties } = require('./lib/config')
const chromedriver = require('chromedriver')

/*!  _   _  _         _      _                     _          _
 *  | \ | |(_)       | |    | |                   | |        | |
 *  |  \| | _   __ _ | |__  | |_ __      __  __ _ | |_   ___ | |__
 *  | . ` || | / _` || '_ \ | __|\ \ /\ / / / _` || __| / __|| '_ \
 *  | |\  || || (_| || | | || |_  \ V  V / | (_| || |_ | (__ | | | |
 *  \_| \_/|_| \__, ||_| |_| \__|  \_/\_/   \__,_| \__| \___||_| |_|
 *              __/ |
 *             |___/
 */

/*!
 * Basic automation configuration
 */
const launchUrl = properties.get('url.launch')
let isHeadless = properties.get('browser.headless')
const workersConfig = { enabled: true, workers: 2 }
const nightwatchOutput = properties.get('nightwatch.output')
const timeout = properties.get('condition.timeout', 60000)
const testWorkers = properties.get('test.workers') ? workersConfig : false
let nightwatchDetailedOutput = properties.get('nightwatch.detailed.output')
const chromeArgs = ['--no-sandbox', '--ignore-certificate-errors', '--allow-insecure-localhost']

if (process.env.NODE_ENV === 'test') {
  isHeadless = true
  nightwatchDetailedOutput = false
}

if (isHeadless) chromeArgs.push('--headless=new')

/*!
 * Paths
 */
const robotConfig = require(path.resolve('./nummusbot.conf.js'))
const screenshotsPath = path.resolve(robotConfig.screenshotsPath)
const stepsPaths = [path.join(__dirname, 'lib', 'steps'), path.resolve(robotConfig.stepsPath)]
const pageObjectsPaths = [path.join(__dirname, 'lib', 'pages'), path.resolve(robotConfig.pagesPath)]
const commandsPaths = [path.join(__dirname, 'lib', 'commands'), path.resolve(robotConfig.commandsPath)]
const featuresPath = `${path.resolve(robotConfig.featuresPath)}/**/*.feature`

properties.set('resolvers.path', path.resolve(robotConfig.resolversPath))

module.exports = {
  src_folders: stepsPaths,
  page_objects_path: pageObjectsPaths,
  custom_commands_path: commandsPaths,
  test_runner: {
    type: 'cucumber',
    options: {
      feature_path: featuresPath,
      auto_start_session: true
    }
  },
  test_workers: testWorkers,
  output_folder: false,
  output: nightwatchOutput,
  detailed_output: nightwatchDetailedOutput,
  skip_testcases_on_fail: false,
  end_session_on_fail: false,
  test_settings: {
    default: {
      element_command_retries: 0,
      disable_error_log: false,
      launch_url: launchUrl,
      screenshots: {
        enabled: true,
        on_failure: true,
        on_sucess: true,
        on_error: true,
        path: screenshotsPath
      },
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        webStorageEnabled: true,
        applicationCacheEnabled: true,
        'goog:chromeOptions': {
          w3c: true,
          args: chromeArgs
        }
      },
      webdriver: {
        start_process: true,
        server_path: chromedriver.path,
        timeout_options: {
          timeout: 60000,
          retry_attempts: 15
        }
      },
      persist_globals: true,
      globals: {
        waitForConditionTimeout: timeout,
        waitForConditionPollInterval: 500,
        abortOnElementLocateError: true
      }
    },
    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        webStorageEnabled: true,
        applicationCacheEnabled: true,
        'goog:chromeOptions': {
          w3c: true,
          args: chromeArgs
        }
      }
    },
    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        alwaysMatch: {
          acceptInsecureCerts: true,
          'moz:firefoxOptions': {
            args: [
              // '-headless',
              // '-verbose'
            ]
          }
        }
      }
    }
  }
}
